﻿using NHibernate.Cfg;
using NHibernateLinQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ
{
    public class Initial
    {
        public static void Seed(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {

                using (var tx = session.BeginTransaction())
                {
                    GetCustomers().ForEach(c => {
                        session.Save(c);
                    });
                    GetCategories().ForEach(i => session.Save(i));
                    GetEmployees().ForEach(i => session.Save(i));
                    //GetOrders().ForEach(i => session.Save(i));
                    //GetOrderDetails().ForEach(i => session.Save(i));
                    //GetProducts().ForEach(i => session.Save(i));
                    //GetShippers().ForEach(i => session.Save(i));
                    //GetSuppliers().ForEach(i => session.Save(i));
                    tx.Commit();
                }

            }


        }
        public static List<SeedCustomer> GetCustomers()
        {
            List<SeedCustomer> customers = new List<SeedCustomer>();
            var item1 = new SeedCustomer()
            {
                CustomerID = 1,
                CustomerName = "Alfreds Futterkiste",
                ContactName = "Maria Anders",
                Address = new Location()
                {
                    Address = "Obere Str. 57",
                    City = "Berlin",
                    PostalCode = "12209",
                    Country = "Germany"
                }
            };
            customers.Add(item1);
            var item2 = new SeedCustomer()
            {
                CustomerID = 2,
                CustomerName = "Ana Trujillo Emparedados y helados",
                ContactName = "Ana Trujillo",
                Address = new Location()
                {
                    Address = "Avda. de la Constitución 2222",
                    City = "México D.F.",
                    PostalCode = "05021",
                    Country = "Mexico"
                }
            };
            customers.Add(item2);
            var item3 = new SeedCustomer()
            {
                CustomerID = 3,
                CustomerName = "Antonio Moreno Taquería",
                ContactName = "Antonio Moreno",
                Address = new Location()
                {
                    Address = "Mataderos 2312",
                    City = "México D.F.",
                    PostalCode = "05023",
                    Country = "Mexico"
                }
            };
            customers.Add(item3);
            var item4 = new SeedCustomer()
            {
                CustomerID = 4,
                CustomerName = "Around the Horn",
                ContactName = "Thomas Hardy",
                Address = new Location()
                {
                    Address = "120 Hanover Sq.",
                    City = "London",
                    PostalCode = "WA1 1DP",
                    Country = "UK"
                }
            };
            customers.Add(item4);
            var item5 = new SeedCustomer()
            {
                CustomerID = 5,
                CustomerName = "Berglunds snabbköp",
                ContactName = "Christina Berglund",
                Address = new Location()
                {
                    Address = "Berguvsvägen 8",
                    City = "Luleå",
                    PostalCode = "S-958 22",
                    Country = "Sweden"
                }
            };
            customers.Add(item5);
            var item6 = new SeedCustomer()
            {
                CustomerID = 6,
                CustomerName = "Blauer See Delikatessen",
                ContactName = "Hanna Moos",
                Address = new Location()
                {
                    Address = "Forsterstr. 57",
                    City = "Mannheim",
                    PostalCode = "68306",
                    Country = "Germany"
                }
            };
            customers.Add(item6);
            var item7 = new SeedCustomer()
            {
                CustomerID = 7,
                CustomerName = "Blondel père et fils",
                ContactName = "Frédérique Citeaux",
                Address = new Location()
                {
                    Address = "24, place Kléber",
                    City = "Strasbourg",
                    PostalCode = "67000",
                    Country = "France"
                }
            };
            customers.Add(item7);
            var item8 = new SeedCustomer()
            {
                CustomerID = 8,
                CustomerName = "Bólido Comidas preparadas",
                ContactName = "Martín Sommer",
                Address = new Location()
                {
                    Address = "C/ Araquil, 67",
                    City = "Madrid",
                    PostalCode = "28023",
                    Country = "Spain"
                }
            };
            customers.Add(item8);
            var item9 = new SeedCustomer()
            {
                CustomerID = 9,
                CustomerName = "Bon app'",
                ContactName = "Laurence Lebihans",
                Address = new Location()
                {
                    Address = "12, rue des Bouchers",
                    City = "Marseille",
                    PostalCode = "13008",
                    Country = "France"
                }
            };
            customers.Add(item9);
            var item10 = new SeedCustomer()
            {
                CustomerID = 10,
                CustomerName = "Bottom-Dollar Marketse",
                ContactName = "Elizabeth Lincoln",
                Address = new Location()
                {
                    Address = "23 Tsawassen Blvd.",
                    City = "Tsawassen",
                    PostalCode = "T2F 8M4",
                    Country = "Canada"
                }
            };
            customers.Add(item10);
            var item11 = new SeedCustomer()
            {
                CustomerID = 11,
                CustomerName = "B's Beverages",
                ContactName = "Victoria Ashworth",
                Address = new Location()
                {
                    Address = "Fauntleroy Circus",
                    City = "London",
                    PostalCode = "EC2 5NT",
                    Country = "UK"
                }
            };
            customers.Add(item11);
            var item12 = new SeedCustomer()
            {
                CustomerID = 12,
                CustomerName = "Cactus Comidas para llevar",
                ContactName = "Patricio Simpson",
                Address = new Location()
                {
                    Address = "Cerrito 333",
                    City = "Buenos Aires",
                    PostalCode = "1010",
                    Country = "Argentina"
                }
            };
            customers.Add(item12);
            var item13 = new SeedCustomer()
            {
                CustomerID = 13,
                CustomerName = "Centro comercial Moctezuma",
                ContactName = "Francisco Chang",
                Address = new Location()
                {
                    Address = "Sierras de Granada 9993",
                    City = "México D.F.",
                    PostalCode = "05022",
                    Country = "Mexico"
                }
            };
            customers.Add(item13);
            var item14 = new SeedCustomer()
            {
                CustomerID = 14,
                CustomerName = "Chop-suey Chinese",
                ContactName = "Yang Wang",
                Address = new Location()
                {
                    Address = "Hauptstr. 29",
                    City = "Bern",
                    PostalCode = "3012",
                    Country = "Switzerland"
                }
            };
            customers.Add(item14);
            var item15 = new SeedCustomer()
            {
                CustomerID = 15,
                CustomerName = "Comércio Mineiro",
                ContactName = "Pedro Afonso",
                Address = new Location()
                {
                    Address = "Av. dos Lusíadas, 23",
                    City = "São Paulo",
                    PostalCode = "05432-043",
                    Country = "Brazil"
                }
            };
            customers.Add(item15);
            var item16 = new SeedCustomer()
            {
                CustomerID = 16,
                CustomerName = "Consolidated Holdings",
                ContactName = "Elizabeth Brown",
                Address = new Location()
                {
                    Address = "Berkeley Gardens 12 Brewery",
                    City = "London",
                    PostalCode = "WX1 6LT",
                    Country = "UK"
                }
            };
            customers.Add(item16);
            var item17 = new SeedCustomer()
            {
                CustomerID = 17,
                CustomerName = "Drachenblut Delikatessend",
                ContactName = "Sven Ottlieb",
                Address = new Location()
                {
                    Address = "Walserweg 21",
                    City = "Aachen",
                    PostalCode = "52066",
                    Country = "Germany"
                }
            };
            customers.Add(item17);
            var item18 = new SeedCustomer()
            {
                CustomerID = 18,
                CustomerName = "Du monde entier",
                ContactName = "Janine Labrune",
                Address = new Location()
                {
                    Address = "67, rue des Cinquante Otages",
                    City = "Nantes",
                    PostalCode = "44000",
                    Country = "France"
                }
            };
            customers.Add(item18);
            var item19 = new SeedCustomer()
            {
                CustomerID = 19,
                CustomerName = "Eastern Connection",
                ContactName = "Ann Devon",
                Address = new Location()
                {
                    Address = "35 King George",
                    City = "London",
                    PostalCode = "WX3 6FW",
                    Country = "UK"
                }
            };
            customers.Add(item19);
            var item20 = new SeedCustomer()
            {
                CustomerID = 20,
                CustomerName = "Ernst Handel",
                ContactName = "Roland Mendel",
                Address = new Location()
                {
                    Address = "Kirchgasse 6",
                    City = "Graz",
                    PostalCode = "8010",
                    Country = "Austria"
                }
            };
            customers.Add(item20);
            var item21 = new SeedCustomer()
            {
                CustomerID = 21,
                CustomerName = "Familia Arquibaldo",
                ContactName = "Aria Cruz",
                Address = new Location()
                {
                    Address = "Rua Orós, 92",
                    City = "São Paulo",
                    PostalCode = "05442-030",
                    Country = "Brazil"
                }
            };
            customers.Add(item21);
            var item22 = new SeedCustomer()
            {
                CustomerID = 22,
                CustomerName = "FISSA Fabrica Inter. Salchichas S.A.",
                ContactName = "Diego Roel",
                Address = new Location()
                {
                    Address = "C/ Moralzarzal, 86",
                    City = "Madrid",
                    PostalCode = "28034",
                    Country = "Spain"
                }
            };
            customers.Add(item22);
            var item23 = new SeedCustomer()
            {
                CustomerID = 23,
                CustomerName = "Folies gourmandes",
                ContactName = "Martine Rancé",
                Address = new Location()
                {
                    Address = "184, chaussée de Tournai",
                    City = "Lille",
                    PostalCode = "59000",
                    Country = "France"
                }
            };
            customers.Add(item23);
            var item24 = new SeedCustomer()
            {
                CustomerID = 24,
                CustomerName = "Folk och fä HB",
                ContactName = "Maria Larsson",
                Address = new Location()
                {
                    Address = "Åkergatan 24",
                    City = "Bräcke",
                    PostalCode = "S-844 67",
                    Country = "Sweden"
                }
            };
            customers.Add(item24);
            var item25 = new SeedCustomer()
            {
                CustomerID = 25,
                CustomerName = "Frankenversand",
                ContactName = "Peter Franken",
                Address = new Location()
                {
                    Address = "Berliner Platz 43",
                    City = "München",
                    PostalCode = "80805",
                    Country = "Germany"
                }
            };
            customers.Add(item25);
            var item26 = new SeedCustomer()
            {
                CustomerID = 26,
                CustomerName = "France restauration",
                ContactName = "Carine Schmitt",
                Address = new Location()
                {
                    Address = "54, rue Royale",
                    City = "Nantes",
                    PostalCode = "44000",
                    Country = "France"
                }
            };
            customers.Add(item26);
            var item27 = new SeedCustomer()
            {
                CustomerID = 27,
                CustomerName = "Franchi S.p.A.",
                ContactName = "Paolo Accorti",
                Address = new Location()
                {
                    Address = "Via Monte Bianco 34",
                    City = "Torino",
                    PostalCode = "10100",
                    Country = "Italy"
                }
            };
            customers.Add(item27);
            var item28 = new SeedCustomer()
            {
                CustomerID = 28,
                CustomerName = "Furia Bacalhau e Frutos do Mar",
                ContactName = "Lino Rodriguez",
                Address = new Location()
                {
                    Address = "Jardim das rosas n. 32",
                    City = "Lisboa",
                    PostalCode = "1675",
                    Country = "Portugal"
                }
            };
            customers.Add(item28);
            var item29 = new SeedCustomer()
            {
                CustomerID = 29,
                CustomerName = "Galería del gastrónomo",
                ContactName = "Eduardo Saavedra",
                Address = new Location()
                {
                    Address = "Rambla de Cataluña, 23",
                    City = "Barcelona",
                    PostalCode = "08022",
                    Country = "Spain"
                }
            };
            customers.Add(item29);
            var item30 = new SeedCustomer()
            {
                CustomerID = 30,
                CustomerName = "Godos Cocina Típica",
                ContactName = "José Pedro Freyre",
                Address = new Location()
                {
                    Address = "C/ Romero, 33",
                    City = "Sevilla",
                    PostalCode = "41101",
                    Country = "Spain"
                }
            };
            customers.Add(item30);
            var item31 = new SeedCustomer()
            {
                CustomerID = 31,
                CustomerName = "Gourmet Lanchonetes",
                ContactName = "André Fonseca",
                Address = new Location()
                {
                    Address = "Av. Brasil, 442",
                    City = "Campinas",
                    PostalCode = "04876-786",
                    Country = "Brazil"
                }
            };
            customers.Add(item31);
            var item32 = new SeedCustomer()
            {
                CustomerID = 32,
                CustomerName = "Great Lakes Food Market",
                ContactName = "Howard Snyder",
                Address = new Location()
                {
                    Address = "2732 Baker Blvd.",
                    City = "Eugene",
                    PostalCode = "97403",
                    Country = "USA"
                }
            };
            customers.Add(item32);
            var item33 = new SeedCustomer()
            {
                CustomerID = 33,
                CustomerName = "GROSELLA-Restaurante",
                ContactName = "Manuel Pereira",
                Address = new Location()
                {
                    Address = "5ª Ave. Los Palos Grandes",
                    City = "Caracas",
                    PostalCode = "1081",
                    Country = "Venezuela"
                }
            };
            customers.Add(item33);
            var item34 = new SeedCustomer()
            {
                CustomerID = 34,
                CustomerName = "Hanari Carnes",
                ContactName = "Mario Pontes",
                Address = new Location()
                {
                    Address = "Rua do Paço, 67",
                    City = "Rio de Janeiro",
                    PostalCode = "05454-876",
                    Country = "Brazil"
                }
            };
            customers.Add(item34);
            var item35 = new SeedCustomer()
            {
                CustomerID = 35,
                CustomerName = "HILARIÓN-Abastos",
                ContactName = "Carlos Hernández",
                Address = new Location()
                {
                    Address = "Carrera 22 con Ave. Carlos Soublette #8-35",
                    City = "San Cristóbal",
                    PostalCode = "5022",
                    Country = "Venezuela"
                }
            };
            customers.Add(item35);
            var item36 = new SeedCustomer()
            {
                CustomerID = 36,
                CustomerName = "Hungry Coyote Import Store",
                ContactName = "Yoshi Latimer",
                Address = new Location()
                {
                    Address = "City Center Plaza 516 Main St.",
                    City = "Elgin",
                    PostalCode = "97827",
                    Country = "USA"
                }
            };
            customers.Add(item36);
            var item37 = new SeedCustomer()
            {
                CustomerID = 37,
                CustomerName = "Hungry Owl All-Night Grocers",
                ContactName = "Patricia McKenna",
                Address = new Location()
                {
                    Address = "8 Johnstown Road",
                    City = "Cork",
                    PostalCode = "",
                    Country = "Ireland"
                }
            };
            customers.Add(item37);
            var item38 = new SeedCustomer()
            {
                CustomerID = 38,
                CustomerName = "Island Trading",
                ContactName = "Helen Bennett",
                Address = new Location()
                {
                    Address = "Garden House Crowther Way",
                    City = "Cowes",
                    PostalCode = "PO31 7PJ",
                    Country = "UK"
                }
            };
            customers.Add(item38);
            var item39 = new SeedCustomer()
            {
                CustomerID = 39,
                CustomerName = "Königlich Essen",
                ContactName = "Philip Cramer",
                Address = new Location()
                {
                    Address = "Maubelstr. 90",
                    City = "Brandenburg",
                    PostalCode = "14776",
                    Country = "Germany"
                }
            };
            customers.Add(item39);
            var item40 = new SeedCustomer()
            {
                CustomerID = 40,
                CustomerName = "La corne d'abondance",
                ContactName = "Daniel Tonini",
                Address = new Location()
                {
                    Address = "67, avenue de l'Europe",
                    City = "Versailles",
                    PostalCode = "78000",
                    Country = "France"
                }
            };
            customers.Add(item40);
            var item41 = new SeedCustomer()
            {
                CustomerID = 41,
                CustomerName = "La maison d'Asie",
                ContactName = "Annette Roulet",
                Address = new Location()
                {
                    Address = "1 rue Alsace-Lorraine",
                    City = "Toulouse",
                    PostalCode = "31000",
                    Country = "France"
                }
            };
            customers.Add(item41);
            var item42 = new SeedCustomer()
            {
                CustomerID = 42,
                CustomerName = "Laughing Bacchus Wine Cellars",
                ContactName = "Yoshi Tannamuri",
                Address = new Location()
                {
                    Address = "1900 Oak St.",
                    City = "Vancouver",
                    PostalCode = "V3F 2K1",
                    Country = "Canada"
                }
            };
            customers.Add(item42);
            var item43 = new SeedCustomer()
            {
                CustomerID = 43,
                CustomerName = "Lazy K Kountry Store",
                ContactName = "John Steel",
                Address = new Location()
                {
                    Address = "12 Orchestra Terrace",
                    City = "Walla Walla",
                    PostalCode = "99362",
                    Country = "USA"
                }
            };
            customers.Add(item43);
            var item44 = new SeedCustomer()
            {
                CustomerID = 44,
                CustomerName = "Lehmanns Marktstand",
                ContactName = "Renate Messner",
                Address = new Location()
                {
                    Address = "Magazinweg 7",
                    City = "Frankfurt a.M.",
                    PostalCode = "60528",
                    Country = "Germany"
                }
            };
            customers.Add(item44);
            var item45 = new SeedCustomer()
            {
                CustomerID = 45,
                CustomerName = "Let's Stop N Shop",
                ContactName = "Jaime Yorres",
                Address = new Location()
                {
                    Address = "87 Polk St. Suite 5",
                    City = "San Francisco",
                    PostalCode = "94117",
                    Country = "USA"
                }
            };
            customers.Add(item45);
            var item46 = new SeedCustomer()
            {
                CustomerID = 46,
                CustomerName = "LILA-Supermercado",
                ContactName = "Carlos González",
                Address = new Location()
                {
                    Address = "Carrera 52 con Ave. Bolívar #65-98 Llano Largo",
                    City = "Barquisimeto",
                    PostalCode = "3508",
                    Country = "Venezuela"
                }
            };
            customers.Add(item46);
            var item47 = new SeedCustomer()
            {
                CustomerID = 47,
                CustomerName = "LINO-Delicateses",
                ContactName = "Felipe Izquierdo",
                Address = new Location()
                {
                    Address = "Ave. 5 de Mayo Porlamar",
                    City = "I. de Margarita",
                    PostalCode = "4980",
                    Country = "Venezuela"
                }
            };
            customers.Add(item47);
            var item48 = new SeedCustomer()
            {
                CustomerID = 48,
                CustomerName = "Lonesome Pine Restaurant",
                ContactName = "Fran Wilson",
                Address = new Location()
                {
                    Address = "89 Chiaroscuro Rd.",
                    City = "Portland",
                    PostalCode = "97219",
                    Country = "USA"
                }
            };
            customers.Add(item48);
            var item49 = new SeedCustomer()
            {
                CustomerID = 49,
                CustomerName = "Magazzini Alimentari Riuniti",
                ContactName = "Giovanni Rovelli",
                Address = new Location()
                {
                    Address = "Via Ludovico il Moro 22",
                    City = "Bergamo",
                    PostalCode = "24100",
                    Country = "Italy"
                }
            };
            customers.Add(item49);
            var item50 = new SeedCustomer()
            {
                CustomerID = 50,
                CustomerName = "Maison Dewey",
                ContactName = "Catherine Dewey",
                Address = new Location()
                {
                    Address = "Rue Joseph-Bens 532",
                    City = "Bruxelles",
                    PostalCode = "B-1180",
                    Country = "Belgium"
                }
            };
            customers.Add(item50);
            var item51 = new SeedCustomer()
            {
                CustomerID = 51,
                CustomerName = "Mère Paillarde",
                ContactName = "Jean Fresnière",
                Address = new Location()
                {
                    Address = "43 rue St. Laurent",
                    City = "Montréal",
                    PostalCode = "H1J 1C3",
                    Country = "Canada"
                }
            };
            customers.Add(item51);
            var item52 = new SeedCustomer()
            {
                CustomerID = 52,
                CustomerName = "Morgenstern Gesundkost",
                ContactName = "Alexander Feuer",
                Address = new Location()
                {
                    Address = "Heerstr. 22",
                    City = "Leipzig",
                    PostalCode = "04179",
                    Country = "Germany"
                }
            };
            customers.Add(item52);
            var item53 = new SeedCustomer()
            {
                CustomerID = 53,
                CustomerName = "North/South",
                ContactName = "Simon Crowther",
                Address = new Location()
                {
                    Address = "South House 300 Queensbridge",
                    City = "London",
                    PostalCode = "SW7 1RZ",
                    Country = "UK"
                }
            };
            customers.Add(item53);
            var item54 = new SeedCustomer()
            {
                CustomerID = 54,
                CustomerName = "Océano Atlántico Ltda.",
                ContactName = "Yvonne Moncada",
                Address = new Location()
                {
                    Address = "Ing. Gustavo Moncada 8585 Piso 20-A",
                    City = "Buenos Aires",
                    PostalCode = "1010",
                    Country = "Argentina"
                }
            };
            customers.Add(item54);
            var item55 = new SeedCustomer()
            {
                CustomerID = 55,
                CustomerName = "Old World Delicatessen",
                ContactName = "Rene Phillips",
                Address = new Location()
                {
                    Address = "2743 Bering St.",
                    City = "Anchorage",
                    PostalCode = "99508",
                    Country = "USA"
                }
            };
            customers.Add(item55);
            var item56 = new SeedCustomer()
            {
                CustomerID = 56,
                CustomerName = "Ottilies Käseladen",
                ContactName = "Henriette Pfalzheim",
                Address = new Location()
                {
                    Address = "Mehrheimerstr. 369",
                    City = "Köln",
                    PostalCode = "50739",
                    Country = "Germany"
                }
            };
            customers.Add(item56);
            var item57 = new SeedCustomer()
            {
                CustomerID = 57,
                CustomerName = "Paris spécialités",
                ContactName = "Marie Bertrand",
                Address = new Location()
                {
                    Address = "265, boulevard Charonne",
                    City = "Paris",
                    PostalCode = "75012",
                    Country = "France"
                }
            };
            customers.Add(item57);
            var item58 = new SeedCustomer()
            {
                CustomerID = 58,
                CustomerName = "Pericles Comidas clásicas",
                ContactName = "Guillermo Fernández",
                Address = new Location()
                {
                    Address = "Calle Dr. Jorge Cash 321",
                    City = "México D.F.",
                    PostalCode = "05033",
                    Country = "Mexico"
                }
            };
            customers.Add(item58);
            var item59 = new SeedCustomer()
            {
                CustomerID = 59,
                CustomerName = "Piccolo und mehr",
                ContactName = "Georg Pipps",
                Address = new Location()
                {
                    Address = "Geislweg 14",
                    City = "Salzburg",
                    PostalCode = "5020",
                    Country = "Austria"
                }
            };
            customers.Add(item59);
            var item60 = new SeedCustomer()
            {
                CustomerID = 60,
                CustomerName = "Princesa Isabel Vinhoss",
                ContactName = "Isabel de Castro",
                Address = new Location()
                {
                    Address = "Estrada da saúde n. 58",
                    City = "Lisboa",
                    PostalCode = "1756",
                    Country = "Portugal"
                }
            };
            customers.Add(item60);
            var item61 = new SeedCustomer()
            {
                CustomerID = 61,
                CustomerName = "Que Delícia",
                ContactName = "Bernardo Batista",
                Address = new Location()
                {
                    Address = "Rua da Panificadora, 12",
                    City = "Rio de Janeiro",
                    PostalCode = "02389-673",
                    Country = "Brazil"
                }
            };
            customers.Add(item61);
            var item62 = new SeedCustomer()
            {
                CustomerID = 62,
                CustomerName = "Queen Cozinha",
                ContactName = "Lúcia Carvalho",
                Address = new Location()
                {
                    Address = "Alameda dos Canàrios, 891",
                    City = "São Paulo",
                    PostalCode = "05487-020",
                    Country = "Brazil"
                }
            };
            customers.Add(item62);
            var item63 = new SeedCustomer()
            {
                CustomerID = 63,
                CustomerName = "QUICK-Stop",
                ContactName = "Horst Kloss",
                Address = new Location()
                {
                    Address = "Taucherstraße 10",
                    City = "Cunewalde",
                    PostalCode = "01307",
                    Country = "Germany"
                }
            };
            customers.Add(item63);
            var item64 = new SeedCustomer()
            {
                CustomerID = 64,
                CustomerName = "Rancho grande",
                ContactName = "Sergio Gutiérrez",
                Address = new Location()
                {
                    Address = "Av. del Libertador 900",
                    City = "Buenos Aires",
                    PostalCode = "1010",
                    Country = "Argentina"
                }
            };
            customers.Add(item64);
            var item65 = new SeedCustomer()
            {
                CustomerID = 65,
                CustomerName = "Rattlesnake Canyon Grocery",
                ContactName = "Paula Wilson",
                Address = new Location()
                {
                    Address = "2817 Milton Dr.",
                    City = "Albuquerque",
                    PostalCode = "87110",
                    Country = "USA"
                }
            };
            customers.Add(item65);
            var item66 = new SeedCustomer()
            {
                CustomerID = 66,
                CustomerName = "Reggiani Caseifici",
                ContactName = "Maurizio Moroni",
                Address = new Location()
                {
                    Address = "Strada Provinciale 124",
                    City = "Reggio Emilia",
                    PostalCode = "42100",
                    Country = "Italy"
                }
            };
            customers.Add(item66);
            var item67 = new SeedCustomer()
            {
                CustomerID = 67,
                CustomerName = "Ricardo Adocicados",
                ContactName = "Janete Limeira",
                Address = new Location()
                {
                    Address = "Av. Copacabana, 267",
                    City = "Rio de Janeiro",
                    PostalCode = "02389-890",
                    Country = "Brazil"
                }
            };
            customers.Add(item67);
            var item68 = new SeedCustomer()
            {
                CustomerID = 68,
                CustomerName = "Richter Supermarkt",
                ContactName = "Michael Holz",
                Address = new Location()
                {
                    Address = "Grenzacherweg 237",
                    City = "Genève",
                    PostalCode = "1203",
                    Country = "Switzerland"
                }
            };
            customers.Add(item68);
            var item69 = new SeedCustomer()
            {
                CustomerID = 69,
                CustomerName = "Romero y tomillo",
                ContactName = "Alejandra Camino",
                Address = new Location()
                {
                    Address = "Gran Vía, 1",
                    City = "Madrid",
                    PostalCode = "28001",
                    Country = "Spain"
                }
            };
            customers.Add(item69);
            var item70 = new SeedCustomer()
            {
                CustomerID = 70,
                CustomerName = "Santé Gourmet",
                ContactName = "Jonas Bergulfsen",
                Address = new Location()
                {
                    Address = "Erling Skakkes gate 78",
                    City = "Stavern",
                    PostalCode = "4110",
                    Country = "Norway"
                }
            };
            customers.Add(item70);
            var item71 = new SeedCustomer()
            {
                CustomerID = 71,
                CustomerName = "Save-a-lot Markets",
                ContactName = "Jose Pavarotti",
                Address = new Location()
                {
                    Address = "187 Suffolk Ln.",
                    City = "Boise",
                    PostalCode = "83720",
                    Country = "USA"
                }
            };
            customers.Add(item71);
            var item72 = new SeedCustomer()
            {
                CustomerID = 72,
                CustomerName = "Seven Seas Imports",
                ContactName = "Hari Kumar",
                Address = new Location()
                {
                    Address = "90 Wadhurst Rd.",
                    City = "London",
                    PostalCode = "OX15 4NB",
                    Country = "UK"
                }
            };
            customers.Add(item72);
            var item73 = new SeedCustomer()
            {
                CustomerID = 73,
                CustomerName = "Simons bistro",
                ContactName = "Jytte Petersen",
                Address = new Location()
                {
                    Address = "Vinbæltet 34",
                    City = "København",
                    PostalCode = "1734",
                    Country = "Denmark"
                }
            };
            customers.Add(item73);
            var item74 = new SeedCustomer()
            {
                CustomerID = 74,
                CustomerName = "Spécialités du monde",
                ContactName = "Dominique Perrier",
                Address = new Location()
                {
                    Address = "25, rue Lauriston",
                    City = "Paris",
                    PostalCode = "75016",
                    Country = "France"
                }
            };
            customers.Add(item74);
            var item75 = new SeedCustomer()
            {
                CustomerID = 75,
                CustomerName = "Split Rail Beer & Ale",
                ContactName = "Art Braunschweiger",
                Address = new Location()
                {
                    Address = "P.O. Box 555",
                    City = "Lander",
                    PostalCode = "82520",
                    Country = "USA"
                }
            };
            customers.Add(item75);
            var item76 = new SeedCustomer()
            {
                CustomerID = 76,
                CustomerName = "Suprêmes délices",
                ContactName = "Pascale Cartrain",
                Address = new Location()
                {
                    Address = "Boulevard Tirou, 255",
                    City = "Charleroi",
                    PostalCode = "B-6000",
                    Country = "Belgium"
                }
            };
            customers.Add(item76);
            var item77 = new SeedCustomer()
            {
                CustomerID = 77,
                CustomerName = "The Big Cheese",
                ContactName = "Liz Nixon",
                Address = new Location()
                {
                    Address = "89 Jefferson Way Suite 2",
                    City = "Portland",
                    PostalCode = "97201",
                    Country = "USA"
                }
            };
            customers.Add(item77);
            var item78 = new SeedCustomer()
            {
                CustomerID = 78,
                CustomerName = "The Cracker Box",
                ContactName = "Liu Wong",
                Address = new Location()
                {
                    Address = "55 Grizzly Peak Rd.",
                    City = "Butte",
                    PostalCode = "59801",
                    Country = "USA"
                }
            };
            customers.Add(item78);
            var item79 = new SeedCustomer()
            {
                CustomerID = 79,
                CustomerName = "Toms Spezialitäten",
                ContactName = "Karin Josephs",
                Address = new Location()
                {
                    Address = "Luisenstr. 48",
                    City = "Münster",
                    PostalCode = "44087",
                    Country = "Germany"
                }
            };
            customers.Add(item79);
            var item80 = new SeedCustomer()
            {
                CustomerID = 80,
                CustomerName = "Tortuga Restaurante",
                ContactName = "Miguel Angel Paolino",
                Address = new Location()
                {
                    Address = "Avda. Azteca 123",
                    City = "México D.F.",
                    PostalCode = "05033",
                    Country = "Mexico"
                }
            };
            customers.Add(item80);
            var item81 = new SeedCustomer()
            {
                CustomerID = 81,
                CustomerName = "Tradição Hipermercados",
                ContactName = "Anabela Domingues",
                Address = new Location()
                {
                    Address = "Av. Inês de Castro, 414",
                    City = "São Paulo",
                    PostalCode = "05634-030",
                    Country = "Brazil"
                }
            };
            customers.Add(item81);
            var item82 = new SeedCustomer()
            {
                CustomerID = 82,
                CustomerName = "Trail's Head Gourmet Provisioners",
                ContactName = "Helvetius Nagy",
                Address = new Location()
                {
                    Address = "722 DaVinci Blvd.",
                    City = "Kirkland",
                    PostalCode = "98034",
                    Country = "USA"
                }
            };
            customers.Add(item82);
            var item83 = new SeedCustomer()
            {
                CustomerID = 83,
                CustomerName = "Vaffeljernet",
                ContactName = "Palle Ibsen",
                Address = new Location()
                {
                    Address = "Smagsløget 45",
                    City = "Århus",
                    PostalCode = "8200",
                    Country = "Denmark"
                }
            };
            customers.Add(item83);
            var item84 = new SeedCustomer()
            {
                CustomerID = 84,
                CustomerName = "Victuailles en stock",
                ContactName = "Mary Saveley",
                Address = new Location()
                {
                    Address = "2, rue du Commerce",
                    City = "Lyon",
                    PostalCode = "69004",
                    Country = "France"
                }
            };
            customers.Add(item84);
            var item85 = new SeedCustomer()
            {
                CustomerID = 85,
                CustomerName = "Vins et alcools Chevalier",
                ContactName = "Paul Henriot",
                Address = new Location()
                {
                    Address = "59 rue de l'Abbaye",
                    City = "Reims",
                    PostalCode = "51100",
                    Country = "France"
                }
            };
            customers.Add(item85);
            var item86 = new SeedCustomer()
            {
                CustomerID = 86,
                CustomerName = "Die Wandernde Kuh",
                ContactName = "Rita Müller",
                Address = new Location()
                {
                    Address = "Adenauerallee 900",
                    City = "Stuttgart",
                    PostalCode = "70563",
                    Country = "Germany"
                }
            };
            customers.Add(item86);
            var item87 = new SeedCustomer()
            {
                CustomerID = 87,
                CustomerName = "Wartian Herkku",
                ContactName = "Pirkko Koskitalo",
                Address = new Location()
                {
                    Address = "Torikatu 38",
                    City = "Oulu",
                    PostalCode = "90110",
                    Country = "Finland"
                }
            };
            customers.Add(item87);
            var item88 = new SeedCustomer()
            {
                CustomerID = 88,
                CustomerName = "Wellington Importadora",
                ContactName = "Paula Parente",
                Address = new Location()
                {
                    Address = "Rua do Mercado, 12",
                    City = "Resende",
                    PostalCode = "08737-363",
                    Country = "Brazil"
                }
            };
            customers.Add(item88);
            var item89 = new SeedCustomer()
            {
                CustomerID = 89,
                CustomerName = "White Clover Markets",
                ContactName = "Karl Jablonski",
                Address = new Location()
                {
                    Address = "305 - 14th Ave. S. Suite 3B",
                    City = "Seattle",
                    PostalCode = "98128",
                    Country = "USA"
                }
            };
            customers.Add(item89);
            var item90 = new SeedCustomer()
            {
                CustomerID = 90,
                CustomerName = "Wilman Kala",
                ContactName = "Matti Karttunen",
                Address = new Location()
                {
                    Address = "Keskuskatu 45",
                    City = "Helsinki",
                    PostalCode = "21240",
                    Country = "Finland"
                }
            };
            customers.Add(item90);
            var item91 = new SeedCustomer()
            {
                CustomerID = 91,
                CustomerName = "Wolski",
                ContactName = "Zbyszek",
                Address = new Location()
                {
                    Address = "ul. Filtrowa 68",
                    City = "Walla",
                    PostalCode = "01-012",
                    Country = "Poland"
                }
            };
            customers.Add(item91);
            return customers;
        }
        public static List<SeedCategory> GetCategories()
        {
            List<SeedCategory> categories = new List<SeedCategory>();
            var item1 = new SeedCategory()
            {
                CategoryID = 1,
                CategoryName = "Beverages",
                Description = "Soft drinks, coffees, teas, beers, and ales"
            };
            categories.Add(item1);
            var item2 = new SeedCategory()
            {
                CategoryID = 2,
                CategoryName = "Condiments",
                Description = "Sweet and savory sauces, relishes, spreads, and seasonings"
            };
            categories.Add(item2);
            var item3 = new SeedCategory()
            {
                CategoryID = 3,
                CategoryName = "Confections",
                Description = "Desserts, candies, and sweet breads"
            };
            categories.Add(item3);
            var item4 = new SeedCategory()
            {
                CategoryID = 4,
                CategoryName = "Dairy Products",
                Description = "Cheeses"
            };
            categories.Add(item4);
            var item5 = new SeedCategory()
            {
                CategoryID = 5,
                CategoryName = "Grains/Cereals",
                Description = "Breads, crackers, pasta, and cereal"
            };
            categories.Add(item5);
            var item6 = new SeedCategory()
            {
                CategoryID = 6,
                CategoryName = "Meat/Poultry",
                Description = "Prepared meats"
            };
            categories.Add(item6);
            var item7 = new SeedCategory()
            {
                CategoryID = 7,
                CategoryName = "Produce",
                Description = "Dried fruit and bean curd"
            };
            categories.Add(item7);
            var item8 = new SeedCategory()
            {
                CategoryID = 8,
                CategoryName = "Seafood",
                Description = "Seaweed and fish"
            };
            categories.Add(item8);
            return categories;
        }
        public static List<SeedEmployee> GetEmployees()
        {
            List<SeedEmployee> employees = new List<SeedEmployee>();
            var item1 = new SeedEmployee()
            {
                EmployeeID = 1,
                LastName = "Davolio",
                FirstName = "Nancy",
                BirthDate = DateTime.Parse("1968-12-08"),
                Photo = "EmpID1.pic",
                Notes = "Education includes a BA in psychology from Colorado State University. She also completed (The Art of the Cold Call). Nancy is a member of 'Toastmasters International'."
            };
            employees.Add(item1);
            var item2 = new SeedEmployee()
            {
                EmployeeID = 2,
                LastName = "Fuller",
                FirstName = "Andrew",
                BirthDate = DateTime.Parse("1952-02-19"),
                Photo = "EmpID2.pic",
                Notes = "Andrew received his BTS commercial and a Ph.D. in international marketing from the University of Dallas. He is fluent in French and Italian and reads German. He joined the company as a sales representative, was promoted to sales manager and was then named vice president of sales. Andrew is a member of the Sales Management Roundtable, the Seattle Chamber of Commerce, and the Pacific Rim Importers Association."
            };
            employees.Add(item2);
            var item3 = new SeedEmployee()
            {
                EmployeeID = 3,
                LastName = "Leverling",
                FirstName = "Janet",
                BirthDate = DateTime.Parse("1963-08-30"),
                Photo = "EmpID3.pic",
                Notes = "Janet has a BS degree in chemistry from Boston College). She has also completed a certificate program in food retailing management. Janet was hired as a sales associate and was promoted to sales representative."
            };
            employees.Add(item3);
            var item4 = new SeedEmployee()
            {
                EmployeeID = 4,
                LastName = "Peacock",
                FirstName = "Margaret",
                BirthDate = DateTime.Parse("1958-09-19"),
                Photo = "EmpID4.pic",
                Notes = "Margaret holds a BA in English literature from Concordia College and an MA from the American Institute of Culinary Arts. She was temporarily assigned to the London office before returning to her permanent post in Seattle."
            };
            employees.Add(item4);
            var item5 = new SeedEmployee()
            {
                EmployeeID = 5,
                LastName = "Buchanan",
                FirstName = "Steven",
                BirthDate = DateTime.Parse("1955-03-04"),
                Photo = "EmpID5.pic",
                Notes = "Steven Buchanan graduated from St. Andrews University, Scotland, with a BSC degree. Upon joining the company as a sales representative, he spent 6 months in an orientation program at the Seattle office and then returned to his permanent post in London, where he was promoted to sales manager. Mr. Buchanan has completed the courses 'Successful Telemarketing' and 'International Sales Management'. He is fluent in French."
            };
            employees.Add(item5);
            var item6 = new SeedEmployee()
            {
                EmployeeID = 6,
                LastName = "Suyama",
                FirstName = "Michael",
                BirthDate = DateTime.Parse("1963-07-02"),
                Photo = "EmpID6.pic",
                Notes = "Michael is a graduate of Sussex University (MA, economics) and the University of California at Los Angeles (MBA, marketing). He has also taken the courses 'Multi-Cultural Selling' and 'Time Management for the Sales Professional'. He is fluent in Japanese and can read and write French, Portuguese, and Spanish."
            };
            employees.Add(item6);
            var item7 = new SeedEmployee()
            {
                EmployeeID = 7,
                LastName = "King",
                FirstName = "Robert",
                BirthDate = DateTime.Parse("1960-05-29"),
                Photo = "EmpID7.pic",
                Notes = "Robert King served in the Peace Corps and traveled extensively before completing his degree in English at the University of Michigan and then joining the company. After completing a course entitled 'Selling in Europe', he was transferred to the London office."
            };
            employees.Add(item7);
            var item8 = new SeedEmployee()
            {
                EmployeeID = 8,
                LastName = "Callahan",
                FirstName = "Laura",
                BirthDate = DateTime.Parse("1958-01-09"),
                Photo = "EmpID8.pic",
                Notes = "Laura received a BA in psychology from the University of Washington. She has also completed a course in business French. She reads and writes French."
            };
            employees.Add(item8);
            var item9 = new SeedEmployee()
            {
                EmployeeID = 9,
                LastName = "Dodsworth",
                FirstName = "Anne",
                BirthDate = DateTime.Parse("1969-07-02"),
                Photo = "EmpID9.pic",
                Notes = "Anne has a BA degree in English from St. Lawrence College. She is fluent in French and German."
            };
            employees.Add(item9);
            var item10 = new SeedEmployee()
            {
                EmployeeID = 10,
                LastName = "West",
                FirstName = "Adam",
                BirthDate = DateTime.Parse("1928-09-19"),
                Photo = "EmpID10.pic",
                Notes = "An old chum."
            };
            employees.Add(item10);
            return employees;
        }

        public static List<SeedOrderDetail> GetOrderDetails()
        {
            List<SeedOrderDetail> orderDetails = new List<SeedOrderDetail>();
            var item1 = new SeedOrderDetail()
            {
                OrderDetailID = 1,
                OrderID = 10248,
                ProductID = 11,
                Quantity = 12
            };
            orderDetails.Add(item1);
            var item2 = new SeedOrderDetail()
            {
                OrderDetailID = 2,
                OrderID = 10248,
                ProductID = 42,
                Quantity = 10
            };
            orderDetails.Add(item2);
            var item3 = new SeedOrderDetail()
            {
                OrderDetailID = 3,
                OrderID = 10248,
                ProductID = 72,
                Quantity = 5
            };
            orderDetails.Add(item3);
            var item4 = new SeedOrderDetail()
            {
                OrderDetailID = 4,
                OrderID = 10249,
                ProductID = 14,
                Quantity = 9
            };
            orderDetails.Add(item4);
            var item5 = new SeedOrderDetail()
            {
                OrderDetailID = 5,
                OrderID = 10249,
                ProductID = 51,
                Quantity = 40
            };
            orderDetails.Add(item5);
            var item6 = new SeedOrderDetail()
            {
                OrderDetailID = 6,
                OrderID = 10250,
                ProductID = 41,
                Quantity = 10
            };
            orderDetails.Add(item6);
            var item7 = new SeedOrderDetail()
            {
                OrderDetailID = 7,
                OrderID = 10250,
                ProductID = 51,
                Quantity = 35
            };
            orderDetails.Add(item7);
            var item8 = new SeedOrderDetail()
            {
                OrderDetailID = 8,
                OrderID = 10250,
                ProductID = 65,
                Quantity = 15
            };
            orderDetails.Add(item8);
            var item9 = new SeedOrderDetail()
            {
                OrderDetailID = 9,
                OrderID = 10251,
                ProductID = 22,
                Quantity = 6
            };
            orderDetails.Add(item9);
            var item10 = new SeedOrderDetail()
            {
                OrderDetailID = 10,
                OrderID = 10251,
                ProductID = 57,
                Quantity = 15
            };
            orderDetails.Add(item10);
            var item11 = new SeedOrderDetail()
            {
                OrderDetailID = 11,
                OrderID = 10251,
                ProductID = 65,
                Quantity = 20
            };
            orderDetails.Add(item11);
            var item12 = new SeedOrderDetail()
            {
                OrderDetailID = 12,
                OrderID = 10252,
                ProductID = 20,
                Quantity = 40
            };
            orderDetails.Add(item12);
            var item13 = new SeedOrderDetail()
            {
                OrderDetailID = 13,
                OrderID = 10252,
                ProductID = 33,
                Quantity = 25
            };
            orderDetails.Add(item13);
            var item14 = new SeedOrderDetail()
            {
                OrderDetailID = 14,
                OrderID = 10252,
                ProductID = 60,
                Quantity = 40
            };
            orderDetails.Add(item14);
            var item15 = new SeedOrderDetail()
            {
                OrderDetailID = 15,
                OrderID = 10253,
                ProductID = 31,
                Quantity = 20
            };
            orderDetails.Add(item15);
            var item16 = new SeedOrderDetail()
            {
                OrderDetailID = 16,
                OrderID = 10253,
                ProductID = 39,
                Quantity = 42
            };
            orderDetails.Add(item16);
            var item17 = new SeedOrderDetail()
            {
                OrderDetailID = 17,
                OrderID = 10253,
                ProductID = 49,
                Quantity = 40
            };
            orderDetails.Add(item17);
            var item18 = new SeedOrderDetail()
            {
                OrderDetailID = 18,
                OrderID = 10254,
                ProductID = 24,
                Quantity = 15
            };
            orderDetails.Add(item18);
            var item19 = new SeedOrderDetail()
            {
                OrderDetailID = 19,
                OrderID = 10254,
                ProductID = 55,
                Quantity = 21
            };
            orderDetails.Add(item19);
            var item20 = new SeedOrderDetail()
            {
                OrderDetailID = 20,
                OrderID = 10254,
                ProductID = 74,
                Quantity = 21
            };
            orderDetails.Add(item20);
            var item21 = new SeedOrderDetail()
            {
                OrderDetailID = 21,
                OrderID = 10255,
                ProductID = 2,
                Quantity = 20
            };
            orderDetails.Add(item21);
            var item22 = new SeedOrderDetail()
            {
                OrderDetailID = 22,
                OrderID = 10255,
                ProductID = 16,
                Quantity = 35
            };
            orderDetails.Add(item22);
            var item23 = new SeedOrderDetail()
            {
                OrderDetailID = 23,
                OrderID = 10255,
                ProductID = 36,
                Quantity = 25
            };
            orderDetails.Add(item23);
            var item24 = new SeedOrderDetail()
            {
                OrderDetailID = 24,
                OrderID = 10255,
                ProductID = 59,
                Quantity = 30
            };
            orderDetails.Add(item24);
            var item25 = new SeedOrderDetail()
            {
                OrderDetailID = 25,
                OrderID = 10256,
                ProductID = 53,
                Quantity = 15
            };
            orderDetails.Add(item25);
            var item26 = new SeedOrderDetail()
            {
                OrderDetailID = 26,
                OrderID = 10256,
                ProductID = 77,
                Quantity = 12
            };
            orderDetails.Add(item26);
            var item27 = new SeedOrderDetail()
            {
                OrderDetailID = 27,
                OrderID = 10257,
                ProductID = 27,
                Quantity = 25
            };
            orderDetails.Add(item27);
            var item28 = new SeedOrderDetail()
            {
                OrderDetailID = 28,
                OrderID = 10257,
                ProductID = 39,
                Quantity = 6
            };
            orderDetails.Add(item28);
            var item29 = new SeedOrderDetail()
            {
                OrderDetailID = 29,
                OrderID = 10257,
                ProductID = 77,
                Quantity = 15
            };
            orderDetails.Add(item29);
            var item30 = new SeedOrderDetail()
            {
                OrderDetailID = 30,
                OrderID = 10258,
                ProductID = 2,
                Quantity = 50
            };
            orderDetails.Add(item30);
            var item31 = new SeedOrderDetail()
            {
                OrderDetailID = 31,
                OrderID = 10258,
                ProductID = 5,
                Quantity = 65
            };
            orderDetails.Add(item31);
            var item32 = new SeedOrderDetail()
            {
                OrderDetailID = 32,
                OrderID = 10258,
                ProductID = 32,
                Quantity = 6
            };
            orderDetails.Add(item32);
            var item33 = new SeedOrderDetail()
            {
                OrderDetailID = 33,
                OrderID = 10259,
                ProductID = 21,
                Quantity = 10
            };
            orderDetails.Add(item33);
            var item34 = new SeedOrderDetail()
            {
                OrderDetailID = 34,
                OrderID = 10259,
                ProductID = 37,
                Quantity = 1
            };
            orderDetails.Add(item34);
            var item35 = new SeedOrderDetail()
            {
                OrderDetailID = 35,
                OrderID = 10260,
                ProductID = 41,
                Quantity = 16
            };
            orderDetails.Add(item35);
            var item36 = new SeedOrderDetail()
            {
                OrderDetailID = 36,
                OrderID = 10260,
                ProductID = 57,
                Quantity = 50
            };
            orderDetails.Add(item36);
            var item37 = new SeedOrderDetail()
            {
                OrderDetailID = 37,
                OrderID = 10260,
                ProductID = 62,
                Quantity = 15
            };
            orderDetails.Add(item37);
            var item38 = new SeedOrderDetail()
            {
                OrderDetailID = 38,
                OrderID = 10260,
                ProductID = 70,
                Quantity = 21
            };
            orderDetails.Add(item38);
            var item39 = new SeedOrderDetail()
            {
                OrderDetailID = 39,
                OrderID = 10261,
                ProductID = 21,
                Quantity = 20
            };
            orderDetails.Add(item39);
            var item40 = new SeedOrderDetail()
            {
                OrderDetailID = 40,
                OrderID = 10261,
                ProductID = 35,
                Quantity = 20
            };
            orderDetails.Add(item40);
            var item41 = new SeedOrderDetail()
            {
                OrderDetailID = 41,
                OrderID = 10262,
                ProductID = 5,
                Quantity = 12
            };
            orderDetails.Add(item41);
            var item42 = new SeedOrderDetail()
            {
                OrderDetailID = 42,
                OrderID = 10262,
                ProductID = 7,
                Quantity = 15
            };
            orderDetails.Add(item42);
            var item43 = new SeedOrderDetail()
            {
                OrderDetailID = 43,
                OrderID = 10262,
                ProductID = 56,
                Quantity = 2
            };
            orderDetails.Add(item43);
            var item44 = new SeedOrderDetail()
            {
                OrderDetailID = 44,
                OrderID = 10263,
                ProductID = 16,
                Quantity = 60
            };
            orderDetails.Add(item44);
            var item45 = new SeedOrderDetail()
            {
                OrderDetailID = 45,
                OrderID = 10263,
                ProductID = 24,
                Quantity = 28
            };
            orderDetails.Add(item45);
            var item46 = new SeedOrderDetail()
            {
                OrderDetailID = 46,
                OrderID = 10263,
                ProductID = 30,
                Quantity = 60
            };
            orderDetails.Add(item46);
            var item47 = new SeedOrderDetail()
            {
                OrderDetailID = 47,
                OrderID = 10263,
                ProductID = 74,
                Quantity = 36
            };
            orderDetails.Add(item47);
            var item48 = new SeedOrderDetail()
            {
                OrderDetailID = 48,
                OrderID = 10264,
                ProductID = 2,
                Quantity = 35
            };
            orderDetails.Add(item48);
            var item49 = new SeedOrderDetail()
            {
                OrderDetailID = 49,
                OrderID = 10264,
                ProductID = 41,
                Quantity = 25
            };
            orderDetails.Add(item49);
            var item50 = new SeedOrderDetail()
            {
                OrderDetailID = 50,
                OrderID = 10265,
                ProductID = 17,
                Quantity = 30
            };
            orderDetails.Add(item50);
            var item51 = new SeedOrderDetail()
            {
                OrderDetailID = 51,
                OrderID = 10265,
                ProductID = 70,
                Quantity = 20
            };
            orderDetails.Add(item51);
            var item52 = new SeedOrderDetail()
            {
                OrderDetailID = 52,
                OrderID = 10266,
                ProductID = 12,
                Quantity = 12
            };
            orderDetails.Add(item52);
            var item53 = new SeedOrderDetail()
            {
                OrderDetailID = 53,
                OrderID = 10267,
                ProductID = 40,
                Quantity = 50
            };
            orderDetails.Add(item53);
            var item54 = new SeedOrderDetail()
            {
                OrderDetailID = 54,
                OrderID = 10267,
                ProductID = 59,
                Quantity = 70
            };
            orderDetails.Add(item54);
            var item55 = new SeedOrderDetail()
            {
                OrderDetailID = 55,
                OrderID = 10267,
                ProductID = 76,
                Quantity = 15
            };
            orderDetails.Add(item55);
            var item56 = new SeedOrderDetail()
            {
                OrderDetailID = 56,
                OrderID = 10268,
                ProductID = 29,
                Quantity = 10
            };
            orderDetails.Add(item56);
            var item57 = new SeedOrderDetail()
            {
                OrderDetailID = 57,
                OrderID = 10268,
                ProductID = 72,
                Quantity = 4
            };
            orderDetails.Add(item57);
            var item58 = new SeedOrderDetail()
            {
                OrderDetailID = 58,
                OrderID = 10269,
                ProductID = 33,
                Quantity = 60
            };
            orderDetails.Add(item58);
            var item59 = new SeedOrderDetail()
            {
                OrderDetailID = 59,
                OrderID = 10269,
                ProductID = 72,
                Quantity = 20
            };
            orderDetails.Add(item59);
            var item60 = new SeedOrderDetail()
            {
                OrderDetailID = 60,
                OrderID = 10270,
                ProductID = 36,
                Quantity = 30
            };
            orderDetails.Add(item60);
            var item61 = new SeedOrderDetail()
            {
                OrderDetailID = 61,
                OrderID = 10270,
                ProductID = 43,
                Quantity = 25
            };
            orderDetails.Add(item61);
            var item62 = new SeedOrderDetail()
            {
                OrderDetailID = 62,
                OrderID = 10271,
                ProductID = 33,
                Quantity = 24
            };
            orderDetails.Add(item62);
            var item63 = new SeedOrderDetail()
            {
                OrderDetailID = 63,
                OrderID = 10272,
                ProductID = 20,
                Quantity = 6
            };
            orderDetails.Add(item63);
            var item64 = new SeedOrderDetail()
            {
                OrderDetailID = 64,
                OrderID = 10272,
                ProductID = 31,
                Quantity = 40
            };
            orderDetails.Add(item64);
            var item65 = new SeedOrderDetail()
            {
                OrderDetailID = 65,
                OrderID = 10272,
                ProductID = 72,
                Quantity = 24
            };
            orderDetails.Add(item65);
            var item66 = new SeedOrderDetail()
            {
                OrderDetailID = 66,
                OrderID = 10273,
                ProductID = 10,
                Quantity = 24
            };
            orderDetails.Add(item66);
            var item67 = new SeedOrderDetail()
            {
                OrderDetailID = 67,
                OrderID = 10273,
                ProductID = 31,
                Quantity = 15
            };
            orderDetails.Add(item67);
            var item68 = new SeedOrderDetail()
            {
                OrderDetailID = 68,
                OrderID = 10273,
                ProductID = 33,
                Quantity = 20
            };
            orderDetails.Add(item68);
            var item69 = new SeedOrderDetail()
            {
                OrderDetailID = 69,
                OrderID = 10273,
                ProductID = 40,
                Quantity = 60
            };
            orderDetails.Add(item69);
            var item70 = new SeedOrderDetail()
            {
                OrderDetailID = 70,
                OrderID = 10273,
                ProductID = 76,
                Quantity = 33
            };
            orderDetails.Add(item70);
            var item71 = new SeedOrderDetail()
            {
                OrderDetailID = 71,
                OrderID = 10274,
                ProductID = 71,
                Quantity = 20
            };
            orderDetails.Add(item71);
            var item72 = new SeedOrderDetail()
            {
                OrderDetailID = 72,
                OrderID = 10274,
                ProductID = 72,
                Quantity = 7
            };
            orderDetails.Add(item72);
            var item73 = new SeedOrderDetail()
            {
                OrderDetailID = 73,
                OrderID = 10275,
                ProductID = 24,
                Quantity = 12
            };
            orderDetails.Add(item73);
            var item74 = new SeedOrderDetail()
            {
                OrderDetailID = 74,
                OrderID = 10275,
                ProductID = 59,
                Quantity = 6
            };
            orderDetails.Add(item74);
            var item75 = new SeedOrderDetail()
            {
                OrderDetailID = 75,
                OrderID = 10276,
                ProductID = 10,
                Quantity = 15
            };
            orderDetails.Add(item75);
            var item76 = new SeedOrderDetail()
            {
                OrderDetailID = 76,
                OrderID = 10276,
                ProductID = 13,
                Quantity = 10
            };
            orderDetails.Add(item76);
            var item77 = new SeedOrderDetail()
            {
                OrderDetailID = 77,
                OrderID = 10277,
                ProductID = 28,
                Quantity = 20
            };
            orderDetails.Add(item77);
            var item78 = new SeedOrderDetail()
            {
                OrderDetailID = 78,
                OrderID = 10277,
                ProductID = 62,
                Quantity = 12
            };
            orderDetails.Add(item78);
            var item79 = new SeedOrderDetail()
            {
                OrderDetailID = 79,
                OrderID = 10278,
                ProductID = 44,
                Quantity = 16
            };
            orderDetails.Add(item79);
            var item80 = new SeedOrderDetail()
            {
                OrderDetailID = 80,
                OrderID = 10278,
                ProductID = 59,
                Quantity = 15
            };
            orderDetails.Add(item80);
            var item81 = new SeedOrderDetail()
            {
                OrderDetailID = 81,
                OrderID = 10278,
                ProductID = 63,
                Quantity = 8
            };
            orderDetails.Add(item81);
            var item82 = new SeedOrderDetail()
            {
                OrderDetailID = 82,
                OrderID = 10278,
                ProductID = 73,
                Quantity = 25
            };
            orderDetails.Add(item82);
            var item83 = new SeedOrderDetail()
            {
                OrderDetailID = 83,
                OrderID = 10279,
                ProductID = 17,
                Quantity = 15
            };
            orderDetails.Add(item83);
            var item84 = new SeedOrderDetail()
            {
                OrderDetailID = 84,
                OrderID = 10280,
                ProductID = 24,
                Quantity = 12
            };
            orderDetails.Add(item84);
            var item85 = new SeedOrderDetail()
            {
                OrderDetailID = 85,
                OrderID = 10280,
                ProductID = 55,
                Quantity = 20
            };
            orderDetails.Add(item85);
            var item86 = new SeedOrderDetail()
            {
                OrderDetailID = 86,
                OrderID = 10280,
                ProductID = 75,
                Quantity = 30
            };
            orderDetails.Add(item86);
            var item87 = new SeedOrderDetail()
            {
                OrderDetailID = 87,
                OrderID = 10281,
                ProductID = 19,
                Quantity = 1
            };
            orderDetails.Add(item87);
            var item88 = new SeedOrderDetail()
            {
                OrderDetailID = 88,
                OrderID = 10281,
                ProductID = 24,
                Quantity = 6
            };
            orderDetails.Add(item88);
            var item89 = new SeedOrderDetail()
            {
                OrderDetailID = 89,
                OrderID = 10281,
                ProductID = 35,
                Quantity = 4
            };
            orderDetails.Add(item89);
            var item90 = new SeedOrderDetail()
            {
                OrderDetailID = 90,
                OrderID = 10282,
                ProductID = 30,
                Quantity = 6
            };
            orderDetails.Add(item90);
            var item91 = new SeedOrderDetail()
            {
                OrderDetailID = 91,
                OrderID = 10282,
                ProductID = 57,
                Quantity = 2
            };
            orderDetails.Add(item91);
            var item92 = new SeedOrderDetail()
            {
                OrderDetailID = 92,
                OrderID = 10283,
                ProductID = 15,
                Quantity = 20
            };
            orderDetails.Add(item92);
            var item93 = new SeedOrderDetail()
            {
                OrderDetailID = 93,
                OrderID = 10283,
                ProductID = 19,
                Quantity = 18
            };
            orderDetails.Add(item93);
            var item94 = new SeedOrderDetail()
            {
                OrderDetailID = 94,
                OrderID = 10283,
                ProductID = 60,
                Quantity = 35
            };
            orderDetails.Add(item94);
            var item95 = new SeedOrderDetail()
            {
                OrderDetailID = 95,
                OrderID = 10283,
                ProductID = 72,
                Quantity = 3
            };
            orderDetails.Add(item95);
            var item96 = new SeedOrderDetail()
            {
                OrderDetailID = 96,
                OrderID = 10284,
                ProductID = 27,
                Quantity = 15
            };
            orderDetails.Add(item96);
            var item97 = new SeedOrderDetail()
            {
                OrderDetailID = 97,
                OrderID = 10284,
                ProductID = 44,
                Quantity = 21
            };
            orderDetails.Add(item97);
            var item98 = new SeedOrderDetail()
            {
                OrderDetailID = 98,
                OrderID = 10284,
                ProductID = 60,
                Quantity = 20
            };
            orderDetails.Add(item98);
            var item99 = new SeedOrderDetail()
            {
                OrderDetailID = 99,
                OrderID = 10284,
                ProductID = 67,
                Quantity = 5
            };
            orderDetails.Add(item99);
            var item100 = new SeedOrderDetail()
            {
                OrderDetailID = 100,
                OrderID = 10285,
                ProductID = 1,
                Quantity = 45
            };
            orderDetails.Add(item100);
            var item101 = new SeedOrderDetail()
            {
                OrderDetailID = 101,
                OrderID = 10285,
                ProductID = 40,
                Quantity = 40
            };
            orderDetails.Add(item101);
            var item102 = new SeedOrderDetail()
            {
                OrderDetailID = 102,
                OrderID = 10285,
                ProductID = 53,
                Quantity = 36
            };
            orderDetails.Add(item102);
            var item103 = new SeedOrderDetail()
            {
                OrderDetailID = 103,
                OrderID = 10286,
                ProductID = 35,
                Quantity = 100
            };
            orderDetails.Add(item103);
            var item104 = new SeedOrderDetail()
            {
                OrderDetailID = 104,
                OrderID = 10286,
                ProductID = 62,
                Quantity = 40
            };
            orderDetails.Add(item104);
            var item105 = new SeedOrderDetail()
            {
                OrderDetailID = 105,
                OrderID = 10287,
                ProductID = 16,
                Quantity = 40
            };
            orderDetails.Add(item105);
            var item106 = new SeedOrderDetail()
            {
                OrderDetailID = 106,
                OrderID = 10287,
                ProductID = 34,
                Quantity = 20
            };
            orderDetails.Add(item106);
            var item107 = new SeedOrderDetail()
            {
                OrderDetailID = 107,
                OrderID = 10287,
                ProductID = 46,
                Quantity = 15
            };
            orderDetails.Add(item107);
            var item108 = new SeedOrderDetail()
            {
                OrderDetailID = 108,
                OrderID = 10288,
                ProductID = 54,
                Quantity = 10
            };
            orderDetails.Add(item108);
            var item109 = new SeedOrderDetail()
            {
                OrderDetailID = 109,
                OrderID = 10288,
                ProductID = 68,
                Quantity = 3
            };
            orderDetails.Add(item109);
            var item110 = new SeedOrderDetail()
            {
                OrderDetailID = 110,
                OrderID = 10289,
                ProductID = 3,
                Quantity = 30
            };
            orderDetails.Add(item110);
            var item111 = new SeedOrderDetail()
            {
                OrderDetailID = 111,
                OrderID = 10289,
                ProductID = 64,
                Quantity = 9
            };
            orderDetails.Add(item111);
            var item112 = new SeedOrderDetail()
            {
                OrderDetailID = 112,
                OrderID = 10290,
                ProductID = 5,
                Quantity = 20
            };
            orderDetails.Add(item112);
            var item113 = new SeedOrderDetail()
            {
                OrderDetailID = 113,
                OrderID = 10290,
                ProductID = 29,
                Quantity = 15
            };
            orderDetails.Add(item113);
            var item114 = new SeedOrderDetail()
            {
                OrderDetailID = 114,
                OrderID = 10290,
                ProductID = 49,
                Quantity = 15
            };
            orderDetails.Add(item114);
            var item115 = new SeedOrderDetail()
            {
                OrderDetailID = 115,
                OrderID = 10290,
                ProductID = 77,
                Quantity = 10
            };
            orderDetails.Add(item115);
            var item116 = new SeedOrderDetail()
            {
                OrderDetailID = 116,
                OrderID = 10291,
                ProductID = 13,
                Quantity = 20
            };
            orderDetails.Add(item116);
            var item117 = new SeedOrderDetail()
            {
                OrderDetailID = 117,
                OrderID = 10291,
                ProductID = 44,
                Quantity = 24
            };
            orderDetails.Add(item117);
            var item118 = new SeedOrderDetail()
            {
                OrderDetailID = 118,
                OrderID = 10291,
                ProductID = 51,
                Quantity = 2
            };
            orderDetails.Add(item118);
            var item119 = new SeedOrderDetail()
            {
                OrderDetailID = 119,
                OrderID = 10292,
                ProductID = 20,
                Quantity = 20
            };
            orderDetails.Add(item119);
            var item120 = new SeedOrderDetail()
            {
                OrderDetailID = 120,
                OrderID = 10293,
                ProductID = 18,
                Quantity = 12
            };
            orderDetails.Add(item120);
            var item121 = new SeedOrderDetail()
            {
                OrderDetailID = 121,
                OrderID = 10293,
                ProductID = 24,
                Quantity = 10
            };
            orderDetails.Add(item121);
            var item122 = new SeedOrderDetail()
            {
                OrderDetailID = 122,
                OrderID = 10293,
                ProductID = 63,
                Quantity = 5
            };
            orderDetails.Add(item122);
            var item123 = new SeedOrderDetail()
            {
                OrderDetailID = 123,
                OrderID = 10293,
                ProductID = 75,
                Quantity = 6
            };
            orderDetails.Add(item123);
            var item124 = new SeedOrderDetail()
            {
                OrderDetailID = 124,
                OrderID = 10294,
                ProductID = 1,
                Quantity = 18
            };
            orderDetails.Add(item124);
            var item125 = new SeedOrderDetail()
            {
                OrderDetailID = 125,
                OrderID = 10294,
                ProductID = 17,
                Quantity = 15
            };
            orderDetails.Add(item125);
            var item126 = new SeedOrderDetail()
            {
                OrderDetailID = 126,
                OrderID = 10294,
                ProductID = 43,
                Quantity = 15
            };
            orderDetails.Add(item126);
            var item127 = new SeedOrderDetail()
            {
                OrderDetailID = 127,
                OrderID = 10294,
                ProductID = 60,
                Quantity = 21
            };
            orderDetails.Add(item127);
            var item128 = new SeedOrderDetail()
            {
                OrderDetailID = 128,
                OrderID = 10294,
                ProductID = 75,
                Quantity = 6
            };
            orderDetails.Add(item128);
            var item129 = new SeedOrderDetail()
            {
                OrderDetailID = 129,
                OrderID = 10295,
                ProductID = 56,
                Quantity = 4
            };
            orderDetails.Add(item129);
            var item130 = new SeedOrderDetail()
            {
                OrderDetailID = 130,
                OrderID = 10296,
                ProductID = 11,
                Quantity = 12
            };
            orderDetails.Add(item130);
            var item131 = new SeedOrderDetail()
            {
                OrderDetailID = 131,
                OrderID = 10296,
                ProductID = 16,
                Quantity = 30
            };
            orderDetails.Add(item131);
            var item132 = new SeedOrderDetail()
            {
                OrderDetailID = 132,
                OrderID = 10296,
                ProductID = 69,
                Quantity = 15
            };
            orderDetails.Add(item132);
            var item133 = new SeedOrderDetail()
            {
                OrderDetailID = 133,
                OrderID = 10297,
                ProductID = 39,
                Quantity = 60
            };
            orderDetails.Add(item133);
            var item134 = new SeedOrderDetail()
            {
                OrderDetailID = 134,
                OrderID = 10297,
                ProductID = 72,
                Quantity = 20
            };
            orderDetails.Add(item134);
            var item135 = new SeedOrderDetail()
            {
                OrderDetailID = 135,
                OrderID = 10298,
                ProductID = 2,
                Quantity = 40
            };
            orderDetails.Add(item135);
            var item136 = new SeedOrderDetail()
            {
                OrderDetailID = 136,
                OrderID = 10298,
                ProductID = 36,
                Quantity = 40
            };
            orderDetails.Add(item136);
            var item137 = new SeedOrderDetail()
            {
                OrderDetailID = 137,
                OrderID = 10298,
                ProductID = 59,
                Quantity = 30
            };
            orderDetails.Add(item137);
            var item138 = new SeedOrderDetail()
            {
                OrderDetailID = 138,
                OrderID = 10298,
                ProductID = 62,
                Quantity = 15
            };
            orderDetails.Add(item138);
            var item139 = new SeedOrderDetail()
            {
                OrderDetailID = 139,
                OrderID = 10299,
                ProductID = 19,
                Quantity = 15
            };
            orderDetails.Add(item139);
            var item140 = new SeedOrderDetail()
            {
                OrderDetailID = 140,
                OrderID = 10299,
                ProductID = 70,
                Quantity = 20
            };
            orderDetails.Add(item140);
            var item141 = new SeedOrderDetail()
            {
                OrderDetailID = 141,
                OrderID = 10300,
                ProductID = 66,
                Quantity = 30
            };
            orderDetails.Add(item141);
            var item142 = new SeedOrderDetail()
            {
                OrderDetailID = 142,
                OrderID = 10300,
                ProductID = 68,
                Quantity = 20
            };
            orderDetails.Add(item142);
            var item143 = new SeedOrderDetail()
            {
                OrderDetailID = 143,
                OrderID = 10301,
                ProductID = 40,
                Quantity = 10
            };
            orderDetails.Add(item143);
            var item144 = new SeedOrderDetail()
            {
                OrderDetailID = 144,
                OrderID = 10301,
                ProductID = 56,
                Quantity = 20
            };
            orderDetails.Add(item144);
            var item145 = new SeedOrderDetail()
            {
                OrderDetailID = 145,
                OrderID = 10302,
                ProductID = 17,
                Quantity = 40
            };
            orderDetails.Add(item145);
            var item146 = new SeedOrderDetail()
            {
                OrderDetailID = 146,
                OrderID = 10302,
                ProductID = 28,
                Quantity = 28
            };
            orderDetails.Add(item146);
            var item147 = new SeedOrderDetail()
            {
                OrderDetailID = 147,
                OrderID = 10302,
                ProductID = 43,
                Quantity = 12
            };
            orderDetails.Add(item147);
            var item148 = new SeedOrderDetail()
            {
                OrderDetailID = 148,
                OrderID = 10303,
                ProductID = 40,
                Quantity = 40
            };
            orderDetails.Add(item148);
            var item149 = new SeedOrderDetail()
            {
                OrderDetailID = 149,
                OrderID = 10303,
                ProductID = 65,
                Quantity = 30
            };
            orderDetails.Add(item149);
            var item150 = new SeedOrderDetail()
            {
                OrderDetailID = 150,
                OrderID = 10303,
                ProductID = 68,
                Quantity = 15
            };
            orderDetails.Add(item150);
            var item151 = new SeedOrderDetail()
            {
                OrderDetailID = 151,
                OrderID = 10304,
                ProductID = 49,
                Quantity = 30
            };
            orderDetails.Add(item151);
            var item152 = new SeedOrderDetail()
            {
                OrderDetailID = 152,
                OrderID = 10304,
                ProductID = 59,
                Quantity = 10
            };
            orderDetails.Add(item152);
            var item153 = new SeedOrderDetail()
            {
                OrderDetailID = 153,
                OrderID = 10304,
                ProductID = 71,
                Quantity = 2
            };
            orderDetails.Add(item153);
            var item154 = new SeedOrderDetail()
            {
                OrderDetailID = 154,
                OrderID = 10305,
                ProductID = 18,
                Quantity = 25
            };
            orderDetails.Add(item154);
            var item155 = new SeedOrderDetail()
            {
                OrderDetailID = 155,
                OrderID = 10305,
                ProductID = 29,
                Quantity = 25
            };
            orderDetails.Add(item155);
            var item156 = new SeedOrderDetail()
            {
                OrderDetailID = 156,
                OrderID = 10305,
                ProductID = 39,
                Quantity = 30
            };
            orderDetails.Add(item156);
            var item157 = new SeedOrderDetail()
            {
                OrderDetailID = 157,
                OrderID = 10306,
                ProductID = 30,
                Quantity = 10
            };
            orderDetails.Add(item157);
            var item158 = new SeedOrderDetail()
            {
                OrderDetailID = 158,
                OrderID = 10306,
                ProductID = 53,
                Quantity = 10
            };
            orderDetails.Add(item158);
            var item159 = new SeedOrderDetail()
            {
                OrderDetailID = 159,
                OrderID = 10306,
                ProductID = 54,
                Quantity = 5
            };
            orderDetails.Add(item159);
            var item160 = new SeedOrderDetail()
            {
                OrderDetailID = 160,
                OrderID = 10307,
                ProductID = 62,
                Quantity = 10
            };
            orderDetails.Add(item160);
            var item161 = new SeedOrderDetail()
            {
                OrderDetailID = 161,
                OrderID = 10307,
                ProductID = 68,
                Quantity = 3
            };
            orderDetails.Add(item161);
            var item162 = new SeedOrderDetail()
            {
                OrderDetailID = 162,
                OrderID = 10308,
                ProductID = 69,
                Quantity = 1
            };
            orderDetails.Add(item162);
            var item163 = new SeedOrderDetail()
            {
                OrderDetailID = 163,
                OrderID = 10308,
                ProductID = 70,
                Quantity = 5
            };
            orderDetails.Add(item163);
            var item164 = new SeedOrderDetail()
            {
                OrderDetailID = 164,
                OrderID = 10309,
                ProductID = 4,
                Quantity = 20
            };
            orderDetails.Add(item164);
            var item165 = new SeedOrderDetail()
            {
                OrderDetailID = 165,
                OrderID = 10309,
                ProductID = 6,
                Quantity = 30
            };
            orderDetails.Add(item165);
            var item166 = new SeedOrderDetail()
            {
                OrderDetailID = 166,
                OrderID = 10309,
                ProductID = 42,
                Quantity = 2
            };
            orderDetails.Add(item166);
            var item167 = new SeedOrderDetail()
            {
                OrderDetailID = 167,
                OrderID = 10309,
                ProductID = 43,
                Quantity = 20
            };
            orderDetails.Add(item167);
            var item168 = new SeedOrderDetail()
            {
                OrderDetailID = 168,
                OrderID = 10309,
                ProductID = 71,
                Quantity = 3
            };
            orderDetails.Add(item168);
            var item169 = new SeedOrderDetail()
            {
                OrderDetailID = 169,
                OrderID = 10310,
                ProductID = 16,
                Quantity = 10
            };
            orderDetails.Add(item169);
            var item170 = new SeedOrderDetail()
            {
                OrderDetailID = 170,
                OrderID = 10310,
                ProductID = 62,
                Quantity = 5
            };
            orderDetails.Add(item170);
            var item171 = new SeedOrderDetail()
            {
                OrderDetailID = 171,
                OrderID = 10311,
                ProductID = 42,
                Quantity = 6
            };
            orderDetails.Add(item171);
            var item172 = new SeedOrderDetail()
            {
                OrderDetailID = 172,
                OrderID = 10311,
                ProductID = 69,
                Quantity = 7
            };
            orderDetails.Add(item172);
            var item173 = new SeedOrderDetail()
            {
                OrderDetailID = 173,
                OrderID = 10312,
                ProductID = 28,
                Quantity = 4
            };
            orderDetails.Add(item173);
            var item174 = new SeedOrderDetail()
            {
                OrderDetailID = 174,
                OrderID = 10312,
                ProductID = 43,
                Quantity = 24
            };
            orderDetails.Add(item174);
            var item175 = new SeedOrderDetail()
            {
                OrderDetailID = 175,
                OrderID = 10312,
                ProductID = 53,
                Quantity = 20
            };
            orderDetails.Add(item175);
            var item176 = new SeedOrderDetail()
            {
                OrderDetailID = 176,
                OrderID = 10312,
                ProductID = 75,
                Quantity = 10
            };
            orderDetails.Add(item176);
            var item177 = new SeedOrderDetail()
            {
                OrderDetailID = 177,
                OrderID = 10313,
                ProductID = 36,
                Quantity = 12
            };
            orderDetails.Add(item177);
            var item178 = new SeedOrderDetail()
            {
                OrderDetailID = 178,
                OrderID = 10314,
                ProductID = 32,
                Quantity = 40
            };
            orderDetails.Add(item178);
            var item179 = new SeedOrderDetail()
            {
                OrderDetailID = 179,
                OrderID = 10314,
                ProductID = 58,
                Quantity = 30
            };
            orderDetails.Add(item179);
            var item180 = new SeedOrderDetail()
            {
                OrderDetailID = 180,
                OrderID = 10314,
                ProductID = 62,
                Quantity = 25
            };
            orderDetails.Add(item180);
            var item181 = new SeedOrderDetail()
            {
                OrderDetailID = 181,
                OrderID = 10315,
                ProductID = 34,
                Quantity = 14
            };
            orderDetails.Add(item181);
            var item182 = new SeedOrderDetail()
            {
                OrderDetailID = 182,
                OrderID = 10315,
                ProductID = 70,
                Quantity = 30
            };
            orderDetails.Add(item182);
            var item183 = new SeedOrderDetail()
            {
                OrderDetailID = 183,
                OrderID = 10316,
                ProductID = 41,
                Quantity = 10
            };
            orderDetails.Add(item183);
            var item184 = new SeedOrderDetail()
            {
                OrderDetailID = 184,
                OrderID = 10316,
                ProductID = 62,
                Quantity = 70
            };
            orderDetails.Add(item184);
            var item185 = new SeedOrderDetail()
            {
                OrderDetailID = 185,
                OrderID = 10317,
                ProductID = 1,
                Quantity = 20
            };
            orderDetails.Add(item185);
            var item186 = new SeedOrderDetail()
            {
                OrderDetailID = 186,
                OrderID = 10318,
                ProductID = 41,
                Quantity = 20
            };
            orderDetails.Add(item186);
            var item187 = new SeedOrderDetail()
            {
                OrderDetailID = 187,
                OrderID = 10318,
                ProductID = 76,
                Quantity = 6
            };
            orderDetails.Add(item187);
            var item188 = new SeedOrderDetail()
            {
                OrderDetailID = 188,
                OrderID = 10319,
                ProductID = 17,
                Quantity = 8
            };
            orderDetails.Add(item188);
            var item189 = new SeedOrderDetail()
            {
                OrderDetailID = 189,
                OrderID = 10319,
                ProductID = 28,
                Quantity = 14
            };
            orderDetails.Add(item189);
            var item190 = new SeedOrderDetail()
            {
                OrderDetailID = 190,
                OrderID = 10319,
                ProductID = 76,
                Quantity = 30
            };
            orderDetails.Add(item190);
            var item191 = new SeedOrderDetail()
            {
                OrderDetailID = 191,
                OrderID = 10320,
                ProductID = 71,
                Quantity = 30
            };
            orderDetails.Add(item191);
            var item192 = new SeedOrderDetail()
            {
                OrderDetailID = 192,
                OrderID = 10321,
                ProductID = 35,
                Quantity = 10
            };
            orderDetails.Add(item192);
            var item193 = new SeedOrderDetail()
            {
                OrderDetailID = 193,
                OrderID = 10322,
                ProductID = 52,
                Quantity = 20
            };
            orderDetails.Add(item193);
            var item194 = new SeedOrderDetail()
            {
                OrderDetailID = 194,
                OrderID = 10323,
                ProductID = 15,
                Quantity = 5
            };
            orderDetails.Add(item194);
            var item195 = new SeedOrderDetail()
            {
                OrderDetailID = 195,
                OrderID = 10323,
                ProductID = 25,
                Quantity = 4
            };
            orderDetails.Add(item195);
            var item196 = new SeedOrderDetail()
            {
                OrderDetailID = 196,
                OrderID = 10323,
                ProductID = 39,
                Quantity = 4
            };
            orderDetails.Add(item196);
            var item197 = new SeedOrderDetail()
            {
                OrderDetailID = 197,
                OrderID = 10324,
                ProductID = 16,
                Quantity = 21
            };
            orderDetails.Add(item197);
            var item198 = new SeedOrderDetail()
            {
                OrderDetailID = 198,
                OrderID = 10324,
                ProductID = 35,
                Quantity = 70
            };
            orderDetails.Add(item198);
            var item199 = new SeedOrderDetail()
            {
                OrderDetailID = 199,
                OrderID = 10324,
                ProductID = 46,
                Quantity = 30
            };
            orderDetails.Add(item199);
            var item200 = new SeedOrderDetail()
            {
                OrderDetailID = 200,
                OrderID = 10324,
                ProductID = 59,
                Quantity = 40
            };
            orderDetails.Add(item200);
            var item201 = new SeedOrderDetail()
            {
                OrderDetailID = 201,
                OrderID = 10324,
                ProductID = 63,
                Quantity = 80
            };
            orderDetails.Add(item201);
            var item202 = new SeedOrderDetail()
            {
                OrderDetailID = 202,
                OrderID = 10325,
                ProductID = 6,
                Quantity = 6
            };
            orderDetails.Add(item202);
            var item203 = new SeedOrderDetail()
            {
                OrderDetailID = 203,
                OrderID = 10325,
                ProductID = 13,
                Quantity = 12
            };
            orderDetails.Add(item203);
            var item204 = new SeedOrderDetail()
            {
                OrderDetailID = 204,
                OrderID = 10325,
                ProductID = 14,
                Quantity = 9
            };
            orderDetails.Add(item204);
            var item205 = new SeedOrderDetail()
            {
                OrderDetailID = 205,
                OrderID = 10325,
                ProductID = 31,
                Quantity = 4
            };
            orderDetails.Add(item205);
            var item206 = new SeedOrderDetail()
            {
                OrderDetailID = 206,
                OrderID = 10325,
                ProductID = 72,
                Quantity = 40
            };
            orderDetails.Add(item206);
            var item207 = new SeedOrderDetail()
            {
                OrderDetailID = 207,
                OrderID = 10326,
                ProductID = 4,
                Quantity = 24
            };
            orderDetails.Add(item207);
            var item208 = new SeedOrderDetail()
            {
                OrderDetailID = 208,
                OrderID = 10326,
                ProductID = 57,
                Quantity = 16
            };
            orderDetails.Add(item208);
            var item209 = new SeedOrderDetail()
            {
                OrderDetailID = 209,
                OrderID = 10326,
                ProductID = 75,
                Quantity = 50
            };
            orderDetails.Add(item209);
            var item210 = new SeedOrderDetail()
            {
                OrderDetailID = 210,
                OrderID = 10327,
                ProductID = 2,
                Quantity = 25
            };
            orderDetails.Add(item210);
            var item211 = new SeedOrderDetail()
            {
                OrderDetailID = 211,
                OrderID = 10327,
                ProductID = 11,
                Quantity = 50
            };
            orderDetails.Add(item211);
            var item212 = new SeedOrderDetail()
            {
                OrderDetailID = 212,
                OrderID = 10327,
                ProductID = 30,
                Quantity = 35
            };
            orderDetails.Add(item212);
            var item213 = new SeedOrderDetail()
            {
                OrderDetailID = 213,
                OrderID = 10327,
                ProductID = 58,
                Quantity = 30
            };
            orderDetails.Add(item213);
            var item214 = new SeedOrderDetail()
            {
                OrderDetailID = 214,
                OrderID = 10328,
                ProductID = 59,
                Quantity = 9
            };
            orderDetails.Add(item214);
            var item215 = new SeedOrderDetail()
            {
                OrderDetailID = 215,
                OrderID = 10328,
                ProductID = 65,
                Quantity = 40
            };
            orderDetails.Add(item215);
            var item216 = new SeedOrderDetail()
            {
                OrderDetailID = 216,
                OrderID = 10328,
                ProductID = 68,
                Quantity = 10
            };
            orderDetails.Add(item216);
            var item217 = new SeedOrderDetail()
            {
                OrderDetailID = 217,
                OrderID = 10329,
                ProductID = 19,
                Quantity = 10
            };
            orderDetails.Add(item217);
            var item218 = new SeedOrderDetail()
            {
                OrderDetailID = 218,
                OrderID = 10329,
                ProductID = 30,
                Quantity = 8
            };
            orderDetails.Add(item218);
            var item219 = new SeedOrderDetail()
            {
                OrderDetailID = 219,
                OrderID = 10329,
                ProductID = 38,
                Quantity = 20
            };
            orderDetails.Add(item219);
            var item220 = new SeedOrderDetail()
            {
                OrderDetailID = 220,
                OrderID = 10329,
                ProductID = 56,
                Quantity = 12
            };
            orderDetails.Add(item220);
            var item221 = new SeedOrderDetail()
            {
                OrderDetailID = 221,
                OrderID = 10330,
                ProductID = 26,
                Quantity = 50
            };
            orderDetails.Add(item221);
            var item222 = new SeedOrderDetail()
            {
                OrderDetailID = 222,
                OrderID = 10330,
                ProductID = 72,
                Quantity = 25
            };
            orderDetails.Add(item222);
            var item223 = new SeedOrderDetail()
            {
                OrderDetailID = 223,
                OrderID = 10331,
                ProductID = 54,
                Quantity = 15
            };
            orderDetails.Add(item223);
            var item224 = new SeedOrderDetail()
            {
                OrderDetailID = 224,
                OrderID = 10332,
                ProductID = 18,
                Quantity = 40
            };
            orderDetails.Add(item224);
            var item225 = new SeedOrderDetail()
            {
                OrderDetailID = 225,
                OrderID = 10332,
                ProductID = 42,
                Quantity = 10
            };
            orderDetails.Add(item225);
            var item226 = new SeedOrderDetail()
            {
                OrderDetailID = 226,
                OrderID = 10332,
                ProductID = 47,
                Quantity = 16
            };
            orderDetails.Add(item226);
            var item227 = new SeedOrderDetail()
            {
                OrderDetailID = 227,
                OrderID = 10333,
                ProductID = 14,
                Quantity = 10
            };
            orderDetails.Add(item227);
            var item228 = new SeedOrderDetail()
            {
                OrderDetailID = 228,
                OrderID = 10333,
                ProductID = 21,
                Quantity = 10
            };
            orderDetails.Add(item228);
            var item229 = new SeedOrderDetail()
            {
                OrderDetailID = 229,
                OrderID = 10333,
                ProductID = 71,
                Quantity = 40
            };
            orderDetails.Add(item229);
            var item230 = new SeedOrderDetail()
            {
                OrderDetailID = 230,
                OrderID = 10334,
                ProductID = 52,
                Quantity = 8
            };
            orderDetails.Add(item230);
            var item231 = new SeedOrderDetail()
            {
                OrderDetailID = 231,
                OrderID = 10334,
                ProductID = 68,
                Quantity = 10
            };
            orderDetails.Add(item231);
            var item232 = new SeedOrderDetail()
            {
                OrderDetailID = 232,
                OrderID = 10335,
                ProductID = 2,
                Quantity = 7
            };
            orderDetails.Add(item232);
            var item233 = new SeedOrderDetail()
            {
                OrderDetailID = 233,
                OrderID = 10335,
                ProductID = 31,
                Quantity = 25
            };
            orderDetails.Add(item233);
            var item234 = new SeedOrderDetail()
            {
                OrderDetailID = 234,
                OrderID = 10335,
                ProductID = 32,
                Quantity = 6
            };
            orderDetails.Add(item234);
            var item235 = new SeedOrderDetail()
            {
                OrderDetailID = 235,
                OrderID = 10335,
                ProductID = 51,
                Quantity = 48
            };
            orderDetails.Add(item235);
            var item236 = new SeedOrderDetail()
            {
                OrderDetailID = 236,
                OrderID = 10336,
                ProductID = 4,
                Quantity = 18
            };
            orderDetails.Add(item236);
            var item237 = new SeedOrderDetail()
            {
                OrderDetailID = 237,
                OrderID = 10337,
                ProductID = 23,
                Quantity = 40
            };
            orderDetails.Add(item237);
            var item238 = new SeedOrderDetail()
            {
                OrderDetailID = 238,
                OrderID = 10337,
                ProductID = 26,
                Quantity = 24
            };
            orderDetails.Add(item238);
            var item239 = new SeedOrderDetail()
            {
                OrderDetailID = 239,
                OrderID = 10337,
                ProductID = 36,
                Quantity = 20
            };
            orderDetails.Add(item239);
            var item240 = new SeedOrderDetail()
            {
                OrderDetailID = 240,
                OrderID = 10337,
                ProductID = 37,
                Quantity = 28
            };
            orderDetails.Add(item240);
            var item241 = new SeedOrderDetail()
            {
                OrderDetailID = 241,
                OrderID = 10337,
                ProductID = 72,
                Quantity = 25
            };
            orderDetails.Add(item241);
            var item242 = new SeedOrderDetail()
            {
                OrderDetailID = 242,
                OrderID = 10338,
                ProductID = 17,
                Quantity = 20
            };
            orderDetails.Add(item242);
            var item243 = new SeedOrderDetail()
            {
                OrderDetailID = 243,
                OrderID = 10338,
                ProductID = 30,
                Quantity = 15
            };
            orderDetails.Add(item243);
            var item244 = new SeedOrderDetail()
            {
                OrderDetailID = 244,
                OrderID = 10339,
                ProductID = 4,
                Quantity = 10
            };
            orderDetails.Add(item244);
            var item245 = new SeedOrderDetail()
            {
                OrderDetailID = 245,
                OrderID = 10339,
                ProductID = 17,
                Quantity = 70
            };
            orderDetails.Add(item245);
            var item246 = new SeedOrderDetail()
            {
                OrderDetailID = 246,
                OrderID = 10339,
                ProductID = 62,
                Quantity = 28
            };
            orderDetails.Add(item246);
            var item247 = new SeedOrderDetail()
            {
                OrderDetailID = 247,
                OrderID = 10340,
                ProductID = 18,
                Quantity = 20
            };
            orderDetails.Add(item247);
            var item248 = new SeedOrderDetail()
            {
                OrderDetailID = 248,
                OrderID = 10340,
                ProductID = 41,
                Quantity = 12
            };
            orderDetails.Add(item248);
            var item249 = new SeedOrderDetail()
            {
                OrderDetailID = 249,
                OrderID = 10340,
                ProductID = 43,
                Quantity = 40
            };
            orderDetails.Add(item249);
            var item250 = new SeedOrderDetail()
            {
                OrderDetailID = 250,
                OrderID = 10341,
                ProductID = 33,
                Quantity = 8
            };
            orderDetails.Add(item250);
            var item251 = new SeedOrderDetail()
            {
                OrderDetailID = 251,
                OrderID = 10341,
                ProductID = 59,
                Quantity = 9
            };
            orderDetails.Add(item251);
            var item252 = new SeedOrderDetail()
            {
                OrderDetailID = 252,
                OrderID = 10342,
                ProductID = 2,
                Quantity = 24
            };
            orderDetails.Add(item252);
            var item253 = new SeedOrderDetail()
            {
                OrderDetailID = 253,
                OrderID = 10342,
                ProductID = 31,
                Quantity = 56
            };
            orderDetails.Add(item253);
            var item254 = new SeedOrderDetail()
            {
                OrderDetailID = 254,
                OrderID = 10342,
                ProductID = 36,
                Quantity = 40
            };
            orderDetails.Add(item254);
            var item255 = new SeedOrderDetail()
            {
                OrderDetailID = 255,
                OrderID = 10342,
                ProductID = 55,
                Quantity = 40
            };
            orderDetails.Add(item255);
            var item256 = new SeedOrderDetail()
            {
                OrderDetailID = 256,
                OrderID = 10343,
                ProductID = 64,
                Quantity = 50
            };
            orderDetails.Add(item256);
            var item257 = new SeedOrderDetail()
            {
                OrderDetailID = 257,
                OrderID = 10343,
                ProductID = 68,
                Quantity = 4
            };
            orderDetails.Add(item257);
            var item258 = new SeedOrderDetail()
            {
                OrderDetailID = 258,
                OrderID = 10343,
                ProductID = 76,
                Quantity = 15
            };
            orderDetails.Add(item258);
            var item259 = new SeedOrderDetail()
            {
                OrderDetailID = 259,
                OrderID = 10344,
                ProductID = 4,
                Quantity = 35
            };
            orderDetails.Add(item259);
            var item260 = new SeedOrderDetail()
            {
                OrderDetailID = 260,
                OrderID = 10344,
                ProductID = 8,
                Quantity = 70
            };
            orderDetails.Add(item260);
            var item261 = new SeedOrderDetail()
            {
                OrderDetailID = 261,
                OrderID = 10345,
                ProductID = 8,
                Quantity = 70
            };
            orderDetails.Add(item261);
            var item262 = new SeedOrderDetail()
            {
                OrderDetailID = 262,
                OrderID = 10345,
                ProductID = 19,
                Quantity = 80
            };
            orderDetails.Add(item262);
            var item263 = new SeedOrderDetail()
            {
                OrderDetailID = 263,
                OrderID = 10345,
                ProductID = 42,
                Quantity = 9
            };
            orderDetails.Add(item263);
            var item264 = new SeedOrderDetail()
            {
                OrderDetailID = 264,
                OrderID = 10346,
                ProductID = 17,
                Quantity = 36
            };
            orderDetails.Add(item264);
            var item265 = new SeedOrderDetail()
            {
                OrderDetailID = 265,
                OrderID = 10346,
                ProductID = 56,
                Quantity = 20
            };
            orderDetails.Add(item265);
            var item266 = new SeedOrderDetail()
            {
                OrderDetailID = 266,
                OrderID = 10347,
                ProductID = 25,
                Quantity = 10
            };
            orderDetails.Add(item266);
            var item267 = new SeedOrderDetail()
            {
                OrderDetailID = 267,
                OrderID = 10347,
                ProductID = 39,
                Quantity = 50
            };
            orderDetails.Add(item267);
            var item268 = new SeedOrderDetail()
            {
                OrderDetailID = 268,
                OrderID = 10347,
                ProductID = 40,
                Quantity = 4
            };
            orderDetails.Add(item268);
            var item269 = new SeedOrderDetail()
            {
                OrderDetailID = 269,
                OrderID = 10347,
                ProductID = 75,
                Quantity = 6
            };
            orderDetails.Add(item269);
            var item270 = new SeedOrderDetail()
            {
                OrderDetailID = 270,
                OrderID = 10348,
                ProductID = 1,
                Quantity = 15
            };
            orderDetails.Add(item270);
            var item271 = new SeedOrderDetail()
            {
                OrderDetailID = 271,
                OrderID = 10348,
                ProductID = 23,
                Quantity = 25
            };
            orderDetails.Add(item271);
            var item272 = new SeedOrderDetail()
            {
                OrderDetailID = 272,
                OrderID = 10349,
                ProductID = 54,
                Quantity = 24
            };
            orderDetails.Add(item272);
            var item273 = new SeedOrderDetail()
            {
                OrderDetailID = 273,
                OrderID = 10350,
                ProductID = 50,
                Quantity = 15
            };
            orderDetails.Add(item273);
            var item274 = new SeedOrderDetail()
            {
                OrderDetailID = 274,
                OrderID = 10350,
                ProductID = 69,
                Quantity = 18
            };
            orderDetails.Add(item274);
            var item275 = new SeedOrderDetail()
            {
                OrderDetailID = 275,
                OrderID = 10351,
                ProductID = 38,
                Quantity = 20
            };
            orderDetails.Add(item275);
            var item276 = new SeedOrderDetail()
            {
                OrderDetailID = 276,
                OrderID = 10351,
                ProductID = 41,
                Quantity = 13
            };
            orderDetails.Add(item276);
            var item277 = new SeedOrderDetail()
            {
                OrderDetailID = 277,
                OrderID = 10351,
                ProductID = 44,
                Quantity = 77
            };
            orderDetails.Add(item277);
            var item278 = new SeedOrderDetail()
            {
                OrderDetailID = 278,
                OrderID = 10351,
                ProductID = 65,
                Quantity = 10
            };
            orderDetails.Add(item278);
            var item279 = new SeedOrderDetail()
            {
                OrderDetailID = 279,
                OrderID = 10352,
                ProductID = 24,
                Quantity = 10
            };
            orderDetails.Add(item279);
            var item280 = new SeedOrderDetail()
            {
                OrderDetailID = 280,
                OrderID = 10352,
                ProductID = 54,
                Quantity = 20
            };
            orderDetails.Add(item280);
            var item281 = new SeedOrderDetail()
            {
                OrderDetailID = 281,
                OrderID = 10353,
                ProductID = 11,
                Quantity = 12
            };
            orderDetails.Add(item281);
            var item282 = new SeedOrderDetail()
            {
                OrderDetailID = 282,
                OrderID = 10353,
                ProductID = 38,
                Quantity = 50
            };
            orderDetails.Add(item282);
            var item283 = new SeedOrderDetail()
            {
                OrderDetailID = 283,
                OrderID = 10354,
                ProductID = 1,
                Quantity = 12
            };
            orderDetails.Add(item283);
            var item284 = new SeedOrderDetail()
            {
                OrderDetailID = 284,
                OrderID = 10354,
                ProductID = 29,
                Quantity = 4
            };
            orderDetails.Add(item284);
            var item285 = new SeedOrderDetail()
            {
                OrderDetailID = 285,
                OrderID = 10355,
                ProductID = 24,
                Quantity = 25
            };
            orderDetails.Add(item285);
            var item286 = new SeedOrderDetail()
            {
                OrderDetailID = 286,
                OrderID = 10355,
                ProductID = 57,
                Quantity = 25
            };
            orderDetails.Add(item286);
            var item287 = new SeedOrderDetail()
            {
                OrderDetailID = 287,
                OrderID = 10356,
                ProductID = 31,
                Quantity = 30
            };
            orderDetails.Add(item287);
            var item288 = new SeedOrderDetail()
            {
                OrderDetailID = 288,
                OrderID = 10356,
                ProductID = 55,
                Quantity = 12
            };
            orderDetails.Add(item288);
            var item289 = new SeedOrderDetail()
            {
                OrderDetailID = 289,
                OrderID = 10356,
                ProductID = 69,
                Quantity = 20
            };
            orderDetails.Add(item289);
            var item290 = new SeedOrderDetail()
            {
                OrderDetailID = 290,
                OrderID = 10357,
                ProductID = 10,
                Quantity = 30
            };
            orderDetails.Add(item290);
            var item291 = new SeedOrderDetail()
            {
                OrderDetailID = 291,
                OrderID = 10357,
                ProductID = 26,
                Quantity = 16
            };
            orderDetails.Add(item291);
            var item292 = new SeedOrderDetail()
            {
                OrderDetailID = 292,
                OrderID = 10357,
                ProductID = 60,
                Quantity = 8
            };
            orderDetails.Add(item292);
            var item293 = new SeedOrderDetail()
            {
                OrderDetailID = 293,
                OrderID = 10358,
                ProductID = 24,
                Quantity = 10
            };
            orderDetails.Add(item293);
            var item294 = new SeedOrderDetail()
            {
                OrderDetailID = 294,
                OrderID = 10358,
                ProductID = 34,
                Quantity = 10
            };
            orderDetails.Add(item294);
            var item295 = new SeedOrderDetail()
            {
                OrderDetailID = 295,
                OrderID = 10358,
                ProductID = 36,
                Quantity = 20
            };
            orderDetails.Add(item295);
            var item296 = new SeedOrderDetail()
            {
                OrderDetailID = 296,
                OrderID = 10359,
                ProductID = 16,
                Quantity = 56
            };
            orderDetails.Add(item296);
            var item297 = new SeedOrderDetail()
            {
                OrderDetailID = 297,
                OrderID = 10359,
                ProductID = 31,
                Quantity = 70
            };
            orderDetails.Add(item297);
            var item298 = new SeedOrderDetail()
            {
                OrderDetailID = 298,
                OrderID = 10359,
                ProductID = 60,
                Quantity = 80
            };
            orderDetails.Add(item298);
            var item299 = new SeedOrderDetail()
            {
                OrderDetailID = 299,
                OrderID = 10360,
                ProductID = 28,
                Quantity = 30
            };
            orderDetails.Add(item299);
            var item300 = new SeedOrderDetail()
            {
                OrderDetailID = 300,
                OrderID = 10360,
                ProductID = 29,
                Quantity = 35
            };
            orderDetails.Add(item300);
            var item301 = new SeedOrderDetail()
            {
                OrderDetailID = 301,
                OrderID = 10360,
                ProductID = 38,
                Quantity = 10
            };
            orderDetails.Add(item301);
            var item302 = new SeedOrderDetail()
            {
                OrderDetailID = 302,
                OrderID = 10360,
                ProductID = 49,
                Quantity = 35
            };
            orderDetails.Add(item302);
            var item303 = new SeedOrderDetail()
            {
                OrderDetailID = 303,
                OrderID = 10360,
                ProductID = 54,
                Quantity = 28
            };
            orderDetails.Add(item303);
            var item304 = new SeedOrderDetail()
            {
                OrderDetailID = 304,
                OrderID = 10361,
                ProductID = 39,
                Quantity = 54
            };
            orderDetails.Add(item304);
            var item305 = new SeedOrderDetail()
            {
                OrderDetailID = 305,
                OrderID = 10361,
                ProductID = 60,
                Quantity = 55
            };
            orderDetails.Add(item305);
            var item306 = new SeedOrderDetail()
            {
                OrderDetailID = 306,
                OrderID = 10362,
                ProductID = 25,
                Quantity = 50
            };
            orderDetails.Add(item306);
            var item307 = new SeedOrderDetail()
            {
                OrderDetailID = 307,
                OrderID = 10362,
                ProductID = 51,
                Quantity = 20
            };
            orderDetails.Add(item307);
            var item308 = new SeedOrderDetail()
            {
                OrderDetailID = 308,
                OrderID = 10362,
                ProductID = 54,
                Quantity = 24
            };
            orderDetails.Add(item308);
            var item309 = new SeedOrderDetail()
            {
                OrderDetailID = 309,
                OrderID = 10363,
                ProductID = 31,
                Quantity = 20
            };
            orderDetails.Add(item309);
            var item310 = new SeedOrderDetail()
            {
                OrderDetailID = 310,
                OrderID = 10363,
                ProductID = 75,
                Quantity = 12
            };
            orderDetails.Add(item310);
            var item311 = new SeedOrderDetail()
            {
                OrderDetailID = 311,
                OrderID = 10363,
                ProductID = 76,
                Quantity = 12
            };
            orderDetails.Add(item311);
            var item312 = new SeedOrderDetail()
            {
                OrderDetailID = 312,
                OrderID = 10364,
                ProductID = 69,
                Quantity = 30
            };
            orderDetails.Add(item312);
            var item313 = new SeedOrderDetail()
            {
                OrderDetailID = 313,
                OrderID = 10364,
                ProductID = 71,
                Quantity = 5
            };
            orderDetails.Add(item313);
            var item314 = new SeedOrderDetail()
            {
                OrderDetailID = 314,
                OrderID = 10365,
                ProductID = 11,
                Quantity = 24
            };
            orderDetails.Add(item314);
            var item315 = new SeedOrderDetail()
            {
                OrderDetailID = 315,
                OrderID = 10366,
                ProductID = 65,
                Quantity = 5
            };
            orderDetails.Add(item315);
            var item316 = new SeedOrderDetail()
            {
                OrderDetailID = 316,
                OrderID = 10366,
                ProductID = 77,
                Quantity = 5
            };
            orderDetails.Add(item316);
            var item317 = new SeedOrderDetail()
            {
                OrderDetailID = 317,
                OrderID = 10367,
                ProductID = 34,
                Quantity = 36
            };
            orderDetails.Add(item317);
            var item318 = new SeedOrderDetail()
            {
                OrderDetailID = 318,
                OrderID = 10367,
                ProductID = 54,
                Quantity = 18
            };
            orderDetails.Add(item318);
            var item319 = new SeedOrderDetail()
            {
                OrderDetailID = 319,
                OrderID = 10367,
                ProductID = 65,
                Quantity = 15
            };
            orderDetails.Add(item319);
            var item320 = new SeedOrderDetail()
            {
                OrderDetailID = 320,
                OrderID = 10367,
                ProductID = 77,
                Quantity = 7
            };
            orderDetails.Add(item320);
            var item321 = new SeedOrderDetail()
            {
                OrderDetailID = 321,
                OrderID = 10368,
                ProductID = 21,
                Quantity = 5
            };
            orderDetails.Add(item321);
            var item322 = new SeedOrderDetail()
            {
                OrderDetailID = 322,
                OrderID = 10368,
                ProductID = 28,
                Quantity = 13
            };
            orderDetails.Add(item322);
            var item323 = new SeedOrderDetail()
            {
                OrderDetailID = 323,
                OrderID = 10368,
                ProductID = 57,
                Quantity = 25
            };
            orderDetails.Add(item323);
            var item324 = new SeedOrderDetail()
            {
                OrderDetailID = 324,
                OrderID = 10368,
                ProductID = 64,
                Quantity = 35
            };
            orderDetails.Add(item324);
            var item325 = new SeedOrderDetail()
            {
                OrderDetailID = 325,
                OrderID = 10369,
                ProductID = 29,
                Quantity = 20
            };
            orderDetails.Add(item325);
            var item326 = new SeedOrderDetail()
            {
                OrderDetailID = 326,
                OrderID = 10369,
                ProductID = 56,
                Quantity = 18
            };
            orderDetails.Add(item326);
            var item327 = new SeedOrderDetail()
            {
                OrderDetailID = 327,
                OrderID = 10370,
                ProductID = 1,
                Quantity = 15
            };
            orderDetails.Add(item327);
            var item328 = new SeedOrderDetail()
            {
                OrderDetailID = 328,
                OrderID = 10370,
                ProductID = 64,
                Quantity = 30
            };
            orderDetails.Add(item328);
            var item329 = new SeedOrderDetail()
            {
                OrderDetailID = 329,
                OrderID = 10370,
                ProductID = 74,
                Quantity = 20
            };
            orderDetails.Add(item329);
            var item330 = new SeedOrderDetail()
            {
                OrderDetailID = 330,
                OrderID = 10371,
                ProductID = 36,
                Quantity = 6
            };
            orderDetails.Add(item330);
            var item331 = new SeedOrderDetail()
            {
                OrderDetailID = 331,
                OrderID = 10372,
                ProductID = 20,
                Quantity = 12
            };
            orderDetails.Add(item331);
            var item332 = new SeedOrderDetail()
            {
                OrderDetailID = 332,
                OrderID = 10372,
                ProductID = 38,
                Quantity = 40
            };
            orderDetails.Add(item332);
            var item333 = new SeedOrderDetail()
            {
                OrderDetailID = 333,
                OrderID = 10372,
                ProductID = 60,
                Quantity = 70
            };
            orderDetails.Add(item333);
            var item334 = new SeedOrderDetail()
            {
                OrderDetailID = 334,
                OrderID = 10372,
                ProductID = 72,
                Quantity = 42
            };
            orderDetails.Add(item334);
            var item335 = new SeedOrderDetail()
            {
                OrderDetailID = 335,
                OrderID = 10373,
                ProductID = 58,
                Quantity = 80
            };
            orderDetails.Add(item335);
            var item336 = new SeedOrderDetail()
            {
                OrderDetailID = 336,
                OrderID = 10373,
                ProductID = 71,
                Quantity = 50
            };
            orderDetails.Add(item336);
            var item337 = new SeedOrderDetail()
            {
                OrderDetailID = 337,
                OrderID = 10374,
                ProductID = 31,
                Quantity = 30
            };
            orderDetails.Add(item337);
            var item338 = new SeedOrderDetail()
            {
                OrderDetailID = 338,
                OrderID = 10374,
                ProductID = 58,
                Quantity = 15
            };
            orderDetails.Add(item338);
            var item339 = new SeedOrderDetail()
            {
                OrderDetailID = 339,
                OrderID = 10375,
                ProductID = 14,
                Quantity = 15
            };
            orderDetails.Add(item339);
            var item340 = new SeedOrderDetail()
            {
                OrderDetailID = 340,
                OrderID = 10375,
                ProductID = 54,
                Quantity = 10
            };
            orderDetails.Add(item340);
            var item341 = new SeedOrderDetail()
            {
                OrderDetailID = 341,
                OrderID = 10376,
                ProductID = 31,
                Quantity = 42
            };
            orderDetails.Add(item341);
            var item342 = new SeedOrderDetail()
            {
                OrderDetailID = 342,
                OrderID = 10377,
                ProductID = 28,
                Quantity = 20
            };
            orderDetails.Add(item342);
            var item343 = new SeedOrderDetail()
            {
                OrderDetailID = 343,
                OrderID = 10377,
                ProductID = 39,
                Quantity = 20
            };
            orderDetails.Add(item343);
            var item344 = new SeedOrderDetail()
            {
                OrderDetailID = 344,
                OrderID = 10378,
                ProductID = 71,
                Quantity = 6
            };
            orderDetails.Add(item344);
            var item345 = new SeedOrderDetail()
            {
                OrderDetailID = 345,
                OrderID = 10379,
                ProductID = 41,
                Quantity = 8
            };
            orderDetails.Add(item345);
            var item346 = new SeedOrderDetail()
            {
                OrderDetailID = 346,
                OrderID = 10379,
                ProductID = 63,
                Quantity = 16
            };
            orderDetails.Add(item346);
            var item347 = new SeedOrderDetail()
            {
                OrderDetailID = 347,
                OrderID = 10379,
                ProductID = 65,
                Quantity = 20
            };
            orderDetails.Add(item347);
            var item348 = new SeedOrderDetail()
            {
                OrderDetailID = 348,
                OrderID = 10380,
                ProductID = 30,
                Quantity = 18
            };
            orderDetails.Add(item348);
            var item349 = new SeedOrderDetail()
            {
                OrderDetailID = 349,
                OrderID = 10380,
                ProductID = 53,
                Quantity = 20
            };
            orderDetails.Add(item349);
            var item350 = new SeedOrderDetail()
            {
                OrderDetailID = 350,
                OrderID = 10380,
                ProductID = 60,
                Quantity = 6
            };
            orderDetails.Add(item350);
            var item351 = new SeedOrderDetail()
            {
                OrderDetailID = 351,
                OrderID = 10380,
                ProductID = 70,
                Quantity = 30
            };
            orderDetails.Add(item351);
            var item352 = new SeedOrderDetail()
            {
                OrderDetailID = 352,
                OrderID = 10381,
                ProductID = 74,
                Quantity = 14
            };
            orderDetails.Add(item352);
            var item353 = new SeedOrderDetail()
            {
                OrderDetailID = 353,
                OrderID = 10382,
                ProductID = 5,
                Quantity = 32
            };
            orderDetails.Add(item353);
            var item354 = new SeedOrderDetail()
            {
                OrderDetailID = 354,
                OrderID = 10382,
                ProductID = 18,
                Quantity = 9
            };
            orderDetails.Add(item354);
            var item355 = new SeedOrderDetail()
            {
                OrderDetailID = 355,
                OrderID = 10382,
                ProductID = 29,
                Quantity = 14
            };
            orderDetails.Add(item355);
            var item356 = new SeedOrderDetail()
            {
                OrderDetailID = 356,
                OrderID = 10382,
                ProductID = 33,
                Quantity = 60
            };
            orderDetails.Add(item356);
            var item357 = new SeedOrderDetail()
            {
                OrderDetailID = 357,
                OrderID = 10382,
                ProductID = 74,
                Quantity = 50
            };
            orderDetails.Add(item357);
            var item358 = new SeedOrderDetail()
            {
                OrderDetailID = 358,
                OrderID = 10383,
                ProductID = 13,
                Quantity = 20
            };
            orderDetails.Add(item358);
            var item359 = new SeedOrderDetail()
            {
                OrderDetailID = 359,
                OrderID = 10383,
                ProductID = 50,
                Quantity = 15
            };
            orderDetails.Add(item359);
            var item360 = new SeedOrderDetail()
            {
                OrderDetailID = 360,
                OrderID = 10383,
                ProductID = 56,
                Quantity = 20
            };
            orderDetails.Add(item360);
            var item361 = new SeedOrderDetail()
            {
                OrderDetailID = 361,
                OrderID = 10384,
                ProductID = 20,
                Quantity = 28
            };
            orderDetails.Add(item361);
            var item362 = new SeedOrderDetail()
            {
                OrderDetailID = 362,
                OrderID = 10384,
                ProductID = 60,
                Quantity = 15
            };
            orderDetails.Add(item362);
            var item363 = new SeedOrderDetail()
            {
                OrderDetailID = 363,
                OrderID = 10385,
                ProductID = 7,
                Quantity = 10
            };
            orderDetails.Add(item363);
            var item364 = new SeedOrderDetail()
            {
                OrderDetailID = 364,
                OrderID = 10385,
                ProductID = 60,
                Quantity = 20
            };
            orderDetails.Add(item364);
            var item365 = new SeedOrderDetail()
            {
                OrderDetailID = 365,
                OrderID = 10385,
                ProductID = 68,
                Quantity = 8
            };
            orderDetails.Add(item365);
            var item366 = new SeedOrderDetail()
            {
                OrderDetailID = 366,
                OrderID = 10386,
                ProductID = 24,
                Quantity = 15
            };
            orderDetails.Add(item366);
            var item367 = new SeedOrderDetail()
            {
                OrderDetailID = 367,
                OrderID = 10386,
                ProductID = 34,
                Quantity = 10
            };
            orderDetails.Add(item367);
            var item368 = new SeedOrderDetail()
            {
                OrderDetailID = 368,
                OrderID = 10387,
                ProductID = 24,
                Quantity = 15
            };
            orderDetails.Add(item368);
            var item369 = new SeedOrderDetail()
            {
                OrderDetailID = 369,
                OrderID = 10387,
                ProductID = 28,
                Quantity = 6
            };
            orderDetails.Add(item369);
            var item370 = new SeedOrderDetail()
            {
                OrderDetailID = 370,
                OrderID = 10387,
                ProductID = 59,
                Quantity = 12
            };
            orderDetails.Add(item370);
            var item371 = new SeedOrderDetail()
            {
                OrderDetailID = 371,
                OrderID = 10387,
                ProductID = 71,
                Quantity = 15
            };
            orderDetails.Add(item371);
            var item372 = new SeedOrderDetail()
            {
                OrderDetailID = 372,
                OrderID = 10388,
                ProductID = 45,
                Quantity = 15
            };
            orderDetails.Add(item372);
            var item373 = new SeedOrderDetail()
            {
                OrderDetailID = 373,
                OrderID = 10388,
                ProductID = 52,
                Quantity = 20
            };
            orderDetails.Add(item373);
            var item374 = new SeedOrderDetail()
            {
                OrderDetailID = 374,
                OrderID = 10388,
                ProductID = 53,
                Quantity = 40
            };
            orderDetails.Add(item374);
            var item375 = new SeedOrderDetail()
            {
                OrderDetailID = 375,
                OrderID = 10389,
                ProductID = 10,
                Quantity = 16
            };
            orderDetails.Add(item375);
            var item376 = new SeedOrderDetail()
            {
                OrderDetailID = 376,
                OrderID = 10389,
                ProductID = 55,
                Quantity = 15
            };
            orderDetails.Add(item376);
            var item377 = new SeedOrderDetail()
            {
                OrderDetailID = 377,
                OrderID = 10389,
                ProductID = 62,
                Quantity = 20
            };
            orderDetails.Add(item377);
            var item378 = new SeedOrderDetail()
            {
                OrderDetailID = 378,
                OrderID = 10389,
                ProductID = 70,
                Quantity = 30
            };
            orderDetails.Add(item378);
            var item379 = new SeedOrderDetail()
            {
                OrderDetailID = 379,
                OrderID = 10390,
                ProductID = 31,
                Quantity = 60
            };
            orderDetails.Add(item379);
            var item380 = new SeedOrderDetail()
            {
                OrderDetailID = 380,
                OrderID = 10390,
                ProductID = 35,
                Quantity = 40
            };
            orderDetails.Add(item380);
            var item381 = new SeedOrderDetail()
            {
                OrderDetailID = 381,
                OrderID = 10390,
                ProductID = 46,
                Quantity = 45
            };
            orderDetails.Add(item381);
            var item382 = new SeedOrderDetail()
            {
                OrderDetailID = 382,
                OrderID = 10390,
                ProductID = 72,
                Quantity = 24
            };
            orderDetails.Add(item382);
            var item383 = new SeedOrderDetail()
            {
                OrderDetailID = 383,
                OrderID = 10391,
                ProductID = 13,
                Quantity = 18
            };
            orderDetails.Add(item383);
            var item384 = new SeedOrderDetail()
            {
                OrderDetailID = 384,
                OrderID = 10392,
                ProductID = 69,
                Quantity = 50
            };
            orderDetails.Add(item384);
            var item385 = new SeedOrderDetail()
            {
                OrderDetailID = 385,
                OrderID = 10393,
                ProductID = 2,
                Quantity = 25
            };
            orderDetails.Add(item385);
            var item386 = new SeedOrderDetail()
            {
                OrderDetailID = 386,
                OrderID = 10393,
                ProductID = 14,
                Quantity = 42
            };
            orderDetails.Add(item386);
            var item387 = new SeedOrderDetail()
            {
                OrderDetailID = 387,
                OrderID = 10393,
                ProductID = 25,
                Quantity = 7
            };
            orderDetails.Add(item387);
            var item388 = new SeedOrderDetail()
            {
                OrderDetailID = 388,
                OrderID = 10393,
                ProductID = 26,
                Quantity = 70
            };
            orderDetails.Add(item388);
            var item389 = new SeedOrderDetail()
            {
                OrderDetailID = 389,
                OrderID = 10393,
                ProductID = 31,
                Quantity = 32
            };
            orderDetails.Add(item389);
            var item390 = new SeedOrderDetail()
            {
                OrderDetailID = 390,
                OrderID = 10394,
                ProductID = 13,
                Quantity = 10
            };
            orderDetails.Add(item390);
            var item391 = new SeedOrderDetail()
            {
                OrderDetailID = 391,
                OrderID = 10394,
                ProductID = 62,
                Quantity = 10
            };
            orderDetails.Add(item391);
            var item392 = new SeedOrderDetail()
            {
                OrderDetailID = 392,
                OrderID = 10395,
                ProductID = 46,
                Quantity = 28
            };
            orderDetails.Add(item392);
            var item393 = new SeedOrderDetail()
            {
                OrderDetailID = 393,
                OrderID = 10395,
                ProductID = 53,
                Quantity = 70
            };
            orderDetails.Add(item393);
            var item394 = new SeedOrderDetail()
            {
                OrderDetailID = 394,
                OrderID = 10395,
                ProductID = 69,
                Quantity = 8
            };
            orderDetails.Add(item394);
            var item395 = new SeedOrderDetail()
            {
                OrderDetailID = 395,
                OrderID = 10396,
                ProductID = 23,
                Quantity = 40
            };
            orderDetails.Add(item395);
            var item396 = new SeedOrderDetail()
            {
                OrderDetailID = 396,
                OrderID = 10396,
                ProductID = 71,
                Quantity = 60
            };
            orderDetails.Add(item396);
            var item397 = new SeedOrderDetail()
            {
                OrderDetailID = 397,
                OrderID = 10396,
                ProductID = 72,
                Quantity = 21
            };
            orderDetails.Add(item397);
            var item398 = new SeedOrderDetail()
            {
                OrderDetailID = 398,
                OrderID = 10397,
                ProductID = 21,
                Quantity = 10
            };
            orderDetails.Add(item398);
            var item399 = new SeedOrderDetail()
            {
                OrderDetailID = 399,
                OrderID = 10397,
                ProductID = 51,
                Quantity = 18
            };
            orderDetails.Add(item399);
            var item400 = new SeedOrderDetail()
            {
                OrderDetailID = 400,
                OrderID = 10398,
                ProductID = 35,
                Quantity = 30
            };
            orderDetails.Add(item400);
            var item401 = new SeedOrderDetail()
            {
                OrderDetailID = 401,
                OrderID = 10398,
                ProductID = 55,
                Quantity = 120
            };
            orderDetails.Add(item401);
            var item402 = new SeedOrderDetail()
            {
                OrderDetailID = 402,
                OrderID = 10399,
                ProductID = 68,
                Quantity = 60
            };
            orderDetails.Add(item402);
            var item403 = new SeedOrderDetail()
            {
                OrderDetailID = 403,
                OrderID = 10399,
                ProductID = 71,
                Quantity = 30
            };
            orderDetails.Add(item403);
            var item404 = new SeedOrderDetail()
            {
                OrderDetailID = 404,
                OrderID = 10399,
                ProductID = 76,
                Quantity = 35
            };
            orderDetails.Add(item404);
            var item405 = new SeedOrderDetail()
            {
                OrderDetailID = 405,
                OrderID = 10399,
                ProductID = 77,
                Quantity = 14
            };
            orderDetails.Add(item405);
            var item406 = new SeedOrderDetail()
            {
                OrderDetailID = 406,
                OrderID = 10400,
                ProductID = 29,
                Quantity = 21
            };
            orderDetails.Add(item406);
            var item407 = new SeedOrderDetail()
            {
                OrderDetailID = 407,
                OrderID = 10400,
                ProductID = 35,
                Quantity = 35
            };
            orderDetails.Add(item407);
            var item408 = new SeedOrderDetail()
            {
                OrderDetailID = 408,
                OrderID = 10400,
                ProductID = 49,
                Quantity = 30
            };
            orderDetails.Add(item408);
            var item409 = new SeedOrderDetail()
            {
                OrderDetailID = 409,
                OrderID = 10401,
                ProductID = 30,
                Quantity = 18
            };
            orderDetails.Add(item409);
            var item410 = new SeedOrderDetail()
            {
                OrderDetailID = 410,
                OrderID = 10401,
                ProductID = 56,
                Quantity = 70
            };
            orderDetails.Add(item410);
            var item411 = new SeedOrderDetail()
            {
                OrderDetailID = 411,
                OrderID = 10401,
                ProductID = 65,
                Quantity = 20
            };
            orderDetails.Add(item411);
            var item412 = new SeedOrderDetail()
            {
                OrderDetailID = 412,
                OrderID = 10401,
                ProductID = 71,
                Quantity = 60
            };
            orderDetails.Add(item412);
            var item413 = new SeedOrderDetail()
            {
                OrderDetailID = 413,
                OrderID = 10402,
                ProductID = 23,
                Quantity = 60
            };
            orderDetails.Add(item413);
            var item414 = new SeedOrderDetail()
            {
                OrderDetailID = 414,
                OrderID = 10402,
                ProductID = 63,
                Quantity = 65
            };
            orderDetails.Add(item414);
            var item415 = new SeedOrderDetail()
            {
                OrderDetailID = 415,
                OrderID = 10403,
                ProductID = 16,
                Quantity = 21
            };
            orderDetails.Add(item415);
            var item416 = new SeedOrderDetail()
            {
                OrderDetailID = 416,
                OrderID = 10403,
                ProductID = 48,
                Quantity = 70
            };
            orderDetails.Add(item416);
            var item417 = new SeedOrderDetail()
            {
                OrderDetailID = 417,
                OrderID = 10404,
                ProductID = 26,
                Quantity = 30
            };
            orderDetails.Add(item417);
            var item418 = new SeedOrderDetail()
            {
                OrderDetailID = 418,
                OrderID = 10404,
                ProductID = 42,
                Quantity = 40
            };
            orderDetails.Add(item418);
            var item419 = new SeedOrderDetail()
            {
                OrderDetailID = 419,
                OrderID = 10404,
                ProductID = 49,
                Quantity = 30
            };
            orderDetails.Add(item419);
            var item420 = new SeedOrderDetail()
            {
                OrderDetailID = 420,
                OrderID = 10405,
                ProductID = 3,
                Quantity = 50
            };
            orderDetails.Add(item420);
            var item421 = new SeedOrderDetail()
            {
                OrderDetailID = 421,
                OrderID = 10406,
                ProductID = 1,
                Quantity = 10
            };
            orderDetails.Add(item421);
            var item422 = new SeedOrderDetail()
            {
                OrderDetailID = 422,
                OrderID = 10406,
                ProductID = 21,
                Quantity = 30
            };
            orderDetails.Add(item422);
            var item423 = new SeedOrderDetail()
            {
                OrderDetailID = 423,
                OrderID = 10406,
                ProductID = 28,
                Quantity = 42
            };
            orderDetails.Add(item423);
            var item424 = new SeedOrderDetail()
            {
                OrderDetailID = 424,
                OrderID = 10406,
                ProductID = 36,
                Quantity = 5
            };
            orderDetails.Add(item424);
            var item425 = new SeedOrderDetail()
            {
                OrderDetailID = 425,
                OrderID = 10406,
                ProductID = 40,
                Quantity = 2
            };
            orderDetails.Add(item425);
            var item426 = new SeedOrderDetail()
            {
                OrderDetailID = 426,
                OrderID = 10407,
                ProductID = 11,
                Quantity = 30
            };
            orderDetails.Add(item426);
            var item427 = new SeedOrderDetail()
            {
                OrderDetailID = 427,
                OrderID = 10407,
                ProductID = 69,
                Quantity = 15
            };
            orderDetails.Add(item427);
            var item428 = new SeedOrderDetail()
            {
                OrderDetailID = 428,
                OrderID = 10407,
                ProductID = 71,
                Quantity = 15
            };
            orderDetails.Add(item428);
            var item429 = new SeedOrderDetail()
            {
                OrderDetailID = 429,
                OrderID = 10408,
                ProductID = 37,
                Quantity = 10
            };
            orderDetails.Add(item429);
            var item430 = new SeedOrderDetail()
            {
                OrderDetailID = 430,
                OrderID = 10408,
                ProductID = 54,
                Quantity = 6
            };
            orderDetails.Add(item430);
            var item431 = new SeedOrderDetail()
            {
                OrderDetailID = 431,
                OrderID = 10408,
                ProductID = 62,
                Quantity = 35
            };
            orderDetails.Add(item431);
            var item432 = new SeedOrderDetail()
            {
                OrderDetailID = 432,
                OrderID = 10409,
                ProductID = 14,
                Quantity = 12
            };
            orderDetails.Add(item432);
            var item433 = new SeedOrderDetail()
            {
                OrderDetailID = 433,
                OrderID = 10409,
                ProductID = 21,
                Quantity = 12
            };
            orderDetails.Add(item433);
            var item434 = new SeedOrderDetail()
            {
                OrderDetailID = 434,
                OrderID = 10410,
                ProductID = 33,
                Quantity = 49
            };
            orderDetails.Add(item434);
            var item435 = new SeedOrderDetail()
            {
                OrderDetailID = 435,
                OrderID = 10410,
                ProductID = 59,
                Quantity = 16
            };
            orderDetails.Add(item435);
            var item436 = new SeedOrderDetail()
            {
                OrderDetailID = 436,
                OrderID = 10411,
                ProductID = 41,
                Quantity = 25
            };
            orderDetails.Add(item436);
            var item437 = new SeedOrderDetail()
            {
                OrderDetailID = 437,
                OrderID = 10411,
                ProductID = 44,
                Quantity = 40
            };
            orderDetails.Add(item437);
            var item438 = new SeedOrderDetail()
            {
                OrderDetailID = 438,
                OrderID = 10411,
                ProductID = 59,
                Quantity = 9
            };
            orderDetails.Add(item438);
            var item439 = new SeedOrderDetail()
            {
                OrderDetailID = 439,
                OrderID = 10412,
                ProductID = 14,
                Quantity = 20
            };
            orderDetails.Add(item439);
            var item440 = new SeedOrderDetail()
            {
                OrderDetailID = 440,
                OrderID = 10413,
                ProductID = 1,
                Quantity = 24
            };
            orderDetails.Add(item440);
            var item441 = new SeedOrderDetail()
            {
                OrderDetailID = 441,
                OrderID = 10413,
                ProductID = 62,
                Quantity = 40
            };
            orderDetails.Add(item441);
            var item442 = new SeedOrderDetail()
            {
                OrderDetailID = 442,
                OrderID = 10413,
                ProductID = 76,
                Quantity = 14
            };
            orderDetails.Add(item442);
            var item443 = new SeedOrderDetail()
            {
                OrderDetailID = 443,
                OrderID = 10414,
                ProductID = 19,
                Quantity = 18
            };
            orderDetails.Add(item443);
            var item444 = new SeedOrderDetail()
            {
                OrderDetailID = 444,
                OrderID = 10414,
                ProductID = 33,
                Quantity = 50
            };
            orderDetails.Add(item444);
            var item445 = new SeedOrderDetail()
            {
                OrderDetailID = 445,
                OrderID = 10415,
                ProductID = 17,
                Quantity = 2
            };
            orderDetails.Add(item445);
            var item446 = new SeedOrderDetail()
            {
                OrderDetailID = 446,
                OrderID = 10415,
                ProductID = 33,
                Quantity = 20
            };
            orderDetails.Add(item446);
            var item447 = new SeedOrderDetail()
            {
                OrderDetailID = 447,
                OrderID = 10416,
                ProductID = 19,
                Quantity = 20
            };
            orderDetails.Add(item447);
            var item448 = new SeedOrderDetail()
            {
                OrderDetailID = 448,
                OrderID = 10416,
                ProductID = 53,
                Quantity = 10
            };
            orderDetails.Add(item448);
            var item449 = new SeedOrderDetail()
            {
                OrderDetailID = 449,
                OrderID = 10416,
                ProductID = 57,
                Quantity = 20
            };
            orderDetails.Add(item449);
            var item450 = new SeedOrderDetail()
            {
                OrderDetailID = 450,
                OrderID = 10417,
                ProductID = 38,
                Quantity = 50
            };
            orderDetails.Add(item450);
            var item451 = new SeedOrderDetail()
            {
                OrderDetailID = 451,
                OrderID = 10417,
                ProductID = 46,
                Quantity = 2
            };
            orderDetails.Add(item451);
            var item452 = new SeedOrderDetail()
            {
                OrderDetailID = 452,
                OrderID = 10417,
                ProductID = 68,
                Quantity = 36
            };
            orderDetails.Add(item452);
            var item453 = new SeedOrderDetail()
            {
                OrderDetailID = 453,
                OrderID = 10417,
                ProductID = 77,
                Quantity = 35
            };
            orderDetails.Add(item453);
            var item454 = new SeedOrderDetail()
            {
                OrderDetailID = 454,
                OrderID = 10418,
                ProductID = 2,
                Quantity = 60
            };
            orderDetails.Add(item454);
            var item455 = new SeedOrderDetail()
            {
                OrderDetailID = 455,
                OrderID = 10418,
                ProductID = 47,
                Quantity = 55
            };
            orderDetails.Add(item455);
            var item456 = new SeedOrderDetail()
            {
                OrderDetailID = 456,
                OrderID = 10418,
                ProductID = 61,
                Quantity = 16
            };
            orderDetails.Add(item456);
            var item457 = new SeedOrderDetail()
            {
                OrderDetailID = 457,
                OrderID = 10418,
                ProductID = 74,
                Quantity = 15
            };
            orderDetails.Add(item457);
            var item458 = new SeedOrderDetail()
            {
                OrderDetailID = 458,
                OrderID = 10419,
                ProductID = 60,
                Quantity = 60
            };
            orderDetails.Add(item458);
            var item459 = new SeedOrderDetail()
            {
                OrderDetailID = 459,
                OrderID = 10419,
                ProductID = 69,
                Quantity = 20
            };
            orderDetails.Add(item459);
            var item460 = new SeedOrderDetail()
            {
                OrderDetailID = 460,
                OrderID = 10420,
                ProductID = 9,
                Quantity = 20
            };
            orderDetails.Add(item460);
            var item461 = new SeedOrderDetail()
            {
                OrderDetailID = 461,
                OrderID = 10420,
                ProductID = 13,
                Quantity = 2
            };
            orderDetails.Add(item461);
            var item462 = new SeedOrderDetail()
            {
                OrderDetailID = 462,
                OrderID = 10420,
                ProductID = 70,
                Quantity = 8
            };
            orderDetails.Add(item462);
            var item463 = new SeedOrderDetail()
            {
                OrderDetailID = 463,
                OrderID = 10420,
                ProductID = 73,
                Quantity = 20
            };
            orderDetails.Add(item463);
            var item464 = new SeedOrderDetail()
            {
                OrderDetailID = 464,
                OrderID = 10421,
                ProductID = 19,
                Quantity = 4
            };
            orderDetails.Add(item464);
            var item465 = new SeedOrderDetail()
            {
                OrderDetailID = 465,
                OrderID = 10421,
                ProductID = 26,
                Quantity = 30
            };
            orderDetails.Add(item465);
            var item466 = new SeedOrderDetail()
            {
                OrderDetailID = 466,
                OrderID = 10421,
                ProductID = 53,
                Quantity = 15
            };
            orderDetails.Add(item466);
            var item467 = new SeedOrderDetail()
            {
                OrderDetailID = 467,
                OrderID = 10421,
                ProductID = 77,
                Quantity = 10
            };
            orderDetails.Add(item467);
            var item468 = new SeedOrderDetail()
            {
                OrderDetailID = 468,
                OrderID = 10422,
                ProductID = 26,
                Quantity = 2
            };
            orderDetails.Add(item468);
            var item469 = new SeedOrderDetail()
            {
                OrderDetailID = 469,
                OrderID = 10423,
                ProductID = 31,
                Quantity = 14
            };
            orderDetails.Add(item469);
            var item470 = new SeedOrderDetail()
            {
                OrderDetailID = 470,
                OrderID = 10423,
                ProductID = 59,
                Quantity = 20
            };
            orderDetails.Add(item470);
            var item471 = new SeedOrderDetail()
            {
                OrderDetailID = 471,
                OrderID = 10424,
                ProductID = 35,
                Quantity = 60
            };
            orderDetails.Add(item471);
            var item472 = new SeedOrderDetail()
            {
                OrderDetailID = 472,
                OrderID = 10424,
                ProductID = 38,
                Quantity = 49
            };
            orderDetails.Add(item472);
            var item473 = new SeedOrderDetail()
            {
                OrderDetailID = 473,
                OrderID = 10424,
                ProductID = 68,
                Quantity = 30
            };
            orderDetails.Add(item473);
            var item474 = new SeedOrderDetail()
            {
                OrderDetailID = 474,
                OrderID = 10425,
                ProductID = 55,
                Quantity = 10
            };
            orderDetails.Add(item474);
            var item475 = new SeedOrderDetail()
            {
                OrderDetailID = 475,
                OrderID = 10425,
                ProductID = 76,
                Quantity = 20
            };
            orderDetails.Add(item475);
            var item476 = new SeedOrderDetail()
            {
                OrderDetailID = 476,
                OrderID = 10426,
                ProductID = 56,
                Quantity = 5
            };
            orderDetails.Add(item476);
            var item477 = new SeedOrderDetail()
            {
                OrderDetailID = 477,
                OrderID = 10426,
                ProductID = 64,
                Quantity = 7
            };
            orderDetails.Add(item477);
            var item478 = new SeedOrderDetail()
            {
                OrderDetailID = 478,
                OrderID = 10427,
                ProductID = 14,
                Quantity = 35
            };
            orderDetails.Add(item478);
            var item479 = new SeedOrderDetail()
            {
                OrderDetailID = 479,
                OrderID = 10428,
                ProductID = 46,
                Quantity = 20
            };
            orderDetails.Add(item479);
            var item480 = new SeedOrderDetail()
            {
                OrderDetailID = 480,
                OrderID = 10429,
                ProductID = 50,
                Quantity = 40
            };
            orderDetails.Add(item480);
            var item481 = new SeedOrderDetail()
            {
                OrderDetailID = 481,
                OrderID = 10429,
                ProductID = 63,
                Quantity = 35
            };
            orderDetails.Add(item481);
            var item482 = new SeedOrderDetail()
            {
                OrderDetailID = 482,
                OrderID = 10430,
                ProductID = 17,
                Quantity = 45
            };
            orderDetails.Add(item482);
            var item483 = new SeedOrderDetail()
            {
                OrderDetailID = 483,
                OrderID = 10430,
                ProductID = 21,
                Quantity = 50
            };
            orderDetails.Add(item483);
            var item484 = new SeedOrderDetail()
            {
                OrderDetailID = 484,
                OrderID = 10430,
                ProductID = 56,
                Quantity = 30
            };
            orderDetails.Add(item484);
            var item485 = new SeedOrderDetail()
            {
                OrderDetailID = 485,
                OrderID = 10430,
                ProductID = 59,
                Quantity = 70
            };
            orderDetails.Add(item485);
            var item486 = new SeedOrderDetail()
            {
                OrderDetailID = 486,
                OrderID = 10431,
                ProductID = 17,
                Quantity = 50
            };
            orderDetails.Add(item486);
            var item487 = new SeedOrderDetail()
            {
                OrderDetailID = 487,
                OrderID = 10431,
                ProductID = 40,
                Quantity = 50
            };
            orderDetails.Add(item487);
            var item488 = new SeedOrderDetail()
            {
                OrderDetailID = 488,
                OrderID = 10431,
                ProductID = 47,
                Quantity = 30
            };
            orderDetails.Add(item488);
            var item489 = new SeedOrderDetail()
            {
                OrderDetailID = 489,
                OrderID = 10432,
                ProductID = 26,
                Quantity = 10
            };
            orderDetails.Add(item489);
            var item490 = new SeedOrderDetail()
            {
                OrderDetailID = 490,
                OrderID = 10432,
                ProductID = 54,
                Quantity = 40
            };
            orderDetails.Add(item490);
            var item491 = new SeedOrderDetail()
            {
                OrderDetailID = 491,
                OrderID = 10433,
                ProductID = 56,
                Quantity = 28
            };
            orderDetails.Add(item491);
            var item492 = new SeedOrderDetail()
            {
                OrderDetailID = 492,
                OrderID = 10434,
                ProductID = 11,
                Quantity = 6
            };
            orderDetails.Add(item492);
            var item493 = new SeedOrderDetail()
            {
                OrderDetailID = 493,
                OrderID = 10434,
                ProductID = 76,
                Quantity = 18
            };
            orderDetails.Add(item493);
            var item494 = new SeedOrderDetail()
            {
                OrderDetailID = 494,
                OrderID = 10435,
                ProductID = 2,
                Quantity = 10
            };
            orderDetails.Add(item494);
            var item495 = new SeedOrderDetail()
            {
                OrderDetailID = 495,
                OrderID = 10435,
                ProductID = 22,
                Quantity = 12
            };
            orderDetails.Add(item495);
            var item496 = new SeedOrderDetail()
            {
                OrderDetailID = 496,
                OrderID = 10435,
                ProductID = 72,
                Quantity = 10
            };
            orderDetails.Add(item496);
            var item497 = new SeedOrderDetail()
            {
                OrderDetailID = 497,
                OrderID = 10436,
                ProductID = 46,
                Quantity = 5
            };
            orderDetails.Add(item497);
            var item498 = new SeedOrderDetail()
            {
                OrderDetailID = 498,
                OrderID = 10436,
                ProductID = 56,
                Quantity = 40
            };
            orderDetails.Add(item498);
            var item499 = new SeedOrderDetail()
            {
                OrderDetailID = 499,
                OrderID = 10436,
                ProductID = 64,
                Quantity = 30
            };
            orderDetails.Add(item499);
            var item500 = new SeedOrderDetail()
            {
                OrderDetailID = 500,
                OrderID = 10436,
                ProductID = 75,
                Quantity = 24
            };
            orderDetails.Add(item500);
            var item501 = new SeedOrderDetail()
            {
                OrderDetailID = 501,
                OrderID = 10437,
                ProductID = 53,
                Quantity = 15
            };
            orderDetails.Add(item501);
            var item502 = new SeedOrderDetail()
            {
                OrderDetailID = 502,
                OrderID = 10438,
                ProductID = 19,
                Quantity = 15
            };
            orderDetails.Add(item502);
            var item503 = new SeedOrderDetail()
            {
                OrderDetailID = 503,
                OrderID = 10438,
                ProductID = 34,
                Quantity = 20
            };
            orderDetails.Add(item503);
            var item504 = new SeedOrderDetail()
            {
                OrderDetailID = 504,
                OrderID = 10438,
                ProductID = 57,
                Quantity = 15
            };
            orderDetails.Add(item504);
            var item505 = new SeedOrderDetail()
            {
                OrderDetailID = 505,
                OrderID = 10439,
                ProductID = 12,
                Quantity = 15
            };
            orderDetails.Add(item505);
            var item506 = new SeedOrderDetail()
            {
                OrderDetailID = 506,
                OrderID = 10439,
                ProductID = 16,
                Quantity = 16
            };
            orderDetails.Add(item506);
            var item507 = new SeedOrderDetail()
            {
                OrderDetailID = 507,
                OrderID = 10439,
                ProductID = 64,
                Quantity = 6
            };
            orderDetails.Add(item507);
            var item508 = new SeedOrderDetail()
            {
                OrderDetailID = 508,
                OrderID = 10439,
                ProductID = 74,
                Quantity = 30
            };
            orderDetails.Add(item508);
            var item509 = new SeedOrderDetail()
            {
                OrderDetailID = 509,
                OrderID = 10440,
                ProductID = 2,
                Quantity = 45
            };
            orderDetails.Add(item509);
            var item510 = new SeedOrderDetail()
            {
                OrderDetailID = 510,
                OrderID = 10440,
                ProductID = 16,
                Quantity = 49
            };
            orderDetails.Add(item510);
            var item511 = new SeedOrderDetail()
            {
                OrderDetailID = 511,
                OrderID = 10440,
                ProductID = 29,
                Quantity = 24
            };
            orderDetails.Add(item511);
            var item512 = new SeedOrderDetail()
            {
                OrderDetailID = 512,
                OrderID = 10440,
                ProductID = 61,
                Quantity = 90
            };
            orderDetails.Add(item512);
            var item513 = new SeedOrderDetail()
            {
                OrderDetailID = 513,
                OrderID = 10441,
                ProductID = 27,
                Quantity = 50
            };
            orderDetails.Add(item513);
            var item514 = new SeedOrderDetail()
            {
                OrderDetailID = 514,
                OrderID = 10442,
                ProductID = 11,
                Quantity = 30
            };
            orderDetails.Add(item514);
            var item515 = new SeedOrderDetail()
            {
                OrderDetailID = 515,
                OrderID = 10442,
                ProductID = 54,
                Quantity = 80
            };
            orderDetails.Add(item515);
            var item516 = new SeedOrderDetail()
            {
                OrderDetailID = 516,
                OrderID = 10442,
                ProductID = 66,
                Quantity = 60
            };
            orderDetails.Add(item516);
            var item517 = new SeedOrderDetail()
            {
                OrderDetailID = 517,
                OrderID = 10443,
                ProductID = 11,
                Quantity = 6
            };
            orderDetails.Add(item517);
            var item518 = new SeedOrderDetail()
            {
                OrderDetailID = 518,
                OrderID = 10443,
                ProductID = 28,
                Quantity = 12
            };
            orderDetails.Add(item518);
            return orderDetails;
        }

        public static List<SeedOrder> GetOrders()
        {
            List<SeedOrder> orders = new List<SeedOrder>();
            var item10248 = new SeedOrder()
            {
                OrderID = 10248,
                CustomerID = 90,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-07-04"),
                ShipperID = 3
            };
            orders.Add(item10248);
            var item10249 = new SeedOrder()
            {
                OrderID = 10249,
                CustomerID = 81,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-07-05"),
                ShipperID = 1
            };
            orders.Add(item10249);
            var item10250 = new SeedOrder()
            {
                OrderID = 10250,
                CustomerID = 34,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-08"),
                ShipperID = 2
            };
            orders.Add(item10250);
            var item10251 = new SeedOrder()
            {
                OrderID = 10251,
                CustomerID = 84,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-07-08"),
                ShipperID = 1
            };
            orders.Add(item10251);
            var item10252 = new SeedOrder()
            {
                OrderID = 10252,
                CustomerID = 76,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-09"),
                ShipperID = 2
            };
            orders.Add(item10252);
            var item10253 = new SeedOrder()
            {
                OrderID = 10253,
                CustomerID = 34,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-07-10"),
                ShipperID = 2
            };
            orders.Add(item10253);
            var item10254 = new SeedOrder()
            {
                OrderID = 10254,
                CustomerID = 14,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-07-11"),
                ShipperID = 2
            };
            orders.Add(item10254);
            var item10255 = new SeedOrder()
            {
                OrderID = 10255,
                CustomerID = 68,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1996-07-12"),
                ShipperID = 3
            };
            orders.Add(item10255);
            var item10256 = new SeedOrder()
            {
                OrderID = 10256,
                CustomerID = 88,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-07-15"),
                ShipperID = 2
            };
            orders.Add(item10256);
            var item10257 = new SeedOrder()
            {
                OrderID = 10257,
                CustomerID = 35,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-16"),
                ShipperID = 3
            };
            orders.Add(item10257);
            var item10258 = new SeedOrder()
            {
                OrderID = 10258,
                CustomerID = 20,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-07-17"),
                ShipperID = 1
            };
            orders.Add(item10258);
            var item10259 = new SeedOrder()
            {
                OrderID = 10259,
                CustomerID = 13,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-18"),
                ShipperID = 3
            };
            orders.Add(item10259);
            var item10260 = new SeedOrder()
            {
                OrderID = 10260,
                CustomerID = 55,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-19"),
                ShipperID = 1
            };
            orders.Add(item10260);
            var item10261 = new SeedOrder()
            {
                OrderID = 10261,
                CustomerID = 61,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-19"),
                ShipperID = 2
            };
            orders.Add(item10261);
            var item10262 = new SeedOrder()
            {
                OrderID = 10262,
                CustomerID = 65,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-07-22"),
                ShipperID = 3
            };
            orders.Add(item10262);
            var item10263 = new SeedOrder()
            {
                OrderID = 10263,
                CustomerID = 20,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1996-07-23"),
                ShipperID = 3
            };
            orders.Add(item10263);
            var item10264 = new SeedOrder()
            {
                OrderID = 10264,
                CustomerID = 24,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-07-24"),
                ShipperID = 3
            };
            orders.Add(item10264);
            var item10265 = new SeedOrder()
            {
                OrderID = 10265,
                CustomerID = 7,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-07-25"),
                ShipperID = 1
            };
            orders.Add(item10265);
            var item10266 = new SeedOrder()
            {
                OrderID = 10266,
                CustomerID = 87,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-07-26"),
                ShipperID = 3
            };
            orders.Add(item10266);
            var item10267 = new SeedOrder()
            {
                OrderID = 10267,
                CustomerID = 25,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-07-29"),
                ShipperID = 1
            };
            orders.Add(item10267);
            var item10268 = new SeedOrder()
            {
                OrderID = 10268,
                CustomerID = 33,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-07-30"),
                ShipperID = 3
            };
            orders.Add(item10268);
            var item10269 = new SeedOrder()
            {
                OrderID = 10269,
                CustomerID = 89,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-07-31"),
                ShipperID = 1
            };
            orders.Add(item10269);
            var item10270 = new SeedOrder()
            {
                OrderID = 10270,
                CustomerID = 87,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-08-01"),
                ShipperID = 1
            };
            orders.Add(item10270);
            var item10271 = new SeedOrder()
            {
                OrderID = 10271,
                CustomerID = 75,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-08-01"),
                ShipperID = 2
            };
            orders.Add(item10271);
            var item10272 = new SeedOrder()
            {
                OrderID = 10272,
                CustomerID = 65,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-08-02"),
                ShipperID = 2
            };
            orders.Add(item10272);
            var item10273 = new SeedOrder()
            {
                OrderID = 10273,
                CustomerID = 63,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-08-05"),
                ShipperID = 3
            };
            orders.Add(item10273);
            var item10274 = new SeedOrder()
            {
                OrderID = 10274,
                CustomerID = 85,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-08-06"),
                ShipperID = 1
            };
            orders.Add(item10274);
            var item10275 = new SeedOrder()
            {
                OrderID = 10275,
                CustomerID = 49,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-08-07"),
                ShipperID = 1
            };
            orders.Add(item10275);
            var item10276 = new SeedOrder()
            {
                OrderID = 10276,
                CustomerID = 80,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-08"),
                ShipperID = 3
            };
            orders.Add(item10276);
            var item10277 = new SeedOrder()
            {
                OrderID = 10277,
                CustomerID = 52,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-08-09"),
                ShipperID = 3
            };
            orders.Add(item10277);
            var item10278 = new SeedOrder()
            {
                OrderID = 10278,
                CustomerID = 5,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-12"),
                ShipperID = 2
            };
            orders.Add(item10278);
            var item10279 = new SeedOrder()
            {
                OrderID = 10279,
                CustomerID = 44,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-13"),
                ShipperID = 2
            };
            orders.Add(item10279);
            var item10280 = new SeedOrder()
            {
                OrderID = 10280,
                CustomerID = 5,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-08-14"),
                ShipperID = 1
            };
            orders.Add(item10280);
            var item10281 = new SeedOrder()
            {
                OrderID = 10281,
                CustomerID = 69,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-08-14"),
                ShipperID = 1
            };
            orders.Add(item10281);
            var item10282 = new SeedOrder()
            {
                OrderID = 10282,
                CustomerID = 69,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-08-15"),
                ShipperID = 1
            };
            orders.Add(item10282);
            var item10283 = new SeedOrder()
            {
                OrderID = 10283,
                CustomerID = 46,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-08-16"),
                ShipperID = 3
            };
            orders.Add(item10283);
            var item10284 = new SeedOrder()
            {
                OrderID = 10284,
                CustomerID = 44,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-08-19"),
                ShipperID = 1
            };
            orders.Add(item10284);
            var item10285 = new SeedOrder()
            {
                OrderID = 10285,
                CustomerID = 63,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-08-20"),
                ShipperID = 2
            };
            orders.Add(item10285);
            var item10286 = new SeedOrder()
            {
                OrderID = 10286,
                CustomerID = 63,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-21"),
                ShipperID = 3
            };
            orders.Add(item10286);
            var item10287 = new SeedOrder()
            {
                OrderID = 10287,
                CustomerID = 67,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-22"),
                ShipperID = 3
            };
            orders.Add(item10287);
            var item10288 = new SeedOrder()
            {
                OrderID = 10288,
                CustomerID = 66,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-08-23"),
                ShipperID = 1
            };
            orders.Add(item10288);
            var item10289 = new SeedOrder()
            {
                OrderID = 10289,
                CustomerID = 11,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-08-26"),
                ShipperID = 3
            };
            orders.Add(item10289);
            var item10290 = new SeedOrder()
            {
                OrderID = 10290,
                CustomerID = 15,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-08-27"),
                ShipperID = 1
            };
            orders.Add(item10290);
            var item10291 = new SeedOrder()
            {
                OrderID = 10291,
                CustomerID = 61,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-08-27"),
                ShipperID = 2
            };
            orders.Add(item10291);
            var item10292 = new SeedOrder()
            {
                OrderID = 10292,
                CustomerID = 81,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-08-28"),
                ShipperID = 2
            };
            orders.Add(item10292);
            var item10293 = new SeedOrder()
            {
                OrderID = 10293,
                CustomerID = 80,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-08-29"),
                ShipperID = 3
            };
            orders.Add(item10293);
            var item10294 = new SeedOrder()
            {
                OrderID = 10294,
                CustomerID = 65,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-08-30"),
                ShipperID = 2
            };
            orders.Add(item10294);
            var item10295 = new SeedOrder()
            {
                OrderID = 10295,
                CustomerID = 85,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-09-02"),
                ShipperID = 2
            };
            orders.Add(item10295);
            var item10296 = new SeedOrder()
            {
                OrderID = 10296,
                CustomerID = 46,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-09-03"),
                ShipperID = 1
            };
            orders.Add(item10296);
            var item10297 = new SeedOrder()
            {
                OrderID = 10297,
                CustomerID = 7,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-09-04"),
                ShipperID = 2
            };
            orders.Add(item10297);
            var item10298 = new SeedOrder()
            {
                OrderID = 10298,
                CustomerID = 37,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-09-05"),
                ShipperID = 2
            };
            orders.Add(item10298);
            var item10299 = new SeedOrder()
            {
                OrderID = 10299,
                CustomerID = 67,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-09-06"),
                ShipperID = 2
            };
            orders.Add(item10299);
            var item10300 = new SeedOrder()
            {
                OrderID = 10300,
                CustomerID = 49,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-09-09"),
                ShipperID = 2
            };
            orders.Add(item10300);
            var item10301 = new SeedOrder()
            {
                OrderID = 10301,
                CustomerID = 86,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-09-09"),
                ShipperID = 2
            };
            orders.Add(item10301);
            var item10302 = new SeedOrder()
            {
                OrderID = 10302,
                CustomerID = 76,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-09-10"),
                ShipperID = 2
            };
            orders.Add(item10302);
            var item10303 = new SeedOrder()
            {
                OrderID = 10303,
                CustomerID = 30,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-09-11"),
                ShipperID = 2
            };
            orders.Add(item10303);
            var item10304 = new SeedOrder()
            {
                OrderID = 10304,
                CustomerID = 80,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-09-12"),
                ShipperID = 2
            };
            orders.Add(item10304);
            var item10305 = new SeedOrder()
            {
                OrderID = 10305,
                CustomerID = 55,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-09-13"),
                ShipperID = 3
            };
            orders.Add(item10305);
            var item10306 = new SeedOrder()
            {
                OrderID = 10306,
                CustomerID = 69,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-09-16"),
                ShipperID = 3
            };
            orders.Add(item10306);
            var item10307 = new SeedOrder()
            {
                OrderID = 10307,
                CustomerID = 48,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-09-17"),
                ShipperID = 2
            };
            orders.Add(item10307);
            var item10308 = new SeedOrder()
            {
                OrderID = 10308,
                CustomerID = 2,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-09-18"),
                ShipperID = 3
            };
            orders.Add(item10308);
            var item10309 = new SeedOrder()
            {
                OrderID = 10309,
                CustomerID = 37,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-09-19"),
                ShipperID = 1
            };
            orders.Add(item10309);
            var item10310 = new SeedOrder()
            {
                OrderID = 10310,
                CustomerID = 77,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-09-20"),
                ShipperID = 2
            };
            orders.Add(item10310);
            var item10311 = new SeedOrder()
            {
                OrderID = 10311,
                CustomerID = 18,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-09-20"),
                ShipperID = 3
            };
            orders.Add(item10311);
            var item10312 = new SeedOrder()
            {
                OrderID = 10312,
                CustomerID = 86,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-09-23"),
                ShipperID = 2
            };
            orders.Add(item10312);
            var item10313 = new SeedOrder()
            {
                OrderID = 10313,
                CustomerID = 63,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-09-24"),
                ShipperID = 2
            };
            orders.Add(item10313);
            var item10314 = new SeedOrder()
            {
                OrderID = 10314,
                CustomerID = 65,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-09-25"),
                ShipperID = 2
            };
            orders.Add(item10314);
            var item10315 = new SeedOrder()
            {
                OrderID = 10315,
                CustomerID = 38,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-09-26"),
                ShipperID = 2
            };
            orders.Add(item10315);
            var item10316 = new SeedOrder()
            {
                OrderID = 10316,
                CustomerID = 65,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-09-27"),
                ShipperID = 3
            };
            orders.Add(item10316);
            var item10317 = new SeedOrder()
            {
                OrderID = 10317,
                CustomerID = 48,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-09-30"),
                ShipperID = 1
            };
            orders.Add(item10317);
            var item10318 = new SeedOrder()
            {
                OrderID = 10318,
                CustomerID = 38,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-10-01"),
                ShipperID = 2
            };
            orders.Add(item10318);
            var item10319 = new SeedOrder()
            {
                OrderID = 10319,
                CustomerID = 80,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-10-02"),
                ShipperID = 3
            };
            orders.Add(item10319);
            var item10320 = new SeedOrder()
            {
                OrderID = 10320,
                CustomerID = 87,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-10-03"),
                ShipperID = 3
            };
            orders.Add(item10320);
            var item10321 = new SeedOrder()
            {
                OrderID = 10321,
                CustomerID = 38,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-10-03"),
                ShipperID = 2
            };
            orders.Add(item10321);
            var item10322 = new SeedOrder()
            {
                OrderID = 10322,
                CustomerID = 58,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-10-04"),
                ShipperID = 3
            };
            orders.Add(item10322);
            var item10323 = new SeedOrder()
            {
                OrderID = 10323,
                CustomerID = 39,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-07"),
                ShipperID = 1
            };
            orders.Add(item10323);
            var item10324 = new SeedOrder()
            {
                OrderID = 10324,
                CustomerID = 71,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1996-10-08"),
                ShipperID = 1
            };
            orders.Add(item10324);
            var item10325 = new SeedOrder()
            {
                OrderID = 10325,
                CustomerID = 39,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-10-09"),
                ShipperID = 3
            };
            orders.Add(item10325);
            var item10326 = new SeedOrder()
            {
                OrderID = 10326,
                CustomerID = 8,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-10"),
                ShipperID = 2
            };
            orders.Add(item10326);
            var item10327 = new SeedOrder()
            {
                OrderID = 10327,
                CustomerID = 24,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-10-11"),
                ShipperID = 1
            };
            orders.Add(item10327);
            var item10328 = new SeedOrder()
            {
                OrderID = 10328,
                CustomerID = 28,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-14"),
                ShipperID = 3
            };
            orders.Add(item10328);
            var item10329 = new SeedOrder()
            {
                OrderID = 10329,
                CustomerID = 75,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-15"),
                ShipperID = 2
            };
            orders.Add(item10329);
            var item10330 = new SeedOrder()
            {
                OrderID = 10330,
                CustomerID = 46,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-10-16"),
                ShipperID = 1
            };
            orders.Add(item10330);
            var item10331 = new SeedOrder()
            {
                OrderID = 10331,
                CustomerID = 9,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1996-10-16"),
                ShipperID = 1
            };
            orders.Add(item10331);
            var item10332 = new SeedOrder()
            {
                OrderID = 10332,
                CustomerID = 51,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-10-17"),
                ShipperID = 2
            };
            orders.Add(item10332);
            var item10333 = new SeedOrder()
            {
                OrderID = 10333,
                CustomerID = 87,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-10-18"),
                ShipperID = 3
            };
            orders.Add(item10333);
            var item10334 = new SeedOrder()
            {
                OrderID = 10334,
                CustomerID = 84,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-10-21"),
                ShipperID = 2
            };
            orders.Add(item10334);
            var item10335 = new SeedOrder()
            {
                OrderID = 10335,
                CustomerID = 37,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-10-22"),
                ShipperID = 2
            };
            orders.Add(item10335);
            var item10336 = new SeedOrder()
            {
                OrderID = 10336,
                CustomerID = 60,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-10-23"),
                ShipperID = 2
            };
            orders.Add(item10336);
            var item10337 = new SeedOrder()
            {
                OrderID = 10337,
                CustomerID = 25,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-24"),
                ShipperID = 3
            };
            orders.Add(item10337);
            var item10338 = new SeedOrder()
            {
                OrderID = 10338,
                CustomerID = 55,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-25"),
                ShipperID = 3
            };
            orders.Add(item10338);
            var item10339 = new SeedOrder()
            {
                OrderID = 10339,
                CustomerID = 51,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-10-28"),
                ShipperID = 2
            };
            orders.Add(item10339);
            var item10340 = new SeedOrder()
            {
                OrderID = 10340,
                CustomerID = 9,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-10-29"),
                ShipperID = 3
            };
            orders.Add(item10340);
            var item10341 = new SeedOrder()
            {
                OrderID = 10341,
                CustomerID = 73,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-10-29"),
                ShipperID = 3
            };
            orders.Add(item10341);
            var item10342 = new SeedOrder()
            {
                OrderID = 10342,
                CustomerID = 25,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-30"),
                ShipperID = 2
            };
            orders.Add(item10342);
            var item10343 = new SeedOrder()
            {
                OrderID = 10343,
                CustomerID = 44,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-10-31"),
                ShipperID = 1
            };
            orders.Add(item10343);
            var item10344 = new SeedOrder()
            {
                OrderID = 10344,
                CustomerID = 89,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-11-01"),
                ShipperID = 2
            };
            orders.Add(item10344);
            var item10345 = new SeedOrder()
            {
                OrderID = 10345,
                CustomerID = 63,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-11-04"),
                ShipperID = 2
            };
            orders.Add(item10345);
            var item10346 = new SeedOrder()
            {
                OrderID = 10346,
                CustomerID = 65,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-11-05"),
                ShipperID = 3
            };
            orders.Add(item10346);
            var item10347 = new SeedOrder()
            {
                OrderID = 10347,
                CustomerID = 21,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-11-06"),
                ShipperID = 3
            };
            orders.Add(item10347);
            var item10348 = new SeedOrder()
            {
                OrderID = 10348,
                CustomerID = 86,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-11-07"),
                ShipperID = 2
            };
            orders.Add(item10348);
            var item10349 = new SeedOrder()
            {
                OrderID = 10349,
                CustomerID = 75,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-11-08"),
                ShipperID = 1
            };
            orders.Add(item10349);
            var item10350 = new SeedOrder()
            {
                OrderID = 10350,
                CustomerID = 41,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-11-11"),
                ShipperID = 2
            };
            orders.Add(item10350);
            var item10351 = new SeedOrder()
            {
                OrderID = 10351,
                CustomerID = 20,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-11-11"),
                ShipperID = 1
            };
            orders.Add(item10351);
            var item10352 = new SeedOrder()
            {
                OrderID = 10352,
                CustomerID = 28,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-11-12"),
                ShipperID = 3
            };
            orders.Add(item10352);
            var item10353 = new SeedOrder()
            {
                OrderID = 10353,
                CustomerID = 59,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-11-13"),
                ShipperID = 3
            };
            orders.Add(item10353);
            var item10354 = new SeedOrder()
            {
                OrderID = 10354,
                CustomerID = 58,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-11-14"),
                ShipperID = 3
            };
            orders.Add(item10354);
            var item10355 = new SeedOrder()
            {
                OrderID = 10355,
                CustomerID = 4,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-11-15"),
                ShipperID = 1
            };
            orders.Add(item10355);
            var item10356 = new SeedOrder()
            {
                OrderID = 10356,
                CustomerID = 86,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-11-18"),
                ShipperID = 2
            };
            orders.Add(item10356);
            var item10357 = new SeedOrder()
            {
                OrderID = 10357,
                CustomerID = 46,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-11-19"),
                ShipperID = 3
            };
            orders.Add(item10357);
            var item10358 = new SeedOrder()
            {
                OrderID = 10358,
                CustomerID = 41,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-11-20"),
                ShipperID = 1
            };
            orders.Add(item10358);
            var item10359 = new SeedOrder()
            {
                OrderID = 10359,
                CustomerID = 72,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-11-21"),
                ShipperID = 3
            };
            orders.Add(item10359);
            var item10360 = new SeedOrder()
            {
                OrderID = 10360,
                CustomerID = 7,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-11-22"),
                ShipperID = 3
            };
            orders.Add(item10360);
            var item10361 = new SeedOrder()
            {
                OrderID = 10361,
                CustomerID = 63,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-11-22"),
                ShipperID = 2
            };
            orders.Add(item10361);
            var item10362 = new SeedOrder()
            {
                OrderID = 10362,
                CustomerID = 9,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-11-25"),
                ShipperID = 1
            };
            orders.Add(item10362);
            var item10363 = new SeedOrder()
            {
                OrderID = 10363,
                CustomerID = 17,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-11-26"),
                ShipperID = 3
            };
            orders.Add(item10363);
            var item10364 = new SeedOrder()
            {
                OrderID = 10364,
                CustomerID = 19,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-11-26"),
                ShipperID = 1
            };
            orders.Add(item10364);
            var item10365 = new SeedOrder()
            {
                OrderID = 10365,
                CustomerID = 3,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-11-27"),
                ShipperID = 2
            };
            orders.Add(item10365);
            var item10366 = new SeedOrder()
            {
                OrderID = 10366,
                CustomerID = 29,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-11-28"),
                ShipperID = 2
            };
            orders.Add(item10366);
            var item10367 = new SeedOrder()
            {
                OrderID = 10367,
                CustomerID = 83,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1996-11-28"),
                ShipperID = 3
            };
            orders.Add(item10367);
            var item10368 = new SeedOrder()
            {
                OrderID = 10368,
                CustomerID = 20,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-11-29"),
                ShipperID = 2
            };
            orders.Add(item10368);
            var item10369 = new SeedOrder()
            {
                OrderID = 10369,
                CustomerID = 75,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-12-02"),
                ShipperID = 2
            };
            orders.Add(item10369);
            var item10370 = new SeedOrder()
            {
                OrderID = 10370,
                CustomerID = 14,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-12-03"),
                ShipperID = 2
            };
            orders.Add(item10370);
            var item10371 = new SeedOrder()
            {
                OrderID = 10371,
                CustomerID = 41,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-03"),
                ShipperID = 1
            };
            orders.Add(item10371);
            var item10372 = new SeedOrder()
            {
                OrderID = 10372,
                CustomerID = 62,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-12-04"),
                ShipperID = 2
            };
            orders.Add(item10372);
            var item10373 = new SeedOrder()
            {
                OrderID = 10373,
                CustomerID = 37,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-12-05"),
                ShipperID = 3
            };
            orders.Add(item10373);
            var item10374 = new SeedOrder()
            {
                OrderID = 10374,
                CustomerID = 91,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-05"),
                ShipperID = 3
            };
            orders.Add(item10374);
            var item10375 = new SeedOrder()
            {
                OrderID = 10375,
                CustomerID = 36,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-12-06"),
                ShipperID = 2
            };
            orders.Add(item10375);
            var item10376 = new SeedOrder()
            {
                OrderID = 10376,
                CustomerID = 51,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-09"),
                ShipperID = 2
            };
            orders.Add(item10376);
            var item10377 = new SeedOrder()
            {
                OrderID = 10377,
                CustomerID = 72,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-09"),
                ShipperID = 3
            };
            orders.Add(item10377);
            var item10378 = new SeedOrder()
            {
                OrderID = 10378,
                CustomerID = 24,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-12-10"),
                ShipperID = 3
            };
            orders.Add(item10378);
            var item10379 = new SeedOrder()
            {
                OrderID = 10379,
                CustomerID = 61,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-12-11"),
                ShipperID = 1
            };
            orders.Add(item10379);
            var item10380 = new SeedOrder()
            {
                OrderID = 10380,
                CustomerID = 37,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-12-12"),
                ShipperID = 3
            };
            orders.Add(item10380);
            var item10381 = new SeedOrder()
            {
                OrderID = 10381,
                CustomerID = 46,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-12-12"),
                ShipperID = 3
            };
            orders.Add(item10381);
            var item10382 = new SeedOrder()
            {
                OrderID = 10382,
                CustomerID = 20,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-12-13"),
                ShipperID = 1
            };
            orders.Add(item10382);
            var item10383 = new SeedOrder()
            {
                OrderID = 10383,
                CustomerID = 4,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-12-16"),
                ShipperID = 3
            };
            orders.Add(item10383);
            var item10384 = new SeedOrder()
            {
                OrderID = 10384,
                CustomerID = 5,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-12-16"),
                ShipperID = 3
            };
            orders.Add(item10384);
            var item10385 = new SeedOrder()
            {
                OrderID = 10385,
                CustomerID = 75,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-17"),
                ShipperID = 2
            };
            orders.Add(item10385);
            var item10386 = new SeedOrder()
            {
                OrderID = 10386,
                CustomerID = 21,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1996-12-18"),
                ShipperID = 3
            };
            orders.Add(item10386);
            var item10387 = new SeedOrder()
            {
                OrderID = 10387,
                CustomerID = 70,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-18"),
                ShipperID = 2
            };
            orders.Add(item10387);
            var item10388 = new SeedOrder()
            {
                OrderID = 10388,
                CustomerID = 72,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-12-19"),
                ShipperID = 1
            };
            orders.Add(item10388);
            var item10389 = new SeedOrder()
            {
                OrderID = 10389,
                CustomerID = 10,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1996-12-20"),
                ShipperID = 2
            };
            orders.Add(item10389);
            var item10390 = new SeedOrder()
            {
                OrderID = 10390,
                CustomerID = 20,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-12-23"),
                ShipperID = 1
            };
            orders.Add(item10390);
            var item10391 = new SeedOrder()
            {
                OrderID = 10391,
                CustomerID = 17,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1996-12-23"),
                ShipperID = 3
            };
            orders.Add(item10391);
            var item10392 = new SeedOrder()
            {
                OrderID = 10392,
                CustomerID = 59,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-12-24"),
                ShipperID = 3
            };
            orders.Add(item10392);
            var item10393 = new SeedOrder()
            {
                OrderID = 10393,
                CustomerID = 71,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-25"),
                ShipperID = 3
            };
            orders.Add(item10393);
            var item10394 = new SeedOrder()
            {
                OrderID = 10394,
                CustomerID = 36,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-25"),
                ShipperID = 3
            };
            orders.Add(item10394);
            var item10395 = new SeedOrder()
            {
                OrderID = 10395,
                CustomerID = 35,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1996-12-26"),
                ShipperID = 1
            };
            orders.Add(item10395);
            var item10396 = new SeedOrder()
            {
                OrderID = 10396,
                CustomerID = 25,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1996-12-27"),
                ShipperID = 3
            };
            orders.Add(item10396);
            var item10397 = new SeedOrder()
            {
                OrderID = 10397,
                CustomerID = 60,
                EmployeeID = 5,
                OrderDate = DateTime.Parse("1996-12-27"),
                ShipperID = 1
            };
            orders.Add(item10397);
            var item10398 = new SeedOrder()
            {
                OrderID = 10398,
                CustomerID = 71,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1996-12-30"),
                ShipperID = 3
            };
            orders.Add(item10398);
            var item10399 = new SeedOrder()
            {
                OrderID = 10399,
                CustomerID = 83,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1996-12-31"),
                ShipperID = 3
            };
            orders.Add(item10399);
            var item10400 = new SeedOrder()
            {
                OrderID = 10400,
                CustomerID = 19,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1997-01-01"),
                ShipperID = 3
            };
            orders.Add(item10400);
            var item10401 = new SeedOrder()
            {
                OrderID = 10401,
                CustomerID = 65,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1997-01-01"),
                ShipperID = 1
            };
            orders.Add(item10401);
            var item10402 = new SeedOrder()
            {
                OrderID = 10402,
                CustomerID = 20,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-01-02"),
                ShipperID = 2
            };
            orders.Add(item10402);
            var item10403 = new SeedOrder()
            {
                OrderID = 10403,
                CustomerID = 20,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-03"),
                ShipperID = 3
            };
            orders.Add(item10403);
            var item10404 = new SeedOrder()
            {
                OrderID = 10404,
                CustomerID = 49,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1997-01-03"),
                ShipperID = 1
            };
            orders.Add(item10404);
            var item10405 = new SeedOrder()
            {
                OrderID = 10405,
                CustomerID = 47,
                EmployeeID = 1,
                OrderDate = DateTime.Parse("1997-01-06"),
                ShipperID = 1
            };
            orders.Add(item10405);
            var item10406 = new SeedOrder()
            {
                OrderID = 10406,
                CustomerID = 62,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1997-01-07"),
                ShipperID = 1
            };
            orders.Add(item10406);
            var item10407 = new SeedOrder()
            {
                OrderID = 10407,
                CustomerID = 56,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1997-01-07"),
                ShipperID = 2
            };
            orders.Add(item10407);
            var item10408 = new SeedOrder()
            {
                OrderID = 10408,
                CustomerID = 23,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-01-08"),
                ShipperID = 1
            };
            orders.Add(item10408);
            var item10409 = new SeedOrder()
            {
                OrderID = 10409,
                CustomerID = 54,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-09"),
                ShipperID = 1
            };
            orders.Add(item10409);
            var item10410 = new SeedOrder()
            {
                OrderID = 10410,
                CustomerID = 10,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-10"),
                ShipperID = 3
            };
            orders.Add(item10410);
            var item10411 = new SeedOrder()
            {
                OrderID = 10411,
                CustomerID = 10,
                EmployeeID = 9,
                OrderDate = DateTime.Parse("1997-01-10"),
                ShipperID = 3
            };
            orders.Add(item10411);
            var item10412 = new SeedOrder()
            {
                OrderID = 10412,
                CustomerID = 87,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-01-13"),
                ShipperID = 2
            };
            orders.Add(item10412);
            var item10413 = new SeedOrder()
            {
                OrderID = 10413,
                CustomerID = 41,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-14"),
                ShipperID = 2
            };
            orders.Add(item10413);
            var item10414 = new SeedOrder()
            {
                OrderID = 10414,
                CustomerID = 21,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1997-01-14"),
                ShipperID = 3
            };
            orders.Add(item10414);
            var item10415 = new SeedOrder()
            {
                OrderID = 10415,
                CustomerID = 36,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-15"),
                ShipperID = 1
            };
            orders.Add(item10415);
            var item10416 = new SeedOrder()
            {
                OrderID = 10416,
                CustomerID = 87,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-01-16"),
                ShipperID = 3
            };
            orders.Add(item10416);
            var item10417 = new SeedOrder()
            {
                OrderID = 10417,
                CustomerID = 73,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-16"),
                ShipperID = 3
            };
            orders.Add(item10417);
            var item10418 = new SeedOrder()
            {
                OrderID = 10418,
                CustomerID = 63,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-17"),
                ShipperID = 1
            };
            orders.Add(item10418);
            var item10419 = new SeedOrder()
            {
                OrderID = 10419,
                CustomerID = 68,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-20"),
                ShipperID = 2
            };
            orders.Add(item10419);
            var item10420 = new SeedOrder()
            {
                OrderID = 10420,
                CustomerID = 88,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-21"),
                ShipperID = 1
            };
            orders.Add(item10420);
            var item10421 = new SeedOrder()
            {
                OrderID = 10421,
                CustomerID = 61,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-01-21"),
                ShipperID = 1
            };
            orders.Add(item10421);
            var item10422 = new SeedOrder()
            {
                OrderID = 10422,
                CustomerID = 27,
                EmployeeID = 2,
                OrderDate = DateTime.Parse("1997-01-22"),
                ShipperID = 1
            };
            orders.Add(item10422);
            var item10423 = new SeedOrder()
            {
                OrderID = 10423,
                CustomerID = 31,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1997-01-23"),
                ShipperID = 3
            };
            orders.Add(item10423);
            var item10424 = new SeedOrder()
            {
                OrderID = 10424,
                CustomerID = 51,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1997-01-23"),
                ShipperID = 2
            };
            orders.Add(item10424);
            var item10425 = new SeedOrder()
            {
                OrderID = 10425,
                CustomerID = 41,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1997-01-24"),
                ShipperID = 2
            };
            orders.Add(item10425);
            var item10426 = new SeedOrder()
            {
                OrderID = 10426,
                CustomerID = 29,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-27"),
                ShipperID = 1
            };
            orders.Add(item10426);
            var item10427 = new SeedOrder()
            {
                OrderID = 10427,
                CustomerID = 59,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-27"),
                ShipperID = 2
            };
            orders.Add(item10427);
            var item10428 = new SeedOrder()
            {
                OrderID = 10428,
                CustomerID = 66,
                EmployeeID = 7,
                OrderDate = DateTime.Parse("1997-01-28"),
                ShipperID = 1
            };
            orders.Add(item10428);
            var item10429 = new SeedOrder()
            {
                OrderID = 10429,
                CustomerID = 37,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-29"),
                ShipperID = 2
            };
            orders.Add(item10429);
            var item10430 = new SeedOrder()
            {
                OrderID = 10430,
                CustomerID = 20,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-30"),
                ShipperID = 1
            };
            orders.Add(item10430);
            var item10431 = new SeedOrder()
            {
                OrderID = 10431,
                CustomerID = 10,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-01-30"),
                ShipperID = 2
            };
            orders.Add(item10431);
            var item10432 = new SeedOrder()
            {
                OrderID = 10432,
                CustomerID = 75,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-01-31"),
                ShipperID = 2
            };
            orders.Add(item10432);
            var item10433 = new SeedOrder()
            {
                OrderID = 10433,
                CustomerID = 60,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-03"),
                ShipperID = 3
            };
            orders.Add(item10433);
            var item10434 = new SeedOrder()
            {
                OrderID = 10434,
                CustomerID = 24,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-03"),
                ShipperID = 2
            };
            orders.Add(item10434);
            var item10435 = new SeedOrder()
            {
                OrderID = 10435,
                CustomerID = 16,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-02-04"),
                ShipperID = 2
            };
            orders.Add(item10435);
            var item10436 = new SeedOrder()
            {
                OrderID = 10436,
                CustomerID = 7,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-05"),
                ShipperID = 2
            };
            orders.Add(item10436);
            var item10437 = new SeedOrder()
            {
                OrderID = 10437,
                CustomerID = 87,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-02-05"),
                ShipperID = 1
            };
            orders.Add(item10437);
            var item10438 = new SeedOrder()
            {
                OrderID = 10438,
                CustomerID = 79,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-06"),
                ShipperID = 2
            };
            orders.Add(item10438);
            var item10439 = new SeedOrder()
            {
                OrderID = 10439,
                CustomerID = 51,
                EmployeeID = 6,
                OrderDate = DateTime.Parse("1997-02-07"),
                ShipperID = 3
            };
            orders.Add(item10439);
            var item10440 = new SeedOrder()
            {
                OrderID = 10440,
                CustomerID = 71,
                EmployeeID = 4,
                OrderDate = DateTime.Parse("1997-02-10"),
                ShipperID = 2
            };
            orders.Add(item10440);
            var item10441 = new SeedOrder()
            {
                OrderID = 10441,
                CustomerID = 55,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-10"),
                ShipperID = 2
            };
            orders.Add(item10441);
            var item10442 = new SeedOrder()
            {
                OrderID = 10442,
                CustomerID = 20,
                EmployeeID = 3,
                OrderDate = DateTime.Parse("1997-02-11"),
                ShipperID = 2
            };
            orders.Add(item10442);
            var item10443 = new SeedOrder()
            {
                OrderID = 10443,
                CustomerID = 66,
                EmployeeID = 8,
                OrderDate = DateTime.Parse("1997-02-12"),
                ShipperID = 1
            };
            orders.Add(item10443);
            return orders;
        }

        public static List<object> GetProducts()
        {
            List<object> products = new List<object>();
            var item1 = new Product()
            {
                ProductID = 1,
                ProductName = "Chais",
                SupplierID = 1,
                CategoryID = 1,
                Unit = "10 boxes x 20 bags",
                Price = 18
            };
            products.Add(item1);
            var item2 = new Product()
            {
                ProductID = 2,
                ProductName = "Chang",
                SupplierID = 1,
                CategoryID = 1,
                Unit = "24 - 12 oz bottles",
                Price = 19
            };
            products.Add(item2);
            var item3 = new Product()
            {
                ProductID = 3,
                ProductName = "Aniseed Syrup",
                SupplierID = 1,
                CategoryID = 2,
                Unit = "12 - 550 ml bottles",
                Price = 10
            };
            products.Add(item3);
            var item4 = new Product()
            {
                ProductID = 4,
                ProductName = "Chef Anton's Cajun Seasoning",
                SupplierID = 2,
                CategoryID = 2,
                Unit = "48 - 6 oz jars",
                Price = 22
            };
            products.Add(item4);
            var item5 = new Product()
            {
                ProductID = 5,
                ProductName = "Chef Anton's Gumbo Mix",
                SupplierID = 2,
                CategoryID = 2,
                Unit = "36 boxes",
                Price = 21.35
            };
            products.Add(item5);
            var item6 = new Product()
            {
                ProductID = 6,
                ProductName = "Grandma's Boysenberry Spread",
                SupplierID = 3,
                CategoryID = 2,
                Unit = "12 - 8 oz jars",
                Price = 25
            };
            products.Add(item6);
            var item7 = new Product()
            {
                ProductID = 7,
                ProductName = "Uncle Bob's Organic Dried Pears",
                SupplierID = 3,
                CategoryID = 7,
                Unit = "12 - 1 lb pkgs.",
                Price = 30
            };
            products.Add(item7);
            var item8 = new Product()
            {
                ProductID = 8,
                ProductName = "Northwoods Cranberry Sauce",
                SupplierID = 3,
                CategoryID = 2,
                Unit = "12 - 12 oz jars",
                Price = 40
            };
            products.Add(item8);
            var item9 = new Product()
            {
                ProductID = 9,
                ProductName = "Mishi Kobe Niku",
                SupplierID = 4,
                CategoryID = 6,
                Unit = "18 - 500 g pkgs.",
                Price = 97
            };
            products.Add(item9);
            var item10 = new Product()
            {
                ProductID = 10,
                ProductName = "Ikura",
                SupplierID = 4,
                CategoryID = 8,
                Unit = "12 - 200 ml jars",
                Price = 31
            };
            products.Add(item10);
            var item11 = new Product()
            {
                ProductID = 11,
                ProductName = "Queso Cabrales",
                SupplierID = 5,
                CategoryID = 4,
                Unit = "1 kg pkg.",
                Price = 21
            };
            products.Add(item11);
            var item12 = new Product()
            {
                ProductID = 12,
                ProductName = "Queso Manchego La Pastora",
                SupplierID = 5,
                CategoryID = 4,
                Unit = "10 - 500 g pkgs.",
                Price = 38
            };
            products.Add(item12);
            var item13 = new Product()
            {
                ProductID = 13,
                ProductName = "Konbu",
                SupplierID = 6,
                CategoryID = 8,
                Unit = "2 kg box",
                Price = 6
            };
            products.Add(item13);
            var item14 = new Product()
            {
                ProductID = 14,
                ProductName = "Tofu",
                SupplierID = 6,
                CategoryID = 7,
                Unit = "40 - 100 g pkgs.",
                Price = 23.25
            };
            products.Add(item14);
            var item15 = new Product()
            {
                ProductID = 15,
                ProductName = "Genen Shouyu",
                SupplierID = 6,
                CategoryID = 2,
                Unit = "24 - 250 ml bottles",
                Price = 15.5
            };
            products.Add(item15);
            var item16 = new Product()
            {
                ProductID = 16,
                ProductName = "Pavlova",
                SupplierID = 7,
                CategoryID = 3,
                Unit = "32 - 500 g boxes",
                Price = 17.45
            };
            products.Add(item16);
            var item17 = new Product()
            {
                ProductID = 17,
                ProductName = "Alice Mutton",
                SupplierID = 7,
                CategoryID = 6,
                Unit = "20 - 1 kg tins",
                Price = 39
            };
            products.Add(item17);
            var item18 = new Product()
            {
                ProductID = 18,
                ProductName = "Carnarvon Tigers",
                SupplierID = 7,
                CategoryID = 8,
                Unit = "16 kg pkg.",
                Price = 62.5
            };
            products.Add(item18);
            var item19 = new Product()
            {
                ProductID = 19,
                ProductName = "Teatime Chocolate Biscuits",
                SupplierID = 8,
                CategoryID = 3,
                Unit = "10 boxes x 12 pieces",
                Price = 9.2
            };
            products.Add(item19);
            var item20 = new Product()
            {
                ProductID = 20,
                ProductName = "Sir Rodney's Marmalade",
                SupplierID = 8,
                CategoryID = 3,
                Unit = "30 gift boxes",
                Price = 81
            };
            products.Add(item20);
            var item21 = new Product()
            {
                ProductID = 21,
                ProductName = "Sir Rodney's Scones",
                SupplierID = 8,
                CategoryID = 3,
                Unit = "24 pkgs. x 4 pieces",
                Price = 10
            };
            products.Add(item21);
            var item22 = new Product()
            {
                ProductID = 22,
                ProductName = "Gustaf's Knäckebröd",
                SupplierID = 9,
                CategoryID = 5,
                Unit = "24 - 500 g pkgs.",
                Price = 21
            };
            products.Add(item22);
            var item23 = new Product()
            {
                ProductID = 23,
                ProductName = "Tunnbröd",
                SupplierID = 9,
                CategoryID = 5,
                Unit = "12 - 250 g pkgs.",
                Price = 9
            };
            products.Add(item23);
            var item24 = new Product()
            {
                ProductID = 24,
                ProductName = "Guaraná Fantástica",
                SupplierID = 10,
                CategoryID = 1,
                Unit = "12 - 355 ml cans",
                Price = 4.5
            };
            products.Add(item24);
            var item25 = new Product()
            {
                ProductID = 25,
                ProductName = "NuNuCa Nuß-Nougat-Creme",
                SupplierID = 11,
                CategoryID = 3,
                Unit = "20 - 450 g glasses",
                Price = 14
            };
            products.Add(item25);
            var item26 = new Product()
            {
                ProductID = 26,
                ProductName = "Gumbär Gummibärchen",
                SupplierID = 11,
                CategoryID = 3,
                Unit = "100 - 250 g bags",
                Price = 31.23
            };
            products.Add(item26);
            var item27 = new Product()
            {
                ProductID = 27,
                ProductName = "Schoggi Schokolade",
                SupplierID = 11,
                CategoryID = 3,
                Unit = "100 - 100 g pieces",
                Price = 43.9
            };
            products.Add(item27);
            var item28 = new Product()
            {
                ProductID = 28,
                ProductName = "Rössle Sauerkraut",
                SupplierID = 12,
                CategoryID = 7,
                Unit = "25 - 825 g cans",
                Price = 45.6
            };
            products.Add(item28);
            var item29 = new Product()
            {
                ProductID = 29,
                ProductName = "Thüringer Rostbratwurst",
                SupplierID = 12,
                CategoryID = 6,
                Unit = "50 bags x 30 sausgs.",
                Price = 123.79
            };
            products.Add(item29);
            var item30 = new Product()
            {
                ProductID = 30,
                ProductName = "Nord-Ost Matjeshering",
                SupplierID = 13,
                CategoryID = 8,
                Unit = "10 - 200 g glasses",
                Price = 25.89
            };
            products.Add(item30);
            var item31 = new Product()
            {
                ProductID = 31,
                ProductName = "Gorgonzola Telino",
                SupplierID = 14,
                CategoryID = 4,
                Unit = "12 - 100 g pkgs",
                Price = 12.5
            };
            products.Add(item31);
            var item32 = new Product()
            {
                ProductID = 32,
                ProductName = "Mascarpone Fabioli",
                SupplierID = 14,
                CategoryID = 4,
                Unit = "24 - 200 g pkgs.",
                Price = 32
            };
            products.Add(item32);
            var item33 = new Product()
            {
                ProductID = 33,
                ProductName = "Geitost",
                SupplierID = 15,
                CategoryID = 4,
                Unit = "500 g",
                Price = 2.5
            };
            products.Add(item33);
            var item34 = new Product()
            {
                ProductID = 34,
                ProductName = "Sasquatch Ale",
                SupplierID = 16,
                CategoryID = 1,
                Unit = "24 - 12 oz bottles",
                Price = 14
            };
            products.Add(item34);
            var item35 = new Product()
            {
                ProductID = 35,
                ProductName = "Steeleye Stout",
                SupplierID = 16,
                CategoryID = 1,
                Unit = "24 - 12 oz bottles",
                Price = 18
            };
            products.Add(item35);
            var item36 = new Product()
            {
                ProductID = 36,
                ProductName = "Inlagd Sill",
                SupplierID = 17,
                CategoryID = 8,
                Unit = "24 - 250 g jars",
                Price = 19
            };
            products.Add(item36);
            var item37 = new Product()
            {
                ProductID = 37,
                ProductName = "Gravad lax",
                SupplierID = 17,
                CategoryID = 8,
                Unit = "12 - 500 g pkgs.",
                Price = 26
            };
            products.Add(item37);
            var item38 = new Product()
            {
                ProductID = 38,
                ProductName = "Côte de Blaye",
                SupplierID = 18,
                CategoryID = 1,
                Unit = "12 - 75 cl bottles",
                Price = 263.5
            };
            products.Add(item38);
            var item39 = new Product()
            {
                ProductID = 39,
                ProductName = "Chartreuse verte",
                SupplierID = 18,
                CategoryID = 1,
                Unit = "750 cc per bottle",
                Price = 18
            };
            products.Add(item39);
            var item40 = new Product()
            {
                ProductID = 40,
                ProductName = "Boston Crab Meat",
                SupplierID = 19,
                CategoryID = 8,
                Unit = "24 - 4 oz tins",
                Price = 18.4
            };
            products.Add(item40);
            var item41 = new Product()
            {
                ProductID = 41,
                ProductName = "Jack's New England Clam Chowder",
                SupplierID = 19,
                CategoryID = 8,
                Unit = "12 - 12 oz cans",
                Price = 9.65
            };
            products.Add(item41);
            var item42 = new Product()
            {
                ProductID = 42,
                ProductName = "Singaporean Hokkien Fried Mee",
                SupplierID = 20,
                CategoryID = 5,
                Unit = "32 - 1 kg pkgs.",
                Price = 14
            };
            products.Add(item42);
            var item43 = new Product()
            {
                ProductID = 43,
                ProductName = "Ipoh Coffee",
                SupplierID = 20,
                CategoryID = 1,
                Unit = "16 - 500 g tins",
                Price = 46
            };
            products.Add(item43);
            var item44 = new Product()
            {
                ProductID = 44,
                ProductName = "Gula Malacca",
                SupplierID = 20,
                CategoryID = 2,
                Unit = "20 - 2 kg bags",
                Price = 19.45
            };
            products.Add(item44);
            var item45 = new Product()
            {
                ProductID = 45,
                ProductName = "Røgede sild",
                SupplierID = 21,
                CategoryID = 8,
                Unit = "1k pkg.",
                Price = 9.5
            };
            products.Add(item45);
            var item46 = new Product()
            {
                ProductID = 46,
                ProductName = "Spegesild",
                SupplierID = 21,
                CategoryID = 8,
                Unit = "4 - 450 g glasses",
                Price = 12
            };
            products.Add(item46);
            var item47 = new Product()
            {
                ProductID = 47,
                ProductName = "Zaanse koeken",
                SupplierID = 22,
                CategoryID = 3,
                Unit = "10 - 4 oz boxes",
                Price = 9.5
            };
            products.Add(item47);
            var item48 = new Product()
            {
                ProductID = 48,
                ProductName = "Chocolade",
                SupplierID = 22,
                CategoryID = 3,
                Unit = "10 pkgs.",
                Price = 12.75
            };
            products.Add(item48);
            var item49 = new Product()
            {
                ProductID = 49,
                ProductName = "Maxilaku",
                SupplierID = 23,
                CategoryID = 3,
                Unit = "24 - 50 g pkgs.",
                Price = 20
            };
            products.Add(item49);
            var item50 = new Product()
            {
                ProductID = 50,
                ProductName = "Valkoinen suklaa",
                SupplierID = 23,
                CategoryID = 3,
                Unit = "12 - 100 g bars",
                Price = 16.25
            };
            products.Add(item50);
            var item51 = new Product()
            {
                ProductID = 51,
                ProductName = "Manjimup Dried Apples",
                SupplierID = 24,
                CategoryID = 7,
                Unit = "50 - 300 g pkgs.",
                Price = 53
            };
            products.Add(item51);
            var item52 = new Product()
            {
                ProductID = 52,
                ProductName = "Filo Mix",
                SupplierID = 24,
                CategoryID = 5,
                Unit = "16 - 2 kg boxes",
                Price = 7
            };
            products.Add(item52);
            var item53 = new Product()
            {
                ProductID = 53,
                ProductName = "Perth Pasties",
                SupplierID = 24,
                CategoryID = 6,
                Unit = "48 pieces",
                Price = 32.8
            };
            products.Add(item53);
            var item54 = new Product()
            {
                ProductID = 54,
                ProductName = "Tourtière",
                SupplierID = 25,
                CategoryID = 6,
                Unit = "16 pies",
                Price = 7.45
            };
            products.Add(item54);
            var item55 = new Product()
            {
                ProductID = 55,
                ProductName = "Pâté chinois",
                SupplierID = 25,
                CategoryID = 6,
                Unit = "24 boxes x 2 pies",
                Price = 24
            };
            products.Add(item55);
            var item56 = new Product()
            {
                ProductID = 56,
                ProductName = "Gnocchi di nonna Alice",
                SupplierID = 26,
                CategoryID = 5,
                Unit = "24 - 250 g pkgs.",
                Price = 38
            };
            products.Add(item56);
            var item57 = new Product()
            {
                ProductID = 57,
                ProductName = "Ravioli Angelo",
                SupplierID = 26,
                CategoryID = 5,
                Unit = "24 - 250 g pkgs.",
                Price = 19.5
            };
            products.Add(item57);
            var item58 = new Product()
            {
                ProductID = 58,
                ProductName = "Escargots de Bourgogne",
                SupplierID = 27,
                CategoryID = 8,
                Unit = "24 pieces",
                Price = 13.25
            };
            products.Add(item58);
            var item59 = new Product()
            {
                ProductID = 59,
                ProductName = "Raclette Courdavault",
                SupplierID = 28,
                CategoryID = 4,
                Unit = "5 kg pkg.",
                Price = 55
            };
            products.Add(item59);
            var item60 = new Product()
            {
                ProductID = 60,
                ProductName = "Camembert Pierrot",
                SupplierID = 28,
                CategoryID = 4,
                Unit = "15 - 300 g rounds",
                Price = 34
            };
            products.Add(item60);
            var item61 = new Product()
            {
                ProductID = 61,
                ProductName = "Sirop d'érable",
                SupplierID = 29,
                CategoryID = 2,
                Unit = "24 - 500 ml bottles",
                Price = 28.5
            };
            products.Add(item61);
            var item62 = new Product()
            {
                ProductID = 62,
                ProductName = "Tarte au sucre",
                SupplierID = 29,
                CategoryID = 3,
                Unit = "48 pies",
                Price = 49.3
            };
            products.Add(item62);
            var item63 = new Product()
            {
                ProductID = 63,
                ProductName = "Vegie-spread",
                SupplierID = 7,
                CategoryID = 2,
                Unit = "15 - 625 g jars",
                Price = 43.9
            };
            products.Add(item63);
            var item64 = new Product()
            {
                ProductID = 64,
                ProductName = "Wimmers gute Semmelknödel",
                SupplierID = 12,
                CategoryID = 5,
                Unit = "20 bags x 4 pieces",
                Price = 33.25
            };
            products.Add(item64);
            var item65 = new Product()
            {
                ProductID = 65,
                ProductName = "Louisiana Fiery Hot Pepper Sauce",
                SupplierID = 2,
                CategoryID = 2,
                Unit = "32 - 8 oz bottles",
                Price = 21.05
            };
            products.Add(item65);
            var item66 = new Product()
            {
                ProductID = 66,
                ProductName = "Louisiana Hot Spiced Okra",
                SupplierID = 2,
                CategoryID = 2,
                Unit = "24 - 8 oz jars",
                Price = 17
            };
            products.Add(item66);
            var item67 = new Product()
            {
                ProductID = 67,
                ProductName = "Laughing Lumberjack Lager",
                SupplierID = 16,
                CategoryID = 1,
                Unit = "24 - 12 oz bottles",
                Price = 14
            };
            products.Add(item67);
            var item68 = new Product()
            {
                ProductID = 68,
                ProductName = "Scottish Longbreads",
                SupplierID = 8,
                CategoryID = 3,
                Unit = "10 boxes x 8 pieces",
                Price = 12.5
            };
            products.Add(item68);
            var item69 = new Product()
            {
                ProductID = 69,
                ProductName = "Gudbrandsdalsost",
                SupplierID = 15,
                CategoryID = 4,
                Unit = "10 kg pkg.",
                Price = 36
            };
            products.Add(item69);
            var item70 = new Product()
            {
                ProductID = 70,
                ProductName = "Outback Lager",
                SupplierID = 7,
                CategoryID = 1,
                Unit = "24 - 355 ml bottles",
                Price = 15
            };
            products.Add(item70);
            var item71 = new Product()
            {
                ProductID = 71,
                ProductName = "Fløtemysost",
                SupplierID = 15,
                CategoryID = 4,
                Unit = "10 - 500 g pkgs.",
                Price = 21.5
            };
            products.Add(item71);
            var item72 = new Product()
            {
                ProductID = 72,
                ProductName = "Mozzarella di Giovanni",
                SupplierID = 14,
                CategoryID = 4,
                Unit = "24 - 200 g pkgs.",
                Price = 34.8
            };
            products.Add(item72);
            var item73 = new Product()
            {
                ProductID = 73,
                ProductName = "Röd Kaviar",
                SupplierID = 17,
                CategoryID = 8,
                Unit = "24 - 150 g jars",
                Price = 15
            };
            products.Add(item73);
            var item74 = new Product()
            {
                ProductID = 74,
                ProductName = "Longlife Tofu",
                SupplierID = 4,
                CategoryID = 7,
                Unit = "5 kg pkg.",
                Price = 10
            };
            products.Add(item74);
            var item75 = new Product()
            {
                ProductID = 75,
                ProductName = "Rhönbräu Klosterbier",
                SupplierID = 12,
                CategoryID = 1,
                Unit = "24 - 0.5 l bottles",
                Price = 7.75
            };
            products.Add(item75);
            var item76 = new Product()
            {
                ProductID = 76,
                ProductName = "Lakkalikööri",
                SupplierID = 23,
                CategoryID = 1,
                Unit = "500 ml",
                Price = 18
            };
            products.Add(item76);
            var item77 = new Product()
            {
                ProductID = 77,
                ProductName = "Original Frankfurter grüne Soße",
                SupplierID = 12,
                CategoryID = 2,
                Unit = "12 boxes",
                Price = 13
            };
            products.Add(item77);
            return products;
        }

        public static List<object> GetShippers()
        {
            List<object> shippers = new List<object>();
            var item1 = new Shipper()
            {
                ShipperID = 1,
                ShipperName = "Speedy Express",
                Phone = "(503) 555-9831"
            };
            shippers.Add(item1);
            var item2 = new Shipper()
            {
                ShipperID = 2,
                ShipperName = "United Package",
                Phone = "(503) 555-3199"
            };
            shippers.Add(item2);
            var item3 = new Shipper()
            {
                ShipperID = 3,
                ShipperName = "Federal Shipping",
                Phone = "(503) 555-9931"
            };
            shippers.Add(item3);
            return shippers;
        }

        public static List<object> GetSuppliers()
        {
            List<object> suppliers = new List<object>();
            var item1 = new Supplier()
            {
                SupplierID = 1,
                SupplierName = "Exotic Liquid",
                ContactName = "Charlotte Cooper",
                Address = "49 Gilbert St.",
                City = "Londona",
                PostalCode = "EC1 4SD",
                Country = "UK",
                Phone = "(171) 555-2222"
            };
            suppliers.Add(item1);
            var item2 = new Supplier()
            {
                SupplierID = 2,
                SupplierName = "New Orleans Cajun Delights",
                ContactName = "Shelley Burke",
                Address = "P.O. Box 78934",
                City = "New Orleans",
                PostalCode = "70117",
                Country = "USA",
                Phone = "(100) 555-4822"
            };
            suppliers.Add(item2);
            var item3 = new Supplier()
            {
                SupplierID = 3,
                SupplierName = "Grandma Kelly's Homestead",
                ContactName = "Regina Murphy",
                Address = "707 Oxford Rd.",
                City = "Ann Arbor",
                PostalCode = "48104",
                Country = "USA",
                Phone = "(313) 555-5735"
            };
            suppliers.Add(item3);
            var item4 = new Supplier()
            {
                SupplierID = 4,
                SupplierName = "Tokyo Traders",
                ContactName = "Yoshi Nagase",
                Address = "9-8 Sekimai Musashino-shi",
                City = "Tokyo",
                PostalCode = "100",
                Country = "Japan",
                Phone = "(03) 3555-5011"
            };
            suppliers.Add(item4);
            var item5 = new Supplier()
            {
                SupplierID = 5,
                SupplierName = "Cooperativa de Quesos 'Las Cabras'",
                ContactName = "Antonio del Valle Saavedra",
                Address = "Calle del Rosal 4",
                City = "Oviedo",
                PostalCode = "33007",
                Country = "Spain",
                Phone = "(98) 598 76 54"
            };
            suppliers.Add(item5);
            var item6 = new Supplier()
            {
                SupplierID = 6,
                SupplierName = "Mayumi's",
                ContactName = "Mayumi Ohno",
                Address = "92 Setsuko Chuo-ku",
                City = "Osaka",
                PostalCode = "545",
                Country = "Japan",
                Phone = "(06) 431-7877"
            };
            suppliers.Add(item6);
            var item7 = new Supplier()
            {
                SupplierID = 7,
                SupplierName = "Pavlova, Ltd.",
                ContactName = "Ian Devling",
                Address = "74 Rose St. Moonie Ponds",
                City = "Melbourne",
                PostalCode = "3058",
                Country = "Australia",
                Phone = "(03) 444-2343"
            };
            suppliers.Add(item7);
            var item8 = new Supplier()
            {
                SupplierID = 8,
                SupplierName = "Specialty Biscuits, Ltd.",
                ContactName = "Peter Wilson",
                Address = "29 King's Way",
                City = "Manchester",
                PostalCode = "M14 GSD",
                Country = "UK",
                Phone = "(161) 555-4448"
            };
            suppliers.Add(item8);
            var item9 = new Supplier()
            {
                SupplierID = 9,
                SupplierName = "PB Knäckebröd AB",
                ContactName = "Lars Peterson",
                Address = "Kaloadagatan 13",
                City = "Göteborg",
                PostalCode = "S-345 67",
                Country = "Sweden",
                Phone = "031-987 65 43"
            };
            suppliers.Add(item9);
            var item10 = new Supplier()
            {
                SupplierID = 10,
                SupplierName = "Refrescos Americanas LTDA",
                ContactName = "Carlos Diaz",
                Address = "Av. das Americanas 12.890",
                City = "São Paulo",
                PostalCode = "5442",
                Country = "Brazil",
                Phone = "(11) 555 4640"
            };
            suppliers.Add(item10);
            var item11 = new Supplier()
            {
                SupplierID = 11,
                SupplierName = "Heli Süßwaren GmbH & Co. KG",
                ContactName = "Petra Winkler",
                Address = "Tiergartenstraße 5",
                City = "Berlin",
                PostalCode = "10785",
                Country = "Germany",
                Phone = "(010) 9984510"
            };
            suppliers.Add(item11);
            var item12 = new Supplier()
            {
                SupplierID = 12,
                SupplierName = "Plutzer Lebensmittelgroßmärkte AG",
                ContactName = "Martin Bein",
                Address = "Bogenallee 51",
                City = "Frankfurt",
                PostalCode = "60439",
                Country = "Germany",
                Phone = "(069) 992755"
            };
            suppliers.Add(item12);
            var item13 = new Supplier()
            {
                SupplierID = 13,
                SupplierName = "Nord-Ost-Fisch Handelsgesellschaft mbH",
                ContactName = "Sven Petersen",
                Address = "Frahmredder 112a",
                City = "Cuxhaven",
                PostalCode = "27478",
                Country = "Germany",
                Phone = "(04721) 8713"
            };
            suppliers.Add(item13);
            var item14 = new Supplier()
            {
                SupplierID = 14,
                SupplierName = "Formaggi Fortini s.r.l.",
                ContactName = "Elio Rossi",
                Address = "Viale Dante, 75",
                City = "Ravenna",
                PostalCode = "48100",
                Country = "Italy",
                Phone = "(0544) 60323"
            };
            suppliers.Add(item14);
            var item15 = new Supplier()
            {
                SupplierID = 15,
                SupplierName = "Norske Meierier",
                ContactName = "Beate Vileid",
                Address = "Hatlevegen 5",
                City = "Sandvika",
                PostalCode = "1320",
                Country = "Norway",
                Phone = "(0)2-953010"
            };
            suppliers.Add(item15);
            var item16 = new Supplier()
            {
                SupplierID = 16,
                SupplierName = "Bigfoot Breweries",
                ContactName = "Cheryl Saylor",
                Address = "3400 - 8th Avenue Suite 210",
                City = "Bend",
                PostalCode = "97101",
                Country = "USA",
                Phone = "(503) 555-9931"
            };
            suppliers.Add(item16);
            var item17 = new Supplier()
            {
                SupplierID = 17,
                SupplierName = "Svensk Sjöföda AB",
                ContactName = "Michael Björn",
                Address = "Brovallavägen 231",
                City = "Stockholm",
                PostalCode = "S-123 45",
                Country = "Sweden",
                Phone = "08-123 45 67"
            };
            suppliers.Add(item17);
            var item18 = new Supplier()
            {
                SupplierID = 18,
                SupplierName = "Aux joyeux ecclésiastiques",
                ContactName = "Guylène Nodier",
                Address = "203, Rue des Francs-Bourgeois",
                City = "Paris",
                PostalCode = "75004",
                Country = "France",
                Phone = "(1) 03.83.00.68"
            };
            suppliers.Add(item18);
            var item19 = new Supplier()
            {
                SupplierID = 19,
                SupplierName = "New England Seafood Cannery",
                ContactName = "Robb Merchant",
                Address = "SeedOrder Processing Dept. 2100 Paul Revere Blvd.",
                City = "Boston",
                PostalCode = "02134",
                Country = "USA",
                Phone = "(617) 555-3267"
            };
            suppliers.Add(item19);
            var item20 = new Supplier()
            {
                SupplierID = 20,
                SupplierName = "Leka Trading",
                ContactName = "Chandra Leka",
                Address = "471 Serangoon Loop, Suite #402",
                City = "Singapore",
                PostalCode = "0512",
                Country = "Singapore",
                Phone = "555-8787"
            };
            suppliers.Add(item20);
            var item21 = new Supplier()
            {
                SupplierID = 21,
                SupplierName = "Lyngbysild",
                ContactName = "Niels Petersen",
                Address = "Lyngbysild Fiskebakken 10",
                City = "Lyngby",
                PostalCode = "2800",
                Country = "Denmark",
                Phone = "43844108"
            };
            suppliers.Add(item21);
            var item22 = new Supplier()
            {
                SupplierID = 22,
                SupplierName = "Zaanse Snoepfabriek",
                ContactName = "Dirk Luchte",
                Address = "Verkoop Rijnweg 22",
                City = "Zaandam",
                PostalCode = "9999 ZZ",
                Country = "Netherlands",
                Phone = "(12345) 1212"
            };
            suppliers.Add(item22);
            var item23 = new Supplier()
            {
                SupplierID = 23,
                SupplierName = "Karkki Oy",
                ContactName = "Anne Heikkonen",
                Address = "Valtakatu 12",
                City = "Lappeenranta",
                PostalCode = "53120",
                Country = "Finland",
                Phone = "(953) 10956"
            };
            suppliers.Add(item23);
            var item24 = new Supplier()
            {
                SupplierID = 24,
                SupplierName = "G'day, Mate",
                ContactName = "Wendy Mackenzie",
                Address = "170 Prince Edward Parade Hunter's Hill",
                City = "Sydney",
                PostalCode = "2042",
                Country = "Australia",
                Phone = "(02) 555-5914"
            };
            suppliers.Add(item24);
            var item25 = new Supplier()
            {
                SupplierID = 25,
                SupplierName = "Ma Maison",
                ContactName = "Jean-Guy Lauzon",
                Address = "2960 Rue St. Laurent",
                City = "Montréal",
                PostalCode = "H1J 1C3",
                Country = "Canada",
                Phone = "(514) 555-9022"
            };
            suppliers.Add(item25);
            var item26 = new Supplier()
            {
                SupplierID = 26,
                SupplierName = "Pasta Buttini s.r.l.",
                ContactName = "Giovanni Giudici",
                Address = "Via dei Gelsomini, 153",
                City = "Salerno",
                PostalCode = "84100",
                Country = "Italy",
                Phone = "(089) 6547665"
            };
            suppliers.Add(item26);
            var item27 = new Supplier()
            {
                SupplierID = 27,
                SupplierName = "Escargots Nouveaux",
                ContactName = "Marie Delamare",
                Address = "22, rue H. Voiron",
                City = "Montceau",
                PostalCode = "71300",
                Country = "France",
                Phone = "85.57.00.07"
            };
            suppliers.Add(item27);
            var item28 = new Supplier()
            {
                SupplierID = 28,
                SupplierName = "Gai pâturage",
                ContactName = "Eliane Noz",
                Address = "Bat. B 3, rue des Alpes",
                City = "Annecy",
                PostalCode = "74000",
                Country = "France",
                Phone = "38.76.98.06"
            };
            suppliers.Add(item28);
            var item29 = new Supplier()
            {
                SupplierID = 29,
                SupplierName = "Forêts d'érables",
                ContactName = "Chantal Goulet",
                Address = "148 rue Chasseur",
                City = "Ste-Hyacinthe",
                PostalCode = "J2S 7S8",
                Country = "Canada",
                Phone = "(514) 555-2955"
            };
            suppliers.Add(item29);
            return suppliers;
        }
    }
}
