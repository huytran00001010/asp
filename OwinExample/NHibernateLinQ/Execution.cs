﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using NHibernateLinQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ
{
    class Execution
    {
        public static void Select(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().ToList<Customer>();

                    var criteria = session.CreateCriteria<Customer>().List<Customer>();

                    var customers = session.CreateQuery("select c from Customer c").List<Customer>();

                    var queryOver = session.QueryOver<Customer>().List<Customer>();

                    IQuery sqlQuery = session.CreateSQLQuery("SELECT * FROM CUSTOMERS").AddEntity(typeof(Customer));
                    var sqlQuerys = sqlQuery.List<Customer>();
                    tx.Commit();
                }

            }
        }
        public static void SelectFields(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Select(c => new { c.CustomerID,c.CustomerName }).ToList();

                    var hql = session.CreateQuery("select c.CustomerID, c.CustomerName from Customer c").List();

                    var criteria = session.CreateCriteria<Customer>().SetProjection(Projections.ProjectionList()
                            .Add(Projections.Property("CustomerID"))
                            .Add(Projections.Property("CustomerName")))
                            .List();

                    var queryOver = session.QueryOver<Customer>().Select(c=> c.CustomerID, c=> c.CustomerName).List<object>();

                    IQuery native = session.CreateSQLQuery("SELECT CustomerID, CustomerName FROM CUSTOMERS");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void SelectDistinct(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Select(c => new { c.Country}).GroupBy(c => c.Country).Select(g => g.First()).ToList();
                    var hql = session.CreateQuery("select distinct (c.Country) from Customer c").List();
                    var criteria = session.CreateCriteria<Customer>()
                        .SetProjection(Projections.Distinct(Projections.Property("Country")))
                        .List();
                    var queryOver = session.QueryOver<Customer>()
                        .Select(Projections.Distinct(Projections.Property("Country")))
                        .List<object>();

                    var native = session.CreateSQLQuery("SELECT distinct (Country) FROM CUSTOMERS");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void SelectDistinctAndCount(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Select(c => new { c.Country }).GroupBy(c => c.Country).Select(g => g.First()).ToList().Count;
                    var hql = session.CreateQuery("select distinct (c.Country) from Customer c").List().Count;
                    var criteria = session.CreateCriteria<Customer>()
                        .SetProjection(Projections.Distinct(Projections.Property("Country")))
                        .List().Count;
                    var queryOver = session.QueryOver<Customer>()
                        .Select(Projections.Distinct(Projections.Property("Country")))
                        .List<object>().Count;

                    var native = session.CreateSQLQuery("SELECT count(distinct (Country)) FROM CUSTOMERS");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void WhereClause(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Where(c => c.Country == "Mexico").ToList<Customer>();
                    var hql = session.CreateQuery("select c from Customer c where c.Country = 'Mexico'").List();
                    var criteria = session.CreateCriteria<Customer>("c")
                        .Add(Restrictions.Eq("c.Country", "Mexico"))
                        .List();
                    var queryOver = session.QueryOver<Customer>()
                        .Where(c => c.Country == "Mexico")
                        .List<Customer>();

                    var native = session.CreateSQLQuery("SELECT * FROM CUSTOMERS where Country = 'Mexico'");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void WhereClause2(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Where(c => c.Country == "Germany" && (c.City== "Berlin" || c.City == "München")).ToList<Customer>();

                    var hql = session.CreateQuery("select c from Customer c where c.Country = 'Germany' AND (c.City = 'Berlin' OR c.City = 'München') ").List();

                    var criteria = session.CreateCriteria<Customer>("c")
                        .Add(Restrictions.And(Restrictions.Eq("c.Country", "Germany"),
                            Restrictions.Or(Restrictions.Eq("c.City", "Berlin"), Restrictions.Eq("c.City", "München"))))
                        .List();

                    var queryOver = session.QueryOver<Customer>()
                        .Where(c => c.Country == "Germany" && (c.City == "Berlin" || c.City == "München"))
                        .List<Customer>();
                    

                    var native = session.CreateSQLQuery("SELECT * FROM Customers WHERE Country = 'Germany' AND (City = 'Berlin' OR City = 'München') ");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void WhereClauseNULL(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().Where(c => c.PostalCode == null).ToList<Customer>();

                    var hql = session.CreateQuery("select c from Customer c where c.PostalCode is null ").List();

                    var criteria = session.CreateCriteria<Customer>("c")
                        .Add(Restrictions.IsNull("c.PostalCode"))
                        .List();

                    var queryOver = session.QueryOver<Customer>()
                        .Where(c => c.PostalCode == null)
                        .List<Customer>();

                    var native = session.CreateSQLQuery("SELECT * FROM Customers WHERE PostalCode IS NULL");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void SelectAndOrder(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var linq = session.Query<Customer>().OrderBy(c => c.Country).ThenByDescending(c => c.CustomerName).ToList<Customer>();

                    var hql = session.CreateQuery("select c from Customer c order by Country ASC, CustomerName DESC ").List();

                    var criteria = session.CreateCriteria<Customer>("c")
                        .AddOrder(NHibernate.Criterion.Order.Asc("c.Country"))
                        .AddOrder(NHibernate.Criterion.Order.Desc("c.CustomerName"))
                        .List();

                    var queryOver = session.QueryOver<Customer>()
                        .OrderBy(c => c.Country).Asc
                        .OrderBy(c => c.CustomerName).Desc
                        .List<Customer>();


                    var native = session.CreateSQLQuery("SELECT * FROM Customers ORDER BY Country ASC, CustomerName DESC");
                    var nativeResult = native.List<object>();
                    tx.Commit();
                }

            }
        }

        public static void UpdateCustomer(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var customer = session.Query<Customer>().Where(c => c.CustomerID == 32).FirstOrDefault();
                    customer.City = "Oslo";
                    customer.Country = "Norway";
                    session.Save(customer);
                    tx.Commit();
                    customer = session.Load<Customer>(customer.CustomerID);
                    tx.Commit();

                }

            }
        }

        public static void InsertCustomer(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {

                    var customer = new Customer
                    {
                        CustomerName = "Cardinal",
                        ContactName = "Tom B. Erichsen",
                        Address = "Skagen 21",
                        City = " Stavanger",
                        PostalCode = "4006",
                        Country = "Norway"
                    };
                    session.Save(customer);
                    tx.Commit();
                }

            }
        }

        public static void WhereInAndSubquery(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {

                    var linq = session.Query<Customer>()
                        .Where(c => session.Query<Supplier>().Select(s => s.Country).Contains(c.Country) )
                        .ToList<Customer>();

                    var hql = session.CreateQuery("SELECT c FROM Customer c WHERE c.Country IN(SELECT s.Country FROM Supplier s) ").List<Customer>();

                    var criteria = session.CreateCriteria<Customer>("c");
                    DetachedCriteria supplierSubquery = DetachedCriteria.For<Supplier>("s")
                    // Filter the Subquery
                    //.add(Restrictions.eq(s.DOMAIN, domain))
                    // SELECT  
                    .SetProjection(Projections.Property("s.Country") );

                    criteria.Add(Subqueries.PropertyIn("c.Country", supplierSubquery));
                    //criteria.SetResultTransformer(Transformers.AliasToBean(typeof(Customer)));
                    var criteriaResult = criteria.List();


                    var distinctSubQuery = QueryOver.Of<Supplier>()
                            .Select(Projections.Distinct(Projections.Property<Supplier>(s => s.Country)));
                    var queryOver = session.QueryOver<Customer>()
                        .WithSubquery.WhereProperty(c => c.Country).In(distinctSubQuery)
                        .List<Customer>();


                    var native = session.CreateSQLQuery("SELECT * FROM Customers WHERE Country IN(SELECT Country FROM Suppliers)");
                    native.SetResultTransformer(Transformers.AliasToBean(typeof(Customer)));
                    var nativeResult = native.List();

                    tx.Commit();
                }
            }
        }
    }
}
