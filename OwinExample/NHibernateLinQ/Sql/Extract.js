﻿
function formatInsert(rows, table) {

    var title = rows[0].getElementsByTagName("th");
    //console.log(title);
    var constructString = `SET IDENTITY_INSERT ${table} ON;
`
    constructString += `INSERT INTO ${table} (`;
    for (var j = 0; j < title.length; j++) {
        constructString += `${title[j].innerText}`;
        if (j < (title.length - 1)) {
            constructString += ` ,`;
        }
    }
    //console.log(rows);
    constructString += `) VALUES
 `;

    for (var i = 1; i < rows.length; i++) {
        var data = rows[i].getElementsByTagName("td");
        var rowData = "(";
        for (var j = 0; j < title.length; j++) {
            if (title[j].innerText.indexOf('ID') > -1 || title[j].innerText.indexOf('Quantity') > -1
                || title[j].innerText.indexOf('Price') > -1) {
                rowData += `${data[j].innerText}`;
            }
            else if (title[j].innerText.indexOf('Date') > -1) {
                rowData += `'${data[j].innerText.replace(/'/g, '"')}'`;
            }
            else {
                rowData += `'${data[j].innerText.replace(/'/g, '"')}'`;
            }

            if (j < (title.length - 1)) {
                rowData += ` ,`;
            }
        }
        if (i < (rows.length - 1)) {
            rowData += `),
   `;
        }
        else {
            rowData += `);
   `;
        }
        constructString += rowData;
    }
    constructString += `SET IDENTITY_INSERT ${table} OFF;`
    console.log(constructString);
    return constructString;
}

var wrapper = document.getElementById('yourDB');
var links = wrapper.getElementsByTagName('table')[0].rows

var l = 1;
(function loopLinks() {
    if (l < links.length) {
        var td = links[l].getElementsByTagName('td')[0];
        //console.log(td);

        td.click();
        setTimeout(function () {
            //console.log(document.getElementsByClassName("w3-table-all")[0]);
            var rows = document.getElementsByClassName("w3-table-all")[0].rows;

            var table = td.innerText;
            formatInsert(rows, table);
            l++;
            loopLinks();
        }, 1000);
    }
})();