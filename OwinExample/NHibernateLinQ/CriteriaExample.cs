﻿using NHibernate.Cfg;
using NHibernateLinQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ
{
    public class CriteriaExample
    {
        public static void QueryAll(Configuration cfg)
        {
            Initial.Seed(cfg);

            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var criteria = session.CreateCriteria<Employee>();
                    var employees = criteria.List<Employee>();
                    foreach (var e in employees)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", e.EmployeeID, e.FirstName, e.LastName);
                    }

                    tx.Commit();
                }
            }
        }

        public static void InsertBatch(Configuration cfg)
        {

        }

        public static void QueryComponentClass(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var criteria = session.CreateCriteria<Customer>();
                    var customers = criteria.List<Customer>();
                    foreach (var c in customers)
                    {
                        Console.WriteLine("{0} \t{1} \t{2} \t{3}", c.CustomerID, c.CustomerName, c.Address.Address, c.Address.City);
                    }

                    tx.Commit();
                }
            }
        }

        public static void CreateOrders(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var criteria = session.CreateCriteria<Customer>();
                    var customers = criteria.List<Customer>();
                    foreach (var c in customers)
                    {
                        Console.WriteLine(c);
                        Initial.GetOrders().ForEach(o =>
                        {
                            if (o.CustomerID == c.CustomerID)
                            {
                                c.AddOrder(o);
                            }
                        });
                        foreach (var order in c.Orders)
                        {
                            session.Save(order);
                        }

                    }
                    tx.Commit();
                }
            }
        }

        public static void PrintoutCustomersData(Configuration cfg)
        {
            var sessionFactory = cfg.BuildSessionFactory();
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    var criteria = session.CreateCriteria<Customer>();
                    var customers = criteria.List<Customer>();
                    foreach (var c in customers)
                    {
                        Console.WriteLine(c);
                    }
                }
            }
        }
    }
}
