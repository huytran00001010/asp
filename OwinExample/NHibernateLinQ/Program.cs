﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

using System.Reflection;
using NHibernateLinQ.Models;
using NHibernate.Linq;

namespace NHibernateLinQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var cfg = new Configuration();

            cfg.DataBaseIntegration(c =>
            {
                c.ConnectionString = "Data Source=TNDHUY;Initial Catalog=TrialNHibernate;Persist Security Info=True;User ID=ico;Password=ico";
                c.Driver<Sql2008ClientDriver>();
                c.Dialect<MsSql2012Dialect>();
                c.LogSqlInConsole = true;
            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());


            //Execution.Select(cfg);
            //Execution.SelectFields(cfg);
            //Execution.SelectDistinct(cfg);
            //Execution.SelectDistinctAndCount(cfg);
            //Execution.WhereClause(cfg);
            //Execution.WhereClause2(cfg);
            //Execution.WhereClauseNULL(cfg);
            //Execution.SelectAndOrder(cfg);
            //Execution.UpdateCustomer(cfg);
            Execution.WhereInAndSubquery(cfg);


            Console.WriteLine("Press <ENTER> to exit...");
            Console.ReadLine();



        }


        private static Customer CreateCustomer()
        {
            var customer = new Customer
            {
                CustomerName = "John",
                ContactName = "John",
                Address = "912 Str.",
                City = "NY",
                Country = "Ameriaca",
                PostalCode = "4242424242"
            };

            var order1 = new Order { OrderDate = DateTime.Now };

            customer.AddOrder(order1); var order2 = new Order
            {
                OrderDate = DateTime.Now.AddDays(-1)
            };

            customer.AddOrder(order2);
            return customer;
        }
    }
}
