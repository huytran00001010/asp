﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class Customer
    {
        private IList<Order> _orders;
        public Customer()
        {
            Orders = new List<Order>();
        }
        public virtual int CustomerID { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }

        public virtual IList<Order> Orders
        {
            get { return _orders; }
            set
            {
                _orders = value;
            }
        }
        public virtual void AddOrder(Order order) { Orders.Add(order); order.Customer = this; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.AppendFormat("{1} {2} ({0})", CustomerID, CustomerName, ContactName);
            result.AppendLine("\tOrders:");

            foreach (var order in Orders)
            {
                result.AppendLine("\t\t" + order);
            }

            return result.ToString();
        }

    }
}
