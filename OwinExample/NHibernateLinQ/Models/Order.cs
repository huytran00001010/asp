﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class Order
    {
        private Customer _customer;
        private Employee _employee;
        private Shipper _shipper;

        public virtual int OrderID { get; set; }
        public virtual DateTime OrderDate { get; set; }

        public virtual Customer Customer
        {
            get { return _customer; }
            set
            {
                _customer = value;
            }
        }
        public virtual Employee Employee { get => _employee; set => _employee = value; }
        public virtual Shipper Shipper { get => _shipper; set => _shipper = value; }
        public virtual IList<OrderDetail> OrderDetails { get; set; }


        public override string ToString()
        {
            return string.Format("Order Id: {0}", OrderID);
        }
    }
}
