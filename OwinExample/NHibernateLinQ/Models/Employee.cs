﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class Employee
    {
        private IList<Order> _orders;

        public Employee()
        {
            Orders = new List<Order>();
        }
        public virtual int EmployeeID { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual string Photo { get; set; }
        public virtual string Notes { get; set; }

        public virtual IList<Order> Orders { get => _orders; set => _orders = value; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.AppendFormat("{1} {2} ({0})", FirstName, LastName, EmployeeID);
            result.AppendLine("\tOrders:");

            foreach (var order in Orders)
            {
                result.AppendLine("\t\t" + order);
            }

            return result.ToString();
        }

    }
}
