﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class OrderDetail
    {
    
        public virtual int OrderDetailID { get; set; }
        public virtual int Quantity { get; set; }


        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
