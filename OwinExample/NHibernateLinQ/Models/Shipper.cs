﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class Shipper
    {
        public virtual int ShipperID { get; set; }
        public virtual string ShipperName { get; set; }
        public virtual string Phone { get; set; }
        public virtual IList<Order> Orders { get; set; }


    }
}
