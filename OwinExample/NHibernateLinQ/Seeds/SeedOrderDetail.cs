﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class SeedOrderDetail:OrderDetail
    {
        public virtual int OrderID { get; set; }
        public virtual int ProductID { get; set; }
        public virtual int Quantity { get; set; }
    }
}
