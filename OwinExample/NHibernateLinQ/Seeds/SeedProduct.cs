﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    class SeedProduct:Product
    {
        public virtual int SupplierID { get; set; }
        public virtual int CategoryID { get; set; }
    }
}
