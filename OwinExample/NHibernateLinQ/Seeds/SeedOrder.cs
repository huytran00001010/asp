﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateLinQ.Models
{
    public class SeedOrder:Order
    {
        public int CustomerID { get; set; }
        public virtual int EmployeeID { get; set; }
        public virtual int ShipperID { get; set; }
    }
}
