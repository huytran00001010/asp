﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using SelfAddIdentity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelfAddIdentity
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            // Register new instance of db context, user manager and sigin manager
            // Imcoming request will get these instance and use it as singleton class
            app.CreatePerOwinContext<DBModel>(DBModel.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);


            // Register a middleware for authentication by Cookie
            app.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions()
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new Microsoft.Owin.PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider()
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.

                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                    validateInterval: TimeSpan.FromMinutes(30),
                    regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                    /**
                     *  The OnValidateIdentity is configured for the cookie authentication to invalidate and regenerate the identity and auth cookie when the security timestamp is changed in the user table. 
                     *  Please note whenever there is user table changes like password change the identity and cookie is regenerated for making the existing sessions logged off for better security.
                     */
                }

            });
        }
    }
}