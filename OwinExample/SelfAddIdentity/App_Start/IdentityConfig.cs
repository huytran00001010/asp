﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using SelfAddIdentity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace SelfAddIdentity
{
    /***
     * ApplicationUserManager handle bussines logic for User Model
     * Validator
     * Check password
     * Correctness of model when login or create new ...
     * Interact to database for CRUD is already implemented
     */
    public class ApplicationUserManager: UserManager<ApplicationUser>
    {
        public ApplicationUserManager (IUserStore<ApplicationUser> store) : base(store) { }

        // Get instance of this class based on Singleton pattern
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            // Get DBModel from context(IOwinContext) and create new manager by connection from context
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser> (context.Get<DBModel>()));
            // Add a validator for username
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };
            // Add validator for password
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6
            };

            return manager;
        }
    }


    /***
     * ApplicationSignInManager manage task for login and hashed passwork
     * Checking user login informations
     */
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager (ApplicationUserManager userManager,IAuthenticationManager authenticationManager )
            :base(userManager, authenticationManager)
        {
            
        }

        // Get instance of this class based on Singleton pattern
        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            ApplicationSignInManager signInManager = new ApplicationSignInManager(context.Get<ApplicationUserManager>(), context.Authentication);
            return signInManager;
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        
    }


    /***
     * These Create functions will return instance of class as singleton instance
     * Authentication, ApplicationUserManager will be registered on context at Startup class and get to use afterwards 
     */
}