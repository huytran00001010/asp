﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SelfAddIdentity.Models
{
    public class RegisterViewModel
    {

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name ="Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage ="Confirm password does not match")]
        [DataType(DataType.Password)]
        public string  ConfirmPassword { get; set; }

    }

    public class LoginViewModel
    {
        [Display(Name ="User name")]
        [Required]
        public string UserName { get; set; }


        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Display(Name ="Remember login")]
        public bool RememberMe { get; set; }
    }
}