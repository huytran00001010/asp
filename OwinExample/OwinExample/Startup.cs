﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(OwinExample.Startup))]

namespace OwinExample
{
    using AppFunc = Func<IDictionary<string, object>, Task>;

    public class Startup
    {
        /*
         * Instead of Inline defination.
         * We define function to handle middleware in advance
         * We then create a delegate and bind to this method and pass to Use
         */
        public AppFunc Middleware1(AppFunc next)
        {
            AppFunc appFunc = async (IDictionary<string, object> context) =>
            {
                // Do some processing ("inbound")...
                var response = context["owin.ResponseBody"] as Stream;
                using (var writer = new StreamWriter(response))
                {
                    await writer.WriteAsync("<h2>MW2: Hello from Explicit Method middleware!</h2>");
                }
                // Invoke and await the next middleware component:
                await next.Invoke(context);

                // Do additional processing ("outbound")
                using (var writer = new StreamWriter(context["owin.ResponseBody"] as Stream))
                {
                    await writer.WriteAsync("<h2>MW2: Finish MW2!</h2>");
                }
            };
            return appFunc;
        }

        public void Configuration(IAppBuilder app)
        {

            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            /* 
             * Inline middleware
             * Put a function directly on Use
             * Because C# is not supported passing function directly to method
             * So It wrap function to a delegate and use the old syntax 
             * visit https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/delegates/how-to-declare-instantiate-and-use-a-delegate
             */
            app.Use(new Func<AppFunc, AppFunc>((AppFunc next) =>
            {
                AppFunc handler = async (IDictionary<string, object> context) =>
                {
                    // Do some processing ("inbound")...
                    using (var writer = new StreamWriter(context["owin.ResponseBody"] as Stream))
                    {
                        await writer.WriteAsync("<h1>MW1: Hello from inline Method middleware!</h1>");
                    }
                    // Invoke and await the next middleware component:
                    await next.Invoke(context);

                    // Do additional processing ("outbound")
                    using (var writer = new StreamWriter(context["owin.ResponseBody"] as Stream))
                    {
                        await writer.WriteAsync("<h1>MW1: Finish MW1!</h1>");
                    }
                };
                return handler;
            }));

            /*
             * Create delegate point to pre-defined method
             */
            Func<AppFunc, AppFunc> middleware1 = Middleware1;
            app.Use(middleware1);

            /*
             * Bind middleware by class
             */
            app.Use<Middleware2>();

            /*
             * Add middleware by extention method
             */
            app.UseMiddleware3();


            /*
             * Register middleware by add Strong type Provided by Katana
             */
            app.Use(async (context, next) =>
            {

                // Do some processing ("inbound")...
                await context.Response.WriteAsync("<h5>MW5: Hello from inline Method middleware using OwinContext!</h5>");
                // Invoke and await the next middleware component:
                await next.Invoke();
                // Do additional processing ("outbound")

                await context.Response.WriteAsync("<h5>MW5: Finish MW5!</h5>");

            });

            /*
             * Register middleware with no next, It will stop on this middleware and call back 
             */
            app.Run(context =>
            {
                return context.Response.WriteAsync("<h6>MW6: My First Owin Application Ended!</h6>");
            });


        }
    }
    /* 
     * Define middleware as class and pass to IAppBuilder by IAppBuilder.Use<T>()
     */
    public class Middleware2
    {
        AppFunc _next;
        public Middleware2(AppFunc next)
        {
            _next = next;
        }
        public async Task Invoke(IDictionary<string, object> context)
        {
            // Do some processing ("inbound")...
            var response = context["owin.ResponseBody"] as Stream;
            using (var writer = new StreamWriter(response))
            {
                await writer.WriteAsync("<h3>MW3: Hello from Explicit Method middleware!</h3>");
            }
            // Invoke and await the next middleware component:
            await _next.Invoke(context);

            // Do additional processing ("outbound")
            using (var writer = new StreamWriter(context["owin.ResponseBody"] as Stream))
            {
                await writer.WriteAsync("<h3>MW3: Finish MW3!</h3>");
            }
        }
    }

    /*
     * Create extention method for IAppBuilder
     * In C#, Extension methods enable you to "add" methods to existing types without creating a new derived type
     * visit https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods
     */
    public static class OwinExtensions
    {
        public static void UseMiddleware3(this IAppBuilder app)
        {
            app.Use<Middleware3>();
        }

    }

    internal class Middleware3
    {
        AppFunc _next;
        public Middleware3(AppFunc next)
        {
            _next = next;
        }
        public async Task Invoke(IDictionary<string, object> context)
        {
            // Do some processing ("inbound")...
            var response = context["owin.ResponseBody"] as Stream;
            using (var writer = new StreamWriter(response))
            {
                await writer.WriteAsync("<h4>MW4: Hello from Explicit Method middleware!</h4>");
            }
            // Invoke and await the next middleware component:
            await _next.Invoke(context);

            // Do additional processing ("outbound")
            using (var writer = new StreamWriter(context["owin.ResponseBody"] as Stream))
            {
                await writer.WriteAsync("<h4>MW4: Finish MW4!</h4>");
            }
        }

    }
}
